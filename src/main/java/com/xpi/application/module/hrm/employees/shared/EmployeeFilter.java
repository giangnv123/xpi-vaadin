package com.xpi.application.module.hrm.employees.shared;

public record EmployeeFilter(String search, EmpStatus status, Long departmentId) {

    public static EmployeeFilter defaultFilter() {
        return new EmployeeFilter(null, EmpStatus.Active, null);
    }
}
