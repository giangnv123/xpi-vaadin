package com.xpi.application.module.hrm.employees;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.service.annotation.DeleteExchange;
import org.springframework.web.service.annotation.GetExchange;
import org.springframework.web.service.annotation.HttpExchange;
import org.springframework.web.service.annotation.PostExchange;
import org.springframework.web.service.annotation.PutExchange;

import com.xpi.application.base.ApiResponse;
import com.xpi.application.base.GenericHttpClient;
import com.xpi.application.base.XPage;
import com.xpi.application.module.hrm.dto.EmployeeDto;
import com.xpi.application.module.hrm.employees.shared.EmployeeFilter;

@HttpExchange("/employees")
public interface EmployeeHttpClient extends GenericHttpClient<EmployeeDto, EmployeeFilter, String> {
    @GetExchange
    ApiResponse<XPage<EmployeeDto>> getAll(
            @RequestParam int page,

            @RequestParam int size,
            @RequestBody EmployeeFilter filter);

    @GetExchange("/count")
    ApiResponse<Integer> count(@RequestBody EmployeeFilter filter);

    @GetExchange("/{id}")
    ApiResponse<EmployeeDto> get(@PathVariable String id);

    @PostExchange
    ApiResponse<EmployeeDto> create(@RequestBody EmployeeDto dto);

    @PutExchange("/{id}")
    ApiResponse<EmployeeDto> update(@PathVariable String id, @RequestBody EmployeeDto dto);

    @DeleteExchange("/{id}")
    ApiResponse<Void> delete(@PathVariable String id);
}
