package com.xpi.application.module.hrm.insurances;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.service.annotation.DeleteExchange;
import org.springframework.web.service.annotation.GetExchange;
import org.springframework.web.service.annotation.HttpExchange;
import org.springframework.web.service.annotation.PostExchange;
import org.springframework.web.service.annotation.PutExchange;

import com.xpi.application.base.ApiResponse;
import com.xpi.application.base.GenericHttpClient;
import com.xpi.application.base.XPage;
import com.xpi.application.module.hrm.dto.InsuranceDto;
import com.xpi.application.module.hrm.insurances.shared.InsuranceFilter;

@HttpExchange("/insurances")
public interface InsuranceHttpClient extends GenericHttpClient<InsuranceDto, InsuranceFilter, String> {
    @GetExchange
    ApiResponse<XPage<InsuranceDto>> getAll(
            @RequestParam int page,
            @RequestParam int size,
            @RequestBody InsuranceFilter filter);

    @GetExchange("/count")
    ApiResponse<Integer> count(@RequestBody InsuranceFilter filter);

    @GetExchange("/{id}")
    ApiResponse<InsuranceDto> get(@PathVariable String id);

    @PostExchange
    ApiResponse<InsuranceDto> create(@RequestBody InsuranceDto dto);

    @PutExchange("/{id}")
    ApiResponse<InsuranceDto> update(@PathVariable String id, @RequestBody InsuranceDto dto);

    @DeleteExchange("/{id}")
    ApiResponse<Void> delete(@PathVariable String id);
}
