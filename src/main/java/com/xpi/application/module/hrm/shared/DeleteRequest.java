package com.xpi.application.module.hrm.shared;

import java.util.Set;

public class DeleteRequest<T> {

    private Set<T> ids;

    public DeleteRequest(Set<T> ids) {
        this.ids = ids;
    }

    public Set<T> getIds() {
        return ids;
    }

    public void setIds(Set<T> ids) {
        this.ids = ids;
    }
}
