package com.xpi.application.module.hrm.shared;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.xpi.application.base.ApiResponse;

@Service
public class CRUDService {

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private RestTemplate restTemplate;

    private <T> ApiResponse<T> executeHttpOperation(String url, T dto, Class<T> clazz, HttpMethod method) {
        HttpEntity<T> requestEntity = new HttpEntity<>(dto);
        try {
            ResponseEntity<ApiResponse<T>> response = restTemplate.exchange(
                    url, method, requestEntity, new ParameterizedTypeReference<ApiResponse<T>>() {
                    });
            return convertApiResponse(response, clazz);
        } catch (HttpClientErrorException e) {
            return parseErrorResponse(e.getResponseBodyAsString());
        } catch (Exception e) {
            return ApiResponse.UnexpectedError();
        }
    }

    private <T> ApiResponse<T> convertApiResponse(ResponseEntity<ApiResponse<T>> response, Class<T> clazz) {
        ApiResponse<T> data = response.getBody();
        T newDto = mapper.convertValue(data.getData(), clazz);
        data.setData(newDto);
        return data;
    }

    private <T> ApiResponse<T> parseErrorResponse(String responseBody) {
        try {
            return mapper.readValue(responseBody, new TypeReference<ApiResponse<T>>() {
            });
        } catch (IOException parseException) {
            return ApiResponse.FailToParseData();
        }
    }

    public <T> ApiResponse<Void> deleteEntity(String url) {
        return executeHttpOperation(url, null, Void.class, HttpMethod.DELETE);
    }

}
