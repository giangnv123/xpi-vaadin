package com.xpi.application.module.hrm.dto;

import com.xpi.application.module.hrm.employees.shared.FileStatus;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FileDto {

    private FileStatus status;
    private String id;
    private String type;
    private String path;
    private String name;
    private String title;
    private String description;
    private String extension;
    private String source;
    private Long fileSize;
}
