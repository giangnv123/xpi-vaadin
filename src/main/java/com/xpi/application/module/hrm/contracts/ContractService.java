package com.xpi.application.module.hrm.contracts;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xpi.application.base.AbstractService;
import com.xpi.application.base.GenericHttpClient;
import com.xpi.application.module.hrm.contracts.shared.ContractFilter;
import com.xpi.application.module.hrm.dto.ContractDto;

@Service
public class ContractService extends AbstractService<ContractDto, ContractFilter, String> {
    @Autowired
    ContractHttpClient client;

    @Override
    protected GenericHttpClient<ContractDto, ContractFilter, String> getClient() {
        return client;
    }

}
