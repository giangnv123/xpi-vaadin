package com.xpi.application.module.hrm.settings;

import java.time.DayOfWeek;
import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WeekendsDto {
    private Long id;
    private Set<DayOfWeek> daysOff;
}
