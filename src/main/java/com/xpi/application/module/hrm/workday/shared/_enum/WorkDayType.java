package com.xpi.application.module.hrm.workday.shared._enum;

import java.util.List;

public enum WorkDayType {
    WorkingDay,
    WeekendDaysOff,
    HolidaysOff,
    PaidLeaveDaysOff,
    UnpaidLeaveDaysOff,
    Absence;

    public static List<WorkDayType> getEditableTypes() {
        return List.of(WorkingDay, WeekendDaysOff, PaidLeaveDaysOff, UnpaidLeaveDaysOff);
    }

    public static List<WorkDayType> getUncountTypes() {
        return List.of(WeekendDaysOff, UnpaidLeaveDaysOff);
    }

    public static List<WorkDayType> getUnChangeTypes() {
        return List.of(WeekendDaysOff, UnpaidLeaveDaysOff, PaidLeaveDaysOff, Absence);
    }

    @Override
    public String toString() {
        switch (this) {
            case WorkingDay:
                return "Ngày đi làm";
            case WeekendDaysOff:
                return "Nghỉ cuối tuần";
            case HolidaysOff:
                return "Nghỉ lễ";
            case PaidLeaveDaysOff:
                return "Nghỉ phép";
            case UnpaidLeaveDaysOff:
                return "Nghỉ không phép";
            case Absence:
                return "Tạm vắng";
            default:
                return super.toString();
        }
    }
}
