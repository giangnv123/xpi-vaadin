package com.xpi.application.module.hrm.workday;

import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.service.annotation.GetExchange;
import org.springframework.web.service.annotation.PostExchange;
import org.springframework.web.service.annotation.PutExchange;

import com.xpi.application.base.ApiResponse;
import com.xpi.application.module.hrm.workday.shared.EmployeeWorkdayDetailsDto;
import com.xpi.application.module.hrm.workday.shared.EmployeeWorkdayDto;
import com.xpi.application.module.hrm.workday.shared.WorkdayFilter;
import com.xpi.application.module.hrm.workday.shared.WorkingDayOfMonthOverView;

public interface WorkdayHttpClient {
    @GetExchange("/workdays/employees")
    ApiResponse<List<EmployeeWorkdayDto>> getEntities(@RequestBody WorkdayFilter filter);

    @GetExchange("/workdays/summary")
    ApiResponse<WorkingDayOfMonthOverView> getWorkingDaysOverView(@RequestBody WorkdayFilter filter);

    @PutExchange("/employee-workday/{id}")
    ApiResponse<EmployeeWorkdayDetailsDto> updateWorkdayDetails(@PathVariable String id,
            @RequestBody EmployeeWorkdayDetailsDto dto);

    @PostExchange("/employee-workday")
    ApiResponse<EmployeeWorkdayDetailsDto> createWorkdayDetails(@RequestBody EmployeeWorkdayDetailsDto dto);
}
