package com.xpi.application.module.hrm.dashboard;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.xpi.application.base.ApiResponse;
import com.xpi.application.base.XPage;
import com.xpi.application.module.hrm.dashboard.shared.DashboardDto;
import com.xpi.application.module.hrm.dto.LeaveDto;
import com.xpi.application.module.hrm.leave.shared.LeaveFilter;
import com.xpi.application.module.hrm.leave.shared.LeaveStatus;

@Service
public class DashboardService {

    @Autowired
    private DashboardHttpClient client;

    public DashboardDto getDashboardDto() {

        ApiResponse<DashboardDto> data = client.getDashboardDto();

        return Optional.ofNullable(data).map(ApiResponse::getData).orElse(null);
    }

    public List<LeaveDto> getAllPendingLeaves() {
        LeaveFilter filter = new LeaveFilter(null, LeaveStatus.Pending, null);
        PageRequest pageable = PageRequest.of(0, Integer.MAX_VALUE);

        ApiResponse<XPage<LeaveDto>> data = client.getAllPendingLeaves(pageable.getPageNumber(), pageable.getPageSize(),
                filter);
        return Optional.ofNullable(data).map(ApiResponse::getData).map(XPage::getContent)
                .orElse(Collections.emptyList());
    }

    public Boolean updateLeave(String id, LeaveDto dto) {
        ApiResponse<LeaveDto> data = client.updateLeave(id, dto);
        return data.isSuccess();
    }
}
