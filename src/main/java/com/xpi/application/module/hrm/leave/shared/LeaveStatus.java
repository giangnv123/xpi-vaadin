package com.xpi.application.module.hrm.leave.shared;

public enum LeaveStatus {
    Pending,
    Accepted,
    Rejected;

    public String toReadableString() {
        switch (this) {
            case Pending:
                return "Đang chờ duyệt";
            case Accepted:
                return "Đã duyệt";
            case Rejected:
                return "Đã từ chối";
            default:
                return super.toString();
        }
    }
}
