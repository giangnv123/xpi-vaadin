package com.xpi.application.module.hrm.shared;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import com.xpi.application.base.ApiResponse;
import com.xpi.application.base.XPage;
import com.xpi.application.module.hrm.dto.DepartmentDto;
import com.xpi.application.module.hrm.dto.EmployeeDto;
import com.xpi.application.module.hrm.dto.FileDto;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class HelperService {

    @Autowired
    private HelperClientHttp client;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private CRUDService crudService;

    @Value("${api.file}")
    private String API_FILE;

    public <T> T exchangeEntity(String url, ParameterizedTypeReference<T> responseType) {
        try {
            ResponseEntity<T> response = restTemplate.exchange(url, HttpMethod.GET, null, responseType);
            return response.getBody();
        } catch (Exception e) {
            log.error("Error during REST GET exchange for URL: {}", url, e);
            return null;
        }
    }

    /* Additional methods */
    public List<DepartmentDto> getAllDepartments() {
        ApiResponse<XPage<DepartmentDto>> data = client.getDepartments(Integer.MAX_VALUE);

        if (!data.isSuccess())
            return List.of();

        return Optional.ofNullable(data.getData()).map(XPage::getContent).orElse(List.of());
    }

    public List<EmployeeDto> getAllEmployees() {

        ApiResponse<XPage<EmployeeDto>> data = client.getEmployees(Integer.MAX_VALUE);

        if (data == null)
            return List.of();

        return Optional.ofNullable(data.getData()).map(XPage::getContent).orElse(List.of());
    }

    public boolean deleteFile(String id) {
        String url = API_FILE + "/" + id;
        ApiResponse<Void> response = crudService.deleteEntity(url);
        return response.isSuccess();
    }

    public FileDto uploadFile(MultipartFile file) {
        long fixedAccount = 999;
        String fixedSource = "HRM";

        HttpHeaders headers = getMultipartFormDataHeader();

        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();

        body.add("account", fixedAccount);
        body.add("source", fixedSource);
        body.add("file", convertToBytResource(file));

        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);

        try {
            ResponseEntity<FileDto> response = restTemplate.postForEntity(API_FILE, requestEntity, FileDto.class);
            return response.getBody();
        } catch (Exception e) {
            log.error("Failed to upload files", e);
            return null;
        }
    }

    private Resource convertToBytResource(MultipartFile file) {
        if (file.isEmpty()) {
            log.error("File is empty: {}", file.getOriginalFilename());
            throw new IllegalArgumentException("File cannot be empty");
        }
        try {
            Resource resource = new ByteArrayResource(file.getBytes()) {
                @Override
                public String getFilename() {
                    return file.getOriginalFilename();
                }
            };
            return resource;
        } catch (IOException e) {
            log.error("Failed to read file: {}", file.getOriginalFilename(), e);
            throw new RuntimeException("Failed to convert file", e);
        }
    }

    public ApiResponse<DepartmentDto> createDepartment(DepartmentDto departmentDto) {
        return client.createDepartment(departmentDto);
    }

    public HttpHeaders getJsonHeader() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return headers;
    }

    public HttpHeaders getMultipartFormDataHeader() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        return headers;
    }
}
