package com.xpi.application.module.hrm.employees;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.xpi.application.base.AbstractService;
import com.xpi.application.base.ApiResponse;
import com.xpi.application.base.GenericHttpClient;
import com.xpi.application.module.hrm.dto.EmployeeControlDto;
import com.xpi.application.module.hrm.dto.EmployeeDto;
import com.xpi.application.module.hrm.dto.FileDto;
import com.xpi.application.module.hrm.employees.shared.EmpStatus;
import com.xpi.application.module.hrm.employees.shared.EmployeeFilter;
import com.xpi.application.module.hrm.shared.HelperService;

@Service
public class EmployeeService extends AbstractService<EmployeeDto, EmployeeFilter, String> {
    @Autowired
    private HelperService helperService;

    @Autowired
    private EmployeeHttpClient client;

    @Override
    protected GenericHttpClient<EmployeeDto, EmployeeFilter, String> getClient() {
        return client;
    }

    public ApiResponse<EmployeeDto> createEntity(EmployeeControlDto employeeCreateDto) {

        EmployeeDto createEmployeDto = populateData(employeeCreateDto);

        FileDto fileDto = getFileDto(employeeCreateDto.getFileAvatar());

        if (fileDto != null)
            createEmployeDto.setPhoto(fileDto.getName());

        ApiResponse<EmployeeDto> response = client.create(createEmployeDto);

        if (response.getData() == null && fileDto != null) {
            helperService.deleteFile(fileDto.getId());
            return response;
        }
        return response;
    }

    public ApiResponse<EmployeeDto> updateEmployee(String id, EmployeeControlDto employeeCreateDto) {
        EmployeeDto updateEmployeeDto = populateData(employeeCreateDto);

        FileDto fileDto = getFileDto(employeeCreateDto.getFileAvatar());

        if (fileDto != null)
            updateEmployeeDto.setPhoto(fileDto.getName());

        return client.update(id, updateEmployeeDto);
    }

    public ApiResponse<EmployeeDto> deleteEmployee(String id, EmployeeDto dto) {
        dto.setStatus(EmpStatus.Deleted);
        return client.update(id, dto);
    }

    private FileDto getFileDto(MultipartFile file) {
        if (file == null)
            return null;

        FileDto fileDto = helperService.uploadFile(file);

        return fileDto;
    }

    private EmployeeDto populateData(EmployeeControlDto employeeCreateDto) {
        EmployeeDto createEmployeDto = new EmployeeDto();

        /* Personal Detail */
        createEmployeDto.setFullname(employeeCreateDto.getFullname());
        createEmployeDto.setEmail(employeeCreateDto.getEmail());
        createEmployeDto.setPhone(employeeCreateDto.getPhone());
        createEmployeDto.setIdNumber(employeeCreateDto.getIdNumber());
        createEmployeDto.setAddress(employeeCreateDto.getAddress());
        createEmployeDto.setGender(employeeCreateDto.getGender());
        createEmployeDto.setDateOfBirth(employeeCreateDto.getDateOfBirth());
        createEmployeDto.setStatus(employeeCreateDto.getStatus());

        /* Company Detail */
        createEmployeDto.setSalary(employeeCreateDto.getSalary());
        createEmployeDto.setDaysOff(employeeCreateDto.getDaysOff());
        createEmployeDto.setDepartmentId(employeeCreateDto.getDepartmentId());
        createEmployeDto.setDesignation(employeeCreateDto.getDesignation());
        createEmployeDto.setDateStart(employeeCreateDto.getDateStart());

        return createEmployeDto;
    }

}
