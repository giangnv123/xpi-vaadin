package com.xpi.application.module.hrm.settings;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xpi.application.base.ApiResponse;

@Service
public class SettingsService {

    @Autowired
    private SettingHttpClient client;;

    public ApiResponse<WeekendsDto> getWeekends() {
        return client.getWeekends();
    }

    public ApiResponse<WeekendsDto> updateOrCreateWeekends(WeekendsDto dto) {
        System.out.println(dto.getDaysOff().toString() + " Dto weekends");
        if (dto.getId() == null) {
            return client.create(dto);
        } else
            return client.update(dto);
    }
}
