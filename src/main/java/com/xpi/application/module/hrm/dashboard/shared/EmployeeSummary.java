package com.xpi.application.module.hrm.dashboard.shared;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeSummary {
    private long totalEmployees;
    private long activeEmployees;
    private long suspendedEmployees;
    private long absenceEmployees;
}
