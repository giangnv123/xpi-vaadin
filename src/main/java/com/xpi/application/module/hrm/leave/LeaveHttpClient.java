package com.xpi.application.module.hrm.leave;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.service.annotation.DeleteExchange;
import org.springframework.web.service.annotation.GetExchange;
import org.springframework.web.service.annotation.HttpExchange;
import org.springframework.web.service.annotation.PostExchange;
import org.springframework.web.service.annotation.PutExchange;

import com.xpi.application.base.ApiResponse;
import com.xpi.application.base.GenericHttpClient;
import com.xpi.application.base.XPage;
import com.xpi.application.module.hrm.dto.LeaveDto;
import com.xpi.application.module.hrm.leave.shared.LeaveFilter;

@HttpExchange("/leaves")
public interface LeaveHttpClient extends GenericHttpClient<LeaveDto, LeaveFilter, String> {
    @GetExchange
    ApiResponse<XPage<LeaveDto>> getAll(
            @RequestParam int page,
            @RequestParam int size,
            @RequestBody LeaveFilter filter);

    @GetExchange("/count")
    ApiResponse<Integer> count(@RequestBody LeaveFilter filter);

    @GetExchange("/{id}")
    ApiResponse<LeaveDto> get(@PathVariable String id);

    @PostExchange
    ApiResponse<LeaveDto> create(@RequestBody LeaveDto dto);

    @PutExchange("/{id}")
    ApiResponse<LeaveDto> update(@PathVariable String id, @RequestBody LeaveDto dto);

    @DeleteExchange("/{id}")
    ApiResponse<Void> delete(@PathVariable String id);
}
