package com.xpi.application.module.hrm;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatusCode;
import org.springframework.web.client.RestClient;
import org.springframework.web.client.support.RestClientAdapter;
import org.springframework.web.service.invoker.HttpServiceProxyFactory;

import com.xpi.application.module.hrm.contracts.ContractHttpClient;
import com.xpi.application.module.hrm.dashboard.DashboardHttpClient;
import com.xpi.application.module.hrm.department.DepartmentHttpClient;
import com.xpi.application.module.hrm.employees.EmployeeHttpClient;
import com.xpi.application.module.hrm.holidays.HolidayHttpClient;
import com.xpi.application.module.hrm.insurances.InsuranceHttpClient;
import com.xpi.application.module.hrm.leave.LeaveHttpClient;
import com.xpi.application.module.hrm.settings.SettingHttpClient;
import com.xpi.application.module.hrm.shared.HelperClientHttp;
import com.xpi.application.module.hrm.workday.WorkdayHttpClient;

import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
public class HRMApiConfig {
    private String baseUrl = "http://localhost:8080/xapi/v1/hrm";

    @Bean("hrm")
    RestClient createClient() {
        try {
            RestClient client = RestClient.builder().baseUrl(baseUrl)
                    .defaultStatusHandler(HttpStatusCode::isError, (req, res) -> {
                        log.info("Get error from server url: " + req.getURI() + " - Method :" +
                                req.getMethod());
                    }).build();
            return client;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    @Bean
    InsuranceHttpClient insuranceHttpClient(@Qualifier("hrm") RestClient client) {
        HttpServiceProxyFactory factory = HttpServiceProxyFactory.builderFor(RestClientAdapter.create(client)).build();
        return factory.createClient(InsuranceHttpClient.class);
    }

    @Bean
    ContractHttpClient contractHttpClient(@Qualifier("hrm") RestClient client) {
        HttpServiceProxyFactory factory = HttpServiceProxyFactory.builderFor(RestClientAdapter.create(client)).build();
        return factory.createClient(ContractHttpClient.class);
    }

    @Bean
    DashboardHttpClient dashboardHttpClient(@Qualifier("hrm") RestClient client) {
        HttpServiceProxyFactory factory = HttpServiceProxyFactory.builderFor(RestClientAdapter.create(client)).build();
        return factory.createClient(DashboardHttpClient.class);
    }

    @Bean
    LeaveHttpClient leaveHttpClient(@Qualifier("hrm") RestClient client) {
        HttpServiceProxyFactory factory = HttpServiceProxyFactory.builderFor(RestClientAdapter.create(client)).build();
        return factory.createClient(LeaveHttpClient.class);
    }

    @Bean
    EmployeeHttpClient employeeHttpClient(@Qualifier("hrm") RestClient client) {
        HttpServiceProxyFactory factory = HttpServiceProxyFactory.builderFor(RestClientAdapter.create(client)).build();
        return factory.createClient(EmployeeHttpClient.class);
    }

    @Bean
    SettingHttpClient settingsHttpClient(@Qualifier("hrm") RestClient client) {
        HttpServiceProxyFactory factory = HttpServiceProxyFactory.builderFor(RestClientAdapter.create(client)).build();
        return factory.createClient(SettingHttpClient.class);
    }

    @Bean
    DepartmentHttpClient departmentHttpClient(@Qualifier("hrm") RestClient client) {
        HttpServiceProxyFactory factory = HttpServiceProxyFactory.builderFor(RestClientAdapter.create(client)).build();
        return factory.createClient(DepartmentHttpClient.class);
    }

    @Bean
    HolidayHttpClient holidayHttpClient(@Qualifier("hrm") RestClient client) {
        HttpServiceProxyFactory factory = HttpServiceProxyFactory.builderFor(RestClientAdapter.create(client)).build();
        return factory.createClient(HolidayHttpClient.class);
    }

    @Bean
    WorkdayHttpClient workdayHttpClient(@Qualifier("hrm") RestClient client) {
        HttpServiceProxyFactory factory = HttpServiceProxyFactory.builderFor(RestClientAdapter.create(client)).build();
        return factory.createClient(WorkdayHttpClient.class);
    }

    @Bean
    HelperClientHttp helperHttpClient(@Qualifier("hrm") RestClient client) {
        HttpServiceProxyFactory factory = HttpServiceProxyFactory.builderFor(RestClientAdapter.create(client)).build();
        return factory.createClient(HelperClientHttp.class);
    }

}
