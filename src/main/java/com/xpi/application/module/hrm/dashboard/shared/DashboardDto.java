package com.xpi.application.module.hrm.dashboard.shared;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DashboardDto {
    private EmployeeSummary employee;

    private ContractSummary contract;

    private double totalPayout;

}
