package com.xpi.application.module.hrm.workday.shared;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class WorkingDayOfMonthOverView {
    private int totalWorkDays;
    private int totalDaysOfMonth;
    private Set<DayOfWeek> daysOff;
    private List<LocalDate> workingDaysOfMonth;
    private List<LocalDate> allDaysOfMonth;
}
