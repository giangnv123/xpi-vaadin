package com.xpi.application.module.hrm.insurances;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xpi.application.base.AbstractService;
import com.xpi.application.base.GenericHttpClient;
import com.xpi.application.module.hrm.dto.InsuranceDto;
import com.xpi.application.module.hrm.insurances.shared.InsuranceFilter;

@Service
public class InsuranceService extends AbstractService<InsuranceDto, InsuranceFilter, String> {
    @Autowired
    InsuranceHttpClient client;

    @Override
    protected GenericHttpClient<InsuranceDto, InsuranceFilter, String> getClient() {
        return client;
    }
}
