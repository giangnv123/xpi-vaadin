package com.xpi.application.module.hrm.holidays;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.service.annotation.DeleteExchange;
import org.springframework.web.service.annotation.GetExchange;
import org.springframework.web.service.annotation.HttpExchange;
import org.springframework.web.service.annotation.PostExchange;
import org.springframework.web.service.annotation.PutExchange;

import com.xpi.application.base.ApiResponse;
import com.xpi.application.base.GenericHttpClient;
import com.xpi.application.base.XPage;
import com.xpi.application.module.hrm.dto.HolidayDto;
import com.xpi.application.module.hrm.holidays.shared.HolidayFilter;

@HttpExchange("/holidays")
public interface HolidayHttpClient extends GenericHttpClient<HolidayDto, HolidayFilter, Long> {
    @GetExchange
    ApiResponse<XPage<HolidayDto>> getAll(
            @RequestParam int page,
            @RequestParam int size,
            @RequestBody HolidayFilter filter);

    @GetExchange("/count")
    ApiResponse<Integer> count(@RequestBody HolidayFilter filter);

    @GetExchange("/{id}")
    ApiResponse<HolidayDto> get(@PathVariable Long id);

    @PostExchange
    ApiResponse<HolidayDto> create(@RequestBody HolidayDto dto);

    @PutExchange("/{id}")
    ApiResponse<HolidayDto> update(@PathVariable Long id, @RequestBody HolidayDto dto);

    @DeleteExchange("/{id}")
    ApiResponse<Void> delete(@PathVariable Long id);
}
