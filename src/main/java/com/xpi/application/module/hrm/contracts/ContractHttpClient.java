package com.xpi.application.module.hrm.contracts;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.service.annotation.DeleteExchange;
import org.springframework.web.service.annotation.GetExchange;
import org.springframework.web.service.annotation.HttpExchange;
import org.springframework.web.service.annotation.PostExchange;
import org.springframework.web.service.annotation.PutExchange;

import com.xpi.application.base.ApiResponse;
import com.xpi.application.base.GenericHttpClient;
import com.xpi.application.base.XPage;
import com.xpi.application.module.hrm.contracts.shared.ContractFilter;
import com.xpi.application.module.hrm.dto.ContractDto;

@HttpExchange("/contracts")
public interface ContractHttpClient extends GenericHttpClient<ContractDto, ContractFilter, String> {
    @GetExchange
    ApiResponse<XPage<ContractDto>> getAll(
            @RequestParam int page,
            @RequestParam int size,
            @RequestBody ContractFilter filter);

    @GetExchange("/count")
    ApiResponse<Integer> count(@RequestBody ContractFilter filter);

    @GetExchange("/{id}")
    ApiResponse<ContractDto> get(@PathVariable String id);

    @PostExchange
    ApiResponse<ContractDto> create(@RequestBody ContractDto dto);

    @PutExchange("/{id}")
    ApiResponse<ContractDto> update(@PathVariable String id, @RequestBody ContractDto dto);

    @DeleteExchange("/{id}")
    ApiResponse<Void> delete(@PathVariable String id);
}
