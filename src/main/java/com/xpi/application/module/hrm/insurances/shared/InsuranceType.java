package com.xpi.application.module.hrm.insurances.shared;

public enum InsuranceType {
    Compulsory,
    Optional;

    public String toReadableString() {
        switch (this) {
            case Compulsory:
                return "Bắt buộc";
            case Optional:
                return "Tự nguyện";
            default:
                return super.toString();
        }
    }
}
