package com.xpi.application.module.hrm.employees.shared;

public enum EmpStatus {
    Active,
    Absence,
    Retired,
    Deleted;

    public String toReadableString() {
        switch (this) {
            case Active:
                return "Đang hoạt động";
            case Absence:
                return "Tạm vắng";
            case Retired:
                return "Đã nghỉ";
            case Deleted:
                return "Đã xoá";
            default:
                return super.toString();
        }
    }
}
