package com.xpi.application.module.hrm.dto;

import java.time.LocalDate;

import com.xpi.application.base.AbstractDto;
import com.xpi.application.module.hrm.insurances.shared.InsuranceType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class InsuranceDto extends AbstractDto<String> {
        private String employeeId;
        private String insuranceNumber;
        private Double contributionRate;
        private Double contributeAmount;
        private String insuranceCompany;
        private String placeOfProvision;
        private InsuranceType type;
        private String notes;
        private EmployeeDto employee;
        private LocalDate issueDate;
}