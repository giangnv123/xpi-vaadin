package com.xpi.application.module.hrm.workday.shared;

import java.time.LocalDate;
import java.time.Month;

import com.xpi.application.module.hrm.employees.shared.EmpStatus;

public record WorkdayFilter(int year, int month, EmpStatus status) {

    private static final LocalDate currentDate = LocalDate.now();

    public WorkdayFilter(int year, Month month, EmpStatus status) {
        this(year, month.getValue(), status);
    }

    public static WorkdayFilter defaultFilter() {
        return new WorkdayFilter(currentDate.getYear(), currentDate.getMonth(), EmpStatus.Active);
    }
}
