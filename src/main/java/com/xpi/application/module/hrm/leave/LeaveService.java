package com.xpi.application.module.hrm.leave;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xpi.application.base.AbstractService;
import com.xpi.application.base.GenericHttpClient;
import com.xpi.application.module.hrm.dto.LeaveDto;
import com.xpi.application.module.hrm.leave.shared.LeaveFilter;

@Service
public class LeaveService extends AbstractService<LeaveDto, LeaveFilter, String> {

    @Autowired
    private LeaveHttpClient client;

    @Override
    protected GenericHttpClient<LeaveDto, LeaveFilter, String> getClient() {
        return client;
    }

}
