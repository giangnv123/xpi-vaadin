package com.xpi.application.module.hrm.contracts.shared;

import java.util.List;

public enum ContractStatus {
    Empty,
    Running, // Contract is currently active
    Cancel, // Contract has been canceled
    Expired;// Contract has expired

    public static List<ContractStatus> getValuesExceptEmpty() {
        return List.of(Running, Cancel, Expired);
    }

    public String toReadableString() {
        switch (this) {
            case Running:
                return "Đang hoạt động";
            case Cancel:
                return "Đã huỷ";
            case Empty:
                return "Chưa có";
            case Expired:
                return "Đã hết hạn";
            default:
                return super.toString();
        }
    }
}
