package com.xpi.application.module.hrm.dto;

import java.io.Serializable;

import org.springframework.web.multipart.MultipartFile;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
@AllArgsConstructor
public class EmployeeControlDto extends EmployeeDto implements Serializable {
        private MultipartFile fileAvatar;

        public void populateData(EmployeeDto employeeDto,
                        MultipartFile fileAvatarParam) {
                /* Personal Data */
                this.setFullname(employeeDto.getFullname());
                this.setEmail(employeeDto.getEmail());
                this.setPhone(employeeDto.getPhone());
                this.setIdNumber(employeeDto.getIdNumber());
                this.setAddress(employeeDto.getAddress());
                this.setGender(employeeDto.getGender());
                this.setDateOfBirth(employeeDto.getDateOfBirth());
                this.setFileAvatar(fileAvatarParam);

                /* Company Data */
                this.setSalary(employeeDto.getSalary());
                this.setDaysOff(employeeDto.getDaysOff());
                this.setStatus(employeeDto.getStatus());
                this.setDepartmentId(employeeDto.getDepartmentId());
                this.setDesignation(employeeDto.getDesignation());
                this.setDateStart(employeeDto.getDateStart());
        }
}