package com.xpi.application.module.hrm.holidays.shared;

public record HolidayFilter(String search) {

    public static HolidayFilter defaultFilter() {
        return new HolidayFilter("");
    }
}
