package com.xpi.application.module.hrm.workday.shared;

import java.time.LocalDate;

import com.xpi.application.base.AbstractDto;
import com.xpi.application.module.hrm.workday.shared._enum.WorkDayType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class EmployeeWorkdayDetailsDto extends AbstractDto<String> {
    private WorkDayType type;
    private double count;
    private LocalDate date;
    private String note;
    private String employeeId;
}
