package com.xpi.application.module.hrm.dto;

import java.time.LocalDate;

import com.xpi.application.base.AbstractDto;
import com.xpi.application.module.hrm.leave.shared.LeaveStatus;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LeaveDto extends AbstractDto<String> {
        private String title;
        private String description;
        private LeaveStatus status;
        private EmployeeDto employee;
        private String employeeId;
        private LocalDate dateStart;
        private LocalDate dateEnd;
        private Boolean paidLeave;
}