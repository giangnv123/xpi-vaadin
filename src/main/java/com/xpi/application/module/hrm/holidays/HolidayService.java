package com.xpi.application.module.hrm.holidays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xpi.application.base.AbstractService;
import com.xpi.application.base.GenericHttpClient;
import com.xpi.application.module.hrm.dto.HolidayDto;
import com.xpi.application.module.hrm.holidays.shared.HolidayFilter;

@Service
public class HolidayService extends AbstractService<HolidayDto, HolidayFilter, Long> {
    @Autowired
    private HolidayHttpClient client;

    @Override
    protected GenericHttpClient<HolidayDto, HolidayFilter, Long> getClient() {
        return client;
    }

}
