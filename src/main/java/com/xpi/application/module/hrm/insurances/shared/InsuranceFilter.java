package com.xpi.application.module.hrm.insurances.shared;

public record InsuranceFilter(String search) {

    public static InsuranceFilter defaultFilter() {
        return new InsuranceFilter(null);
    }
}
