package com.xpi.application.module.hrm.shared;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.service.annotation.GetExchange;
import org.springframework.web.service.annotation.PostExchange;

import com.xpi.application.base.ApiResponse;
import com.xpi.application.base.XPage;
import com.xpi.application.module.hrm.dto.DepartmentDto;
import com.xpi.application.module.hrm.dto.EmployeeDto;

public interface HelperClientHttp {
    @GetExchange("/departments")
    ApiResponse<XPage<DepartmentDto>> getDepartments(@RequestParam int size);

    @PostExchange("/departments")
    ApiResponse<DepartmentDto> createDepartment(@RequestBody DepartmentDto departmentDto);

    @GetExchange("/employees")
    ApiResponse<XPage<EmployeeDto>> getEmployees(@RequestParam int size);

}
