package com.xpi.application.module.hrm.employees.shared;

public enum FileStatus {
    Pending,
    Public
}