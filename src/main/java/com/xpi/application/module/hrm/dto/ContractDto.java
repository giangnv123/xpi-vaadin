package com.xpi.application.module.hrm.dto;

import java.time.LocalDate;

import com.xpi.application.base.AbstractDto;
import com.xpi.application.module.hrm.contracts.shared.ContractStatus;
import com.xpi.application.module.hrm.contracts.shared.ContractType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ContractDto extends AbstractDto<String> {
        /* Basic infor */
        private String title;
        private String note;
        private LocalDate startDate;
        private LocalDate endDate;
        private ContractStatus status;
        private ContractType contractType;
        private EmployeeDto employee;
        private LocalDate signedDate;
        private String employeeId;

        /* Job */
        private String jobTitle;
        private String jobDescription;
        private Double salary;
}