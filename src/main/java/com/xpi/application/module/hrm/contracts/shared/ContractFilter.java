package com.xpi.application.module.hrm.contracts.shared;

public record ContractFilter(String search, ContractType type, ContractStatus status) {

    public static ContractFilter defaultFilter() {
        return new ContractFilter(null, null, null);
    }
}
