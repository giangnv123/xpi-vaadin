package com.xpi.application.module.hrm.dashboard.shared;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ContractSummary {
    private long totalContracts;
    private long runningContracts;
    private long expiredContracts;
    private long canceledContracts;
}
