package com.xpi.application.module.hrm.settings;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.service.annotation.GetExchange;
import org.springframework.web.service.annotation.HttpExchange;
import org.springframework.web.service.annotation.PostExchange;
import org.springframework.web.service.annotation.PutExchange;

import com.xpi.application.base.ApiResponse;

@HttpExchange("/workdays/weekends")
public interface SettingHttpClient {
    @GetExchange
    ApiResponse<WeekendsDto> getWeekends();

    @PostExchange
    ApiResponse<WeekendsDto> create(@RequestBody WeekendsDto dto);

    @PutExchange
    ApiResponse<WeekendsDto> update(@RequestBody WeekendsDto dto);
}
