package com.xpi.application.module.hrm.dto;

import java.time.LocalDate;
import java.util.List;

import com.xpi.application.base.AbstractDto;
import com.xpi.application.module.hrm.employees.shared.EmpStatus;
import com.xpi.application.module.hrm.employees.shared.Gender;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class EmployeeDto extends AbstractDto<String> {
        /* Personal */
        private String fullname;
        private Gender gender;
        private LocalDate dateOfBirth;
        private String phone;
        private String email;
        private String photo;
        private String address;
        private String idNumber;
        private List<ContractDto> contract;
        private InsuranceDto insurance;

        /* Company */
        private Double salary;
        private int daysOff;
        private int paidDaysOffRemaining;
        private LocalDate dateStart;
        private LocalDate dateEnd;
        private String designation;
        private EmpStatus status;
        private Long departmentId;

        /* Additional */
        private DepartmentDto department;

        public DepartmentDto getDepartmentDto() {
                return department;
        }
}