package com.xpi.application.module.hrm.contracts.shared;

public enum ContractType {
    Permanent,
    Temporary;

    public String toReadableString() {
        switch (this) {
            case Permanent:
                return "Vô thời hạn";
            case Temporary:
                return "Có thời hạn";
            default:
                return super.toString();
        }
    }
}
