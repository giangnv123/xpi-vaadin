package com.xpi.application.module.hrm.workday;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xpi.application.base.ApiResponse;
import com.xpi.application.module.hrm.workday.shared.EmployeeWorkdayDetailsDto;
import com.xpi.application.module.hrm.workday.shared.EmployeeWorkdayDto;
import com.xpi.application.module.hrm.workday.shared.WorkdayFilter;
import com.xpi.application.module.hrm.workday.shared.WorkingDayOfMonthOverView;

@Service
public class WorkDayService {

    @Autowired
    private WorkdayHttpClient client;

    public List<EmployeeWorkdayDto> getEntities(WorkdayFilter filter) {
        ApiResponse<List<EmployeeWorkdayDto>> data = client.getEntities(filter);

        return data.isSuccess() ? data.getData() : Collections.emptyList();
    }

    public WorkingDayOfMonthOverView getWorkingDaysOverView(WorkdayFilter filter) {
        ApiResponse<WorkingDayOfMonthOverView> data = client.getWorkingDaysOverView(filter);
        return data.isSuccess() ? data.getData() : new WorkingDayOfMonthOverView();
    }

    public ApiResponse<EmployeeWorkdayDetailsDto> createWorkdayDetails(EmployeeWorkdayDetailsDto dto) {
        return client.createWorkdayDetails(dto);
    }

    public ApiResponse<EmployeeWorkdayDetailsDto> updateWorkdayDetails(String id, EmployeeWorkdayDetailsDto dto) {
        return client.updateWorkdayDetails(id, dto);
    }
}
