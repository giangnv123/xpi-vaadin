package com.xpi.application.module.hrm.department;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xpi.application.base.AbstractService;
import com.xpi.application.base.GenericHttpClient;
import com.xpi.application.module.hrm.department.shared.DepartmentFilter;
import com.xpi.application.module.hrm.dto.DepartmentDto;

@Service
public class DepartmentService extends AbstractService<DepartmentDto, DepartmentFilter, Long> {

    @Autowired
    private DepartmentHttpClient client;

    @Override
    protected GenericHttpClient<DepartmentDto, DepartmentFilter, Long> getClient() {
        return client;
    }

}
