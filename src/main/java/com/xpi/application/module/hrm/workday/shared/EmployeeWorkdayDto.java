package com.xpi.application.module.hrm.workday.shared;

import java.util.List;

import com.xpi.application.module.hrm.dto.EmployeeDto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeWorkdayDto {
        private EmployeeDto employee;
        private double totalCount;
        private int totalWorkDay;
        private int totalPaidDaysOff;
        private int totalUnPaidDaysOff;
        private List<EmployeeWorkdayDetailsDto> workdays;

}