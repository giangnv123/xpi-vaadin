package com.xpi.application.module.hrm.dto;

import java.time.LocalDate;

import com.xpi.application.base.AbstractDto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class HolidayDto extends AbstractDto<Long> {
        private LocalDate date;
        private String description;
}