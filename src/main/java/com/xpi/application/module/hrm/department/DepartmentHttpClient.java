package com.xpi.application.module.hrm.department;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.service.annotation.DeleteExchange;
import org.springframework.web.service.annotation.GetExchange;
import org.springframework.web.service.annotation.HttpExchange;
import org.springframework.web.service.annotation.PostExchange;
import org.springframework.web.service.annotation.PutExchange;

import com.xpi.application.base.ApiResponse;
import com.xpi.application.base.GenericHttpClient;
import com.xpi.application.base.XPage;
import com.xpi.application.module.hrm.department.shared.DepartmentFilter;
import com.xpi.application.module.hrm.dto.DepartmentDto;

@HttpExchange("/departments")
public interface DepartmentHttpClient extends GenericHttpClient<DepartmentDto, DepartmentFilter, Long> {
    @GetExchange
    ApiResponse<XPage<DepartmentDto>> getAll(
            @RequestParam int page,
            @RequestParam int size,
            @RequestBody DepartmentFilter filter);

    @GetExchange("/count")
    ApiResponse<Integer> count(@RequestBody DepartmentFilter filter);

    @GetExchange("/{id}")
    ApiResponse<DepartmentDto> get(@PathVariable Long id);

    @PostExchange
    ApiResponse<DepartmentDto> create(@RequestBody DepartmentDto dto);

    @PutExchange("/{id}")
    ApiResponse<DepartmentDto> update(@PathVariable Long id, @RequestBody DepartmentDto dto);

    @DeleteExchange("/{id}")
    ApiResponse<Void> delete(@PathVariable Long id);

}
