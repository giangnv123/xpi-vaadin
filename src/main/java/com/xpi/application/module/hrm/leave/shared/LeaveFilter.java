package com.xpi.application.module.hrm.leave.shared;

public record LeaveFilter(String search, LeaveStatus status, Boolean isPaid) {

    public static LeaveFilter defaultFilter() {
        return new LeaveFilter(null, null, null);
    }
}
