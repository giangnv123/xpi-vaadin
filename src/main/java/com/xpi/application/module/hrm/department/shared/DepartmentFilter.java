package com.xpi.application.module.hrm.department.shared;

public record DepartmentFilter(String search) {

    public static DepartmentFilter defaultFilter() {
        return new DepartmentFilter("");
    }
}
