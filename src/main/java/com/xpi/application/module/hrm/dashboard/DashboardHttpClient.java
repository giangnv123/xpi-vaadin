package com.xpi.application.module.hrm.dashboard;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.service.annotation.GetExchange;
import org.springframework.web.service.annotation.HttpExchange;
import org.springframework.web.service.annotation.PutExchange;

import com.xpi.application.base.ApiResponse;
import com.xpi.application.base.XPage;
import com.xpi.application.module.hrm.dashboard.shared.DashboardDto;
import com.xpi.application.module.hrm.dto.LeaveDto;
import com.xpi.application.module.hrm.leave.shared.LeaveFilter;

@HttpExchange
public interface DashboardHttpClient {
    @GetExchange("/dashboard")
    ApiResponse<DashboardDto> getDashboardDto();

    @GetExchange("/leaves")
    ApiResponse<XPage<LeaveDto>> getAllPendingLeaves(
            @RequestParam int page, @RequestParam int size,
            @RequestBody LeaveFilter filter);

    @PutExchange("/leaves/{id}")
    ApiResponse<LeaveDto> updateLeave(@PathVariable String id, @RequestBody LeaveDto dto);
}
