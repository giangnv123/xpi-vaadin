package com.xpi.application.module.hrm.employees.shared;

public enum Gender {
    Male, Female;

    public String toReadableString() {
        switch (this) {
            case Male:
                return "Nam";
            case Female:
                return "Nữ";
            default:
                return super.toString();
        }
    }
}
