package com.xpi.application.module.crm.contacts.shared;

public enum ProductCategory {
    HOUSE_HOLD, GARDEN;

    public String toReadableString() {
        switch (this) {
            case HOUSE_HOLD:
                return "Đồ gia dụng";
            case GARDEN:
                return "Dụng cụ làm vườn";
            default:
                return super.toString();
        }
    }
}
