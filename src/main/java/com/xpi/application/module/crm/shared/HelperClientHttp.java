package com.xpi.application.module.crm.shared;

import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.service.annotation.GetExchange;
import org.springframework.web.service.annotation.PostExchange;
import org.springframework.web.service.annotation.PutExchange;

import com.xpi.application.base.ApiResponse;
import com.xpi.application.base.XPage;
import com.xpi.application.module.crm.contacts.shared.ContactFilter;
import com.xpi.application.module.crm.dto.ContactDto;
import com.xpi.application.module.crm.dto.InteractionDto;
import com.xpi.application.module.crm.dto.subs.ContactSummaryDto;
import com.xpi.application.module.crm.dto.subs.ItemDto;
import com.xpi.application.module.crm.dto.subs.SourceDto;
import com.xpi.application.module.crm.dto.subs.TagDto;
import com.xpi.application.module.crm.dto.subs.TopicDto;

public interface HelperClientHttp {
    /* Contact */

    @GetExchange("/contacts")
    ApiResponse<XPage<ContactDto>> getAllContact(@RequestParam int page, @RequestParam int size);

    @GetExchange("/contacts")
    ApiResponse<XPage<ContactDto>> getAllNewContact(@RequestParam int page, @RequestParam int size,
            @RequestBody ContactFilter filter);

    @GetExchange("/contacts/{id}")
    ApiResponse<ContactDto> getContactById(@PathVariable String id);

    @GetExchange("/contacts/summary")
    ApiResponse<List<ContactSummaryDto>> getContactSummary();

    @PostExchange("/contacts")
    ApiResponse<ContactDto> createContact(@RequestBody ContactDto contactDto);

    @PutExchange("/contacts/{id}")
    ApiResponse<ContactDto> updateContact(@PathVariable String id, @RequestBody ContactDto contactDto);

    /* Interaction */

    @PostExchange("/interactions")
    ApiResponse<InteractionDto> createInteraction(@RequestBody InteractionDto interactionDto);

    @GetExchange("/interactions/today")
    ApiResponse<List<InteractionDto>> getTodayInteraction();

    @PutExchange("/interactions/{id}")
    ApiResponse<InteractionDto> updateInteraction(@PathVariable String id, @RequestBody InteractionDto interactionDto);

    /* Tag */
    @GetExchange("/tags")
    ApiResponse<XPage<TagDto>> getAllTags();

    @PostExchange("/tags")
    ApiResponse<TagDto> createNewTag(@RequestBody TagDto dto);

    /* Source */
    @GetExchange("/sources")
    ApiResponse<XPage<SourceDto>> getAllSources();

    @PostExchange("/sources")
    ApiResponse<SourceDto> createSource(@RequestBody SourceDto dto);

    /* Item */
    @GetExchange("/items")
    ApiResponse<XPage<ItemDto>> getAllItems();

    @PostExchange("/items")
    ApiResponse<ItemDto> createItem(@RequestBody ItemDto dto);

    /* Topic */
    @GetExchange("/topics")
    ApiResponse<XPage<TopicDto>> getAllTopics();

    @PostExchange("/topics")
    ApiResponse<TopicDto> createTopic(@RequestBody TopicDto dto);
}
