package com.xpi.application.module.crm.dashboards.views.components;

import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

import org.vaadin.lineawesome.LineAwesomeIcon;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.xpi.application.module.crm.dto.ContactDto;
import com.xpi.application.views.base.abs.AbstractGrid;
import com.xpi.application.views.base.views.ViewHelper;

public class BasicContactGrid extends AbstractGrid<ContactDto> {

    private static final String NAME_TEXT = "Tên liên hệ";
    private static final String PHONE_TEXT = "Điện thoại";
    private static final String NOTES_TEXT = "Ghi chú";
    private static final String FUTURE_INTEACTION_TEXT = "Tái tương tác";
    private static final String INTERACTION_TEXT = "Tương tác";
    private static final String STATUS_TEXT = "Trạng thái";
    private static final String CATEGORY_TEXT = "Tệp";

    private List<ContactDto> list;

    public BasicContactGrid(Consumer<ContactDto> clickEdit, Consumer<ContactDto> clickDelete, List<ContactDto> list) {
        super(clickEdit, clickDelete);
        this.list = list;
        initialSetup();
    }

    @Override
    protected void setupGrid() {

        addColumnWithIcon(contactNameDisplay(), NAME_TEXT, LineAwesomeIcon.USER_SOLID);
        addColumnWithIcon(phoneDisplay(), PHONE_TEXT, LineAwesomeIcon.PHONE_ALT_SOLID);
        addColumnWithIcon(displayNotes(), NOTES_TEXT, LineAwesomeIcon.EDIT_SOLID);
        addColumnWithIcon(displayFutureInteraction(), FUTURE_INTEACTION_TEXT, LineAwesomeIcon.CLOCK_SOLID);
        addColumnWithIcon(displayInteraction(), INTERACTION_TEXT, LineAwesomeIcon.HANDSHAKE_SOLID);
        addColumnWithIcon(displayCategory(), CATEGORY_TEXT, LineAwesomeIcon.USERS_SOLID);
        addColumnWithIcon(displayStatus(), STATUS_TEXT, LineAwesomeIcon.USER_SOLID);

        this.setItems(list);
    }

    private ComponentRenderer<Component, ContactDto> contactNameDisplay() {
        return new ComponentRenderer<>(dto -> {
            return new Span(dto.getName());
        });
    }

    private ComponentRenderer<Component, ContactDto> displayNotes() {
        return new ComponentRenderer<>(dto -> {
            return new Span(dto.getNotes());
        });
    }

    private ComponentRenderer<Component, ContactDto> displayFutureInteraction() {
        return new ComponentRenderer<>(dto -> {

            if (dto.getFutureInteraction() == null)
                return new Span();

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yy HH:mm");
            Span time = new Span(formatter.format(dto.getFutureInteraction()));

            return time;
        });
    }

    private ComponentRenderer<Component, ContactDto> phoneDisplay() {
        return new ComponentRenderer<>(dto -> {
            return new Span(dto.getPhone());
        });
    }

    private ComponentRenderer<Component, ContactDto> displayCategory() {
        return new ComponentRenderer<>(dto -> {
            Span status = ViewHelper.createStatusComponent(dto.getType().toReadableString(),
                    dto.getType().toString().toLowerCase());

            return status;

        });
    }

    private ComponentRenderer<Component, ContactDto> displayInteraction() {
        return new ComponentRenderer<>(dto -> {
            HorizontalLayout layout = new HorizontalLayout();
            layout.setAlignItems(FlexComponent.Alignment.CENTER);

            int interactionCount = Optional.ofNullable(dto.getInteraction()).map(interactions -> interactions.size())
                    .orElse(0);

            layout.add(new Span(String.valueOf(interactionCount)));
            return layout;
        });
    }

    private ComponentRenderer<Component, ContactDto> displayStatus() {
        return new ComponentRenderer<>(dto -> ViewHelper.createStatusComponent(dto.getStatus().toReadableString(),
                dto.getStatus().toString().toLowerCase()));
    }

}
