package com.xpi.application.module.crm.dashboards.views;

import org.vaadin.lineawesome.LineAwesomeIcon;

import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.xpi.application.module.crm.dashboards.CRMDashboardPresenter;
import com.xpi.application.module.crm.dashboards.views.components.BasicContactGrid;
import com.xpi.application.module.crm.dashboards.views.components.CardContent;
import com.xpi.application.views.base.abs.AbstractView;
import com.xpi.application.views.base.utils.ColorUtils;
import com.xpi.application.views.base.views.ViewHelper;
import com.xpi.application.views.layout.CRMLayout;

@PageTitle("Dashboard")
@Route(value = "crm/dashboard", layout = CRMLayout.class)
public class CRMDashboardView extends AbstractView {

    private CardContent cardContent;

    private CRMDashboardPresenter presenter;

    private BasicContactGrid contactGrid;

    public CRMDashboardView(CRMDashboardPresenter presenter) {
        this.presenter = presenter;
        setupLayout();
    }

    public void initializeUI() {
        cardContent = new CardContent(presenter.getContactSummary());
    }

    private void setupLayout() {
        initializeUI();
        addClassName("dashboard");
        add(cardContent, gridContainer());
    }

    private Div gridContainer() {
        Div container = new Div();
        container.setWidthFull();
        container.add(createNewContactGrid(), createTodayInteraction());
        return container;
    }

    private VerticalLayout createTodayInteraction() {
        VerticalLayout container = new VerticalLayout();
        container.setWidthFull();
        // container.add(ViewHelper.cardTitleWithIcon("Tương tác hôm nay",
        // LineAwesomeIcon.HANDSHAKE,
        // ColorUtils.WHITE, ColorUtils.BG_PURPLE));
        // container.add();
        return container;
    }

    private VerticalLayout createNewContactGrid() {
        VerticalLayout container = new VerticalLayout();
        container.setWidthFull();
        container.add(ViewHelper.cardTitleWithIcon("Liên hệ mới", LineAwesomeIcon.PHONE_SOLID,
                ColorUtils.WHITE, ColorUtils.BG_PRIMARY));
        contactGrid = new BasicContactGrid(null, null, presenter.getAllNewContact());
        container.add(contactGrid);
        return container;
    }

}
