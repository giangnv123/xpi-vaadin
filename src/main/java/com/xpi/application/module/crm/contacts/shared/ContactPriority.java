package com.xpi.application.module.crm.contacts.shared;

public enum ContactPriority {
    LOW, MEDIUM, HIGH;

    public String toReadableString() {
        switch (this) {
            case LOW:
                return "Thấp";
            case MEDIUM:
                return "Trung bình";
            case HIGH:
                return "Cao";
            default:
                return super.toString();
        }
    }

    public int getNumber() {
        switch (this) {
            case LOW:
                return 0;
            case MEDIUM:
                return 1;
            case HIGH:
                return 2;
            default:
                return 1;
        }
    }

    public static ContactPriority getPriorityByNumber(int number) {
        switch (number) {
            case 0:
                return LOW;
            case 1:
                return MEDIUM;
            case 2:
                return HIGH;
            default:
                return MEDIUM;
        }
    }

}
