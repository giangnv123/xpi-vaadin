package com.xpi.application.module.crm.dto;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

import com.xpi.application.base.AbstractDto;
import com.xpi.application.module.crm.contacts.shared.ContactPriority;
import com.xpi.application.module.crm.contacts.shared.ContactStatus;
import com.xpi.application.module.crm.contacts.shared.ContactCategory;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ContactDto extends AbstractDto<String> {
        private String id;
        private String name;
        private String phone;
        private String otherPhone;
        private String website;
        private String email;
        private ContactPriority priority = ContactPriority.MEDIUM;
        private ContactCategory type = ContactCategory.POTENTIAL;
        private ContactStatus status = ContactStatus.NEW;
        private String notes;
        private LocalDateTime futureInteraction;
        private List<InteractionDto> interaction;
        private String address;

        /* relation */
        private Long sourceId;
        private Set<Long> itemIds;

}