package com.xpi.application.module.crm.contacts.shared;

public record ContactFilter(String search, ContactCategory type, ContactStatus status) {

    public static ContactFilter defaultFilter() {
        return new ContactFilter(null, null, null);
    }
}
