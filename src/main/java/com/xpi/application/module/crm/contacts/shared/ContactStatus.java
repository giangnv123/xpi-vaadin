package com.xpi.application.module.crm.contacts.shared;

public enum ContactStatus {
    NEW, INACTIVE, ACTIVE, PENDING, BLACKLISTED, DELETED;

    public String toReadableString() {
        switch (this) {
            case NEW:
                return "Mới";
            case INACTIVE:
                return "Không Hoạt Động";
            case ACTIVE:
                return "Hoạt động";
            case PENDING:
                return "Cần liên hệ";
            case BLACKLISTED:
                return "Hạn chế";
            case DELETED:
                return "Đã xoá";
            default:
                return super.toString();
        }
    }
}
