package com.xpi.application.module.crm.dashboards.views.components;

import java.util.List;

import org.vaadin.lineawesome.LineAwesomeIcon;

import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H3;
import com.xpi.application.module.crm.dto.subs.ContactSummaryDto;
import com.xpi.application.views.base.utils.ColorUtils;
import com.xpi.application.views.base.views.ViewHelper;

public class CardContent extends Div {

    private List<ContactSummaryDto> contactSummaries;

    public CardContent(List<ContactSummaryDto> contactSummaries) {
        this.contactSummaries = contactSummaries;
        setupLayout();
    }

    private void setupLayout() {
        addClassName("card-container");

        if (contactSummaries == null) {
            add(new H3("Xảy ra lỗi! Vui lòng tải lại trang"));
            return;
        }

        initilizeCardContent();
    }

    private void initilizeCardContent() {

        if (contactSummaries == null)
            return;

        contactSummaries.stream().forEach(dto -> {
            Div card = createCard(dto.type().toReadableString(), LineAwesomeIcon.USERS_SOLID,
                    dto.type().toString().toLowerCase(),
                    ColorUtils.WHITE, String.valueOf(dto.count()));
            add(card);
        });
    }

    private Div createCard(String title, LineAwesomeIcon icon, String iconColor, String backgroundColor,
            String content) {

        Div textHeader = ViewHelper.cardTitleWithIcon(title, icon, iconColor, backgroundColor);
        H3 contentHeader = new H3(content);
        contentHeader.getStyle().set("margin-left", "5px");

        Div card = new Div();
        card.addClassName("card");
        card.add(textHeader, contentHeader);

        return card;
    }
}
