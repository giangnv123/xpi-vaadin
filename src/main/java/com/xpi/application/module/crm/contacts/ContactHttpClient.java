package com.xpi.application.module.crm.contacts;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.service.annotation.DeleteExchange;
import org.springframework.web.service.annotation.GetExchange;
import org.springframework.web.service.annotation.HttpExchange;
import org.springframework.web.service.annotation.PostExchange;
import org.springframework.web.service.annotation.PutExchange;

import com.xpi.application.base.ApiResponse;
import com.xpi.application.base.GenericHttpClient;
import com.xpi.application.base.XPage;
import com.xpi.application.module.crm.contacts.shared.ContactFilter;
import com.xpi.application.module.crm.dto.ContactDto;

@HttpExchange("/contacts")
public interface ContactHttpClient extends GenericHttpClient<ContactDto, ContactFilter, String> {

    @GetExchange
    ApiResponse<XPage<ContactDto>> getAll(
            @RequestParam int page,
            @RequestParam int size,
            @RequestBody ContactFilter filter);

    @GetExchange("/count")
    ApiResponse<Integer> count(@RequestBody ContactFilter filter);

    @GetExchange("/{id}")
    ApiResponse<ContactDto> get(@PathVariable String id);

    @PostExchange
    ApiResponse<ContactDto> create(@RequestBody ContactDto dto);

    @PutExchange("/{id}")
    ApiResponse<ContactDto> update(@PathVariable String id, @RequestBody ContactDto dto);

    @DeleteExchange("/{id}")
    ApiResponse<Void> delete(@PathVariable String id);
}
