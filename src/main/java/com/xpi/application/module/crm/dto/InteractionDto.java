package com.xpi.application.module.crm.dto;

import java.time.LocalDateTime;
import java.util.Set;

import com.xpi.application.base.AbstractDto;
import com.xpi.application.module.crm.contacts.shared.ContactPriority;
import com.xpi.application.module.crm.dto.subs.TagDto;
import com.xpi.application.module.crm.contacts.shared.ContactCategory;
import com.xpi.application.module.crm.interactions.shared.InteractionType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class InteractionDto extends AbstractDto<String> {

        /* Interaction Infor */
        private String id;
        private String subject;
        private LocalDateTime dateTime = LocalDateTime.now();
        private Integer duration;
        private String outcome;
        private String notes;
        private InteractionType type;
        private Set<TagDto> tags;
        private String topic;

        /* relation for tag */
        private Set<Long> tagIds;
        private Long topicId;

        /* Contact info */
        private String contactId;
        private String contactName;
        private String contactPhone;
        private String contactWebsite;
        private String contactNotes;
        private ContactPriority contactPriority;
        private ContactCategory contactType;
        private Long sourceId;

        public void populateContact(ContactDto contact) {
                this.contactId = contact.getId();
                this.contactName = contact.getName();
                this.contactPhone = contact.getPhone();
                this.contactWebsite = contact.getWebsite();
                this.contactNotes = contact.getNotes();
                this.contactPriority = contact.getPriority();
                this.contactType = contact.getType();
        }

}