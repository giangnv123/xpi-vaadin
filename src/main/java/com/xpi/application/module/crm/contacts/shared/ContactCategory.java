package com.xpi.application.module.crm.contacts.shared;

public enum ContactCategory {
    UNREPLIED, POTENTIAL, CURRENT, VIP, DISINTERESTED;

    public String toReadableString() {
        switch (this) {
            case UNREPLIED:
                return "Chưa phản hồi";
            case POTENTIAL:
                return "Tiềm năng";
            case CURRENT:
                return "Đã mua hàng";
            case VIP:
                return "VIP";
            case DISINTERESTED:
                return "Không có nhu cầu";
            default:
                return super.toString();
        }
    }
}
