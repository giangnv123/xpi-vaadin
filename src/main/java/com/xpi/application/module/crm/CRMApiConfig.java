package com.xpi.application.module.crm;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatusCode;
import org.springframework.web.client.RestClient;
import org.springframework.web.client.support.RestClientAdapter;
import org.springframework.web.service.invoker.HttpServiceProxyFactory;

import com.xpi.application.module.crm.contacts.ContactHttpClient;
import com.xpi.application.module.crm.interactions.InteractHttpClient;
import com.xpi.application.module.crm.shared.HelperClientHttp;

import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
public class CRMApiConfig {

    private String baseUrl = "http://localhost:8080/xapi/v1/crm";

    @Bean("crm")
    RestClient createClient() {
        RestClient client = RestClient.builder().baseUrl(baseUrl)
                .defaultStatusHandler(HttpStatusCode::isError, (req, res) -> {
                    log.info("Get error from server url: " + req.getURI() + " - Method :" + req.getMethod());
                }).build();
        return client;
    }

    @Bean
    ContactHttpClient contactHttpClient(@Qualifier("crm") RestClient client) {
        HttpServiceProxyFactory factory = HttpServiceProxyFactory.builderFor(RestClientAdapter.create(client)).build();
        return factory.createClient(ContactHttpClient.class);
    }

    @Bean
    InteractHttpClient interactionHttpClient(@Qualifier("crm") RestClient client) {
        HttpServiceProxyFactory factory = HttpServiceProxyFactory.builderFor(RestClientAdapter.create(client)).build();
        return factory.createClient(InteractHttpClient.class);
    }

    @Bean
    HelperClientHttp helperCRMHttpClient(@Qualifier("crm") RestClient client) {
        HttpServiceProxyFactory factory = HttpServiceProxyFactory.builderFor(RestClientAdapter.create(client)).build();
        return factory.createClient(HelperClientHttp.class);
    }
}
