package com.xpi.application.module.crm.interactions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xpi.application.base.AbstractService;
import com.xpi.application.base.GenericHttpClient;
import com.xpi.application.module.crm.dto.InteractionDto;
import com.xpi.application.module.crm.interactions.shared.InteractionFilter;

@Service
public class InteractionService extends AbstractService<InteractionDto, InteractionFilter, String> {

    @Autowired
    private InteractHttpClient client;

    @Override
    protected GenericHttpClient<InteractionDto, InteractionFilter, String> getClient() {
        return client;
    }
}
