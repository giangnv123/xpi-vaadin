package com.xpi.application.module.crm.contacts;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xpi.application.base.AbstractService;
import com.xpi.application.base.GenericHttpClient;
import com.xpi.application.module.crm.contacts.shared.ContactFilter;
import com.xpi.application.module.crm.dto.ContactDto;

@Service
public class ContactService extends AbstractService<ContactDto, ContactFilter, String> {

    @Autowired
    ContactHttpClient client;

    @Override
    protected GenericHttpClient<ContactDto, ContactFilter, String> getClient() {
        return client;
    }
}
