package com.xpi.application.module.crm.dto.subs;

import com.xpi.application.module.crm.contacts.shared.ContactCategory;

public record ContactSummaryDto(
                ContactCategory type,
                int count) {
}