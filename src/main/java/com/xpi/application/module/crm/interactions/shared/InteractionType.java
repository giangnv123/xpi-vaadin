package com.xpi.application.module.crm.interactions.shared;

public enum InteractionType {
    CALL, EMAIL, MEETING, CHAT;

    public String toReadableString() {
        switch (this) {
            case CALL:
                return "Gọi điện";
            case EMAIL:
                return "Email";
            case MEETING:
                return "Gặp mặt";
            case CHAT:
                return "Chat";
            default:
                return super.toString();
        }
    }
}
