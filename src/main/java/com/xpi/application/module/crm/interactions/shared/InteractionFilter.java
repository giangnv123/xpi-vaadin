package com.xpi.application.module.crm.interactions.shared;

import java.util.Set;

public record InteractionFilter(String search, Set<Long> tagIds) {

    public static InteractionFilter defaultFilter() {
        return new InteractionFilter(null, null);
    }
}
