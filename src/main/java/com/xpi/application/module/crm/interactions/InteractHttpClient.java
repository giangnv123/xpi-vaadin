package com.xpi.application.module.crm.interactions;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.service.annotation.DeleteExchange;
import org.springframework.web.service.annotation.GetExchange;
import org.springframework.web.service.annotation.HttpExchange;
import org.springframework.web.service.annotation.PostExchange;
import org.springframework.web.service.annotation.PutExchange;

import com.xpi.application.base.ApiResponse;
import com.xpi.application.base.GenericHttpClient;
import com.xpi.application.base.XPage;
import com.xpi.application.module.crm.dto.InteractionDto;
import com.xpi.application.module.crm.interactions.shared.InteractionFilter;

@HttpExchange("/interactions")
public interface InteractHttpClient extends GenericHttpClient<InteractionDto, InteractionFilter, String> {
    @GetExchange
    ApiResponse<XPage<InteractionDto>> getAll(
            @RequestParam int page,
            @RequestParam int size,
            @RequestBody InteractionFilter filter);

    @GetExchange("/count")
    ApiResponse<Integer> count(@RequestBody InteractionFilter filter);

    @GetExchange("/{id}")
    ApiResponse<InteractionDto> get(@PathVariable String id);

    @PostExchange
    ApiResponse<InteractionDto> create(@RequestBody InteractionDto dto);

    @PutExchange("/{id}")
    ApiResponse<InteractionDto> update(@PathVariable String id, @RequestBody InteractionDto dto);

    @DeleteExchange("/{id}")
    ApiResponse<Void> delete(@PathVariable String id);
}
