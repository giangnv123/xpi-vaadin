package com.xpi.application.module.crm.dashboards;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.flow.spring.annotation.SpringComponent;
import com.xpi.application.module.crm.dto.ContactDto;
import com.xpi.application.module.crm.dto.InteractionDto;
import com.xpi.application.module.crm.dto.subs.ContactSummaryDto;
import com.xpi.application.module.crm.shared.CRMHelperService;

@SpringComponent
public class CRMDashboardPresenter {

    @Autowired
    private CRMHelperService helperService;

    public List<ContactSummaryDto> getContactSummary() {
        return helperService.getContactSummary();
    }

    public List<InteractionDto> getTodayInteraction() {
        return helperService.getTodayInteractions();
    }

    public List<ContactDto> getAllNewContact() {
        return helperService.getAllNewContact();
    }
}