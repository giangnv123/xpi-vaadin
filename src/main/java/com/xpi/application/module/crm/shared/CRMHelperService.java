package com.xpi.application.module.crm.shared;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xpi.application.base.ApiResponse;
import com.xpi.application.base.XPage;
import com.xpi.application.module.crm.contacts.shared.ContactFilter;
import com.xpi.application.module.crm.contacts.shared.ContactStatus;
import com.xpi.application.module.crm.dto.ContactDto;
import com.xpi.application.module.crm.dto.InteractionDto;
import com.xpi.application.module.crm.dto.subs.ContactSummaryDto;
import com.xpi.application.module.crm.dto.subs.ItemDto;
import com.xpi.application.module.crm.dto.subs.SourceDto;
import com.xpi.application.module.crm.dto.subs.TagDto;
import com.xpi.application.module.crm.dto.subs.TopicDto;
import com.xpi.application.views.base.utils.CustomApiResponse;

@Service
public class CRMHelperService {
    @Autowired
    private HelperClientHttp client;


    /* Get all - create - update interactions */
    public CustomApiResponse<InteractionDto> saveInteraction(InteractionDto dto) {
        Boolean isUpdated = dto.getId() != null;

        ApiResponse<InteractionDto> response;

        if (isUpdated) {
            response = client.updateInteraction(dto.getId(), dto);
        } else {
            response = client.createInteraction(dto);
        }

        return CustomApiResponse.save(response, isUpdated, response.getData());
    }

    /* Contact */
    public CustomApiResponse<ContactDto> saveContact(ContactDto dto) {
        Boolean isUpdated = dto.getId() != null;

        ApiResponse<ContactDto> response;

        if (isUpdated) {
            response = client.updateContact(dto.getId(), dto);
        } else {
            response = client.createContact(dto);
        }

        return CustomApiResponse.save(response, isUpdated, response.getData());
    }


    public List<ContactDto> getAllContact() {
        ApiResponse<XPage<ContactDto>> response = client.getAllContact(0, Integer.MAX_VALUE);

        if (response.isSuccess())
            return response.getData().getContent();

        return List.of();
    }

    public List<ContactDto> getAllNewContact() {
        ApiResponse<XPage<ContactDto>> response = client.getAllNewContact(0, Integer.MAX_VALUE,
                new ContactFilter(null, null, ContactStatus.NEW));

        if (response.isSuccess())
            return response.getData().getContent();

        return List.of();
    }

    public List<InteractionDto> getTodayInteractions() {
        ApiResponse<List<InteractionDto>> response = client.getTodayInteraction();

        if (response.isSuccess())
            return response.getData();

        return List.of();
    }

    public CustomApiResponse<ContactDto> getContactById(String id) {
        ApiResponse<ContactDto> response = client.getContactById(id);
        return new CustomApiResponse<>(response.isSuccess(), response.getMessage(), response.getData());
    }

    public List<ContactSummaryDto> getContactSummary() {
        ApiResponse<List<ContactSummaryDto>> response = client.getContactSummary();

        if (response.isSuccess())
            return response.getData();

        return List.of();
    }

    /* Tag */
    public Set<TagDto> getAllTags() {
        ApiResponse<XPage<TagDto>> response = client.getAllTags();

        if (response.isSuccess()) {
            List<TagDto> tags = response.getData().getContent();
            return Set.copyOf(tags);
        }

        return Set.of();
    }

    public CustomApiResponse<TagDto> saveTag(TagDto dto) {
        ApiResponse<TagDto> response = client.createNewTag(dto);
        return CustomApiResponse.save(response, dto.getId() != null, response.getData());
    }

    /* Source */

    public Set<SourceDto> getAllSources() {
        ApiResponse<XPage<SourceDto>> response = client.getAllSources();

        if (response.isSuccess()) {
            List<SourceDto> sources = response.getData().getContent();
            return Set.copyOf(sources);
        }

        return Set.of();
    }

    public CustomApiResponse<SourceDto> saveSource(SourceDto dto) {
        ApiResponse<SourceDto> response = client.createSource(dto);
        return CustomApiResponse.save(response, dto.getId() != null, response.getData());
    }

    /* Topic */

    public Set<TopicDto> getAllTopics() {
        ApiResponse<XPage<TopicDto>> response = client.getAllTopics();

        if (response.isSuccess()) {
            List<TopicDto> topics = response.getData().getContent();
            return Set.copyOf(topics);
        }

        return Set.of();
    }

    public CustomApiResponse<TopicDto> saveTopic(TopicDto dto) {
        ApiResponse<TopicDto> response = client.createTopic(dto);
        return CustomApiResponse.save(response, dto.getId() != null, response.getData());
    }

    /* Item */

    public Set<ItemDto> getAllItems() {
        ApiResponse<XPage<ItemDto>> response = client.getAllItems();

        if (response.isSuccess()) {
            List<ItemDto> items = response.getData().getContent();
            return Set.copyOf(items);
        }

        return Set.of();
    }

    public CustomApiResponse<ItemDto> saveItem(ItemDto dto) {
        ApiResponse<ItemDto> response = client.createItem(dto);
        return CustomApiResponse.save(response, dto.getId() != null, response.getData());
    }
}
