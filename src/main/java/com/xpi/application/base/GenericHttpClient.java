package com.xpi.application.base;

public interface GenericHttpClient<T, F, ID> {
    ApiResponse<XPage<T>> getAll(int page, int size, F filter);

    ApiResponse<Integer> count(F filter);

    ApiResponse<T> get(ID id);

    ApiResponse<T> create(T dto);

    ApiResponse<T> update(ID id, T dto);

    ApiResponse<Void> delete(ID id);
}
