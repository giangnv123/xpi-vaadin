package com.xpi.application.base;

import org.springframework.http.HttpStatus;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ApiResponse<T> {
    private static String DEFAULT_ERROR_MESSAGE = "Unexpected error";
    private static String PARSE_DATA_ERROR_MESSAGE = "Fail to parse data";
    private static String INTERNAL_SERVER_ERROR_MESSAGE = "Internal Server Error";

    private HttpStatus status;
    private T data;
    private String message;
    private String error;
    private Boolean readable;

    public Boolean isSuccess() {
        return status.is2xxSuccessful();
    }

    public static <T> ApiResponse<T> UnexpectedError() {
        return new ApiResponse<>(HttpStatus.INTERNAL_SERVER_ERROR, null, DEFAULT_ERROR_MESSAGE,
                INTERNAL_SERVER_ERROR_MESSAGE, false);
    }

    public static <T> ApiResponse<T> FailToParseData() {
        return new ApiResponse<>(HttpStatus.INTERNAL_SERVER_ERROR, null, PARSE_DATA_ERROR_MESSAGE,
                INTERNAL_SERVER_ERROR_MESSAGE, false);
    }
}
