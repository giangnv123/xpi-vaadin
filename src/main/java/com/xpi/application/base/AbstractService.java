package com.xpi.application.base;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.xpi.application.views.base.utils.CustomApiResponse;

public abstract class AbstractService<T extends AbstractDto<ID>, F, ID> {

    protected abstract GenericHttpClient<T, F, ID> getClient();

    public List<T> getAll(Pageable page, F filter) {
        ApiResponse<XPage<T>> response = getClient().getAll(page.getPageNumber(), page.getPageSize(), filter);
        return response.isSuccess() ? response.getData().getContent() : List.of();
    }

    public int count(F filter) {
        ApiResponse<Integer> response = getClient().count(filter);
        return response.isSuccess() ? response.getData() : 0;
    }

    public T get(ID id) {
        ApiResponse<T> response = getClient().get(id);
        return response.isSuccess() ? response.getData() : null;
    }

    public CustomApiResponse<T> save(T dto) {
        Boolean isUpdated = dto.getId() != null;

        ApiResponse<T> response = !isUpdated ? getClient().create(dto) : update(dto.getId(), dto);

        return CustomApiResponse.save(response, isUpdated, response.getData());
    }

    public ApiResponse<T> update(ID id, T dto) {
        return getClient().update(id, dto);
    }

    public ApiResponse<Void> delete(ID id) {
        return getClient().delete(id);
    }
}