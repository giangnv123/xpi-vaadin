package com.xpi.application.base;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class XPage<T> {
    private List<T> content;
    private int number;
    private int size;
    private int totalPages;
    private int totalElements;
    private boolean first;
    private boolean last;
    private int numberOfElements;
    private boolean empty;

    private List<Link> links; // Define a Link class to hold rel and href

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Link {
        private String rel;
        private String href;
    }
}
