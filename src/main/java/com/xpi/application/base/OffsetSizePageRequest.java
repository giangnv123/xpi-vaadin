package com.xpi.application.base;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

/**
 * @see http://stackoverflow.com/questions/30217552/spring-data-pageable-and-limit-offset
 *
 */
public class OffsetSizePageRequest implements Pageable {

    private final long offset;
    private final int pageSize;
    private final Sort sort;

    public OffsetSizePageRequest() {
        this.offset = 0;
        this.pageSize = 0;
        this.sort = null;
    }

    public OffsetSizePageRequest(long offset, int pageSize) {
        this(offset, pageSize, null);
    }

    public OffsetSizePageRequest(long offset, int pageSize, Sort sort) {
        this.offset = offset;
        this.pageSize = pageSize;
        this.sort = sort;
    }

    @Override
    public boolean isPaged() {
        if (this.pageSize == 0)
            return false;
        else
            return Pageable.super.isPaged();
    }

    @Override
    public int getPageNumber() {
        // this is the odd case, when the offset is > 0 but smaller than the page size.
        // That means that there actually is a previous page.
        if (getOffset() > 0 && getOffset() < getPageSize()) {
            return 1;
        }
        return (int) (getOffset() / getPageSize());
    }

    @Override
    public int getPageSize() {
        return pageSize;
    }

    @Override
    public long getOffset() {
        return offset;
    }

    @Override
    public Sort getSort() {
        return sort;
    }

    @Override
    public Pageable next() {
        return new OffsetSizePageRequest(getOffset() + getPageSize(), getPageSize(), getSort());
    }

    @Override
    public Pageable previousOrFirst() {
        if (getOffset() - getPageSize() <= 0) {
            return first();
        }
        return new OffsetSizePageRequest(getOffset() - getPageSize(), getPageSize(), getSort());
    }

    @Override
    public Pageable first() {
        if (getOffset() == 0) {
            return this;
        }
        return new OffsetSizePageRequest(0, getPageSize(), getSort());
    }

    @Override
    public boolean hasPrevious() {
        return getOffset() > 0;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = (int) (prime * result + offset);
        result = prime * result + pageSize;
        result = prime * result + (sort == null ? 0 : sort.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        OffsetSizePageRequest other = (OffsetSizePageRequest) obj;
        if (offset != other.offset) {
            return false;
        }
        if (pageSize != other.pageSize) {
            return false;
        }
        if (sort == null) {
            if (other.sort != null) {
                return false;
            }
        } else if (!sort.equals(other.sort)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "OffsetSizePageRequest [offset=" + offset + ", pageSize=" + pageSize + ", sort=" + sort + "]";
    }

    @Override
    public Pageable withPage(int pageNumber) {
        return null;
    }

}
