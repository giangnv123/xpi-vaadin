package com.xpi.application.base;

import java.time.LocalDate;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class AbstractDto<T> {
    private T id;
    private LocalDate dateCreated;
    private LocalDateTime dateUpdated;
}
