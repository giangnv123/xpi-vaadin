package com.xpi.application.views.layout;

import org.vaadin.lineawesome.LineAwesomeIcon;

import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.applayout.DrawerToggle;
import com.vaadin.flow.component.html.Footer;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.Header;
import com.vaadin.flow.component.orderedlayout.Scroller;
import com.vaadin.flow.component.sidenav.SideNav;
import com.vaadin.flow.component.sidenav.SideNavItem;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.theme.lumo.LumoUtility;
import com.xpi.application.views.HomeView;
import com.xpi.application.views.hrm.contracts.views.ContractGridView;
import com.xpi.application.views.hrm.dashboard.views.DashboardView;
import com.xpi.application.views.hrm.department.views.DepartmentGridView;
import com.xpi.application.views.hrm.employees.views.EmployeeGridView;
import com.xpi.application.views.hrm.holidays.views.HolidayGridView;
import com.xpi.application.views.hrm.insurances.views.InsuranceGridView;
import com.xpi.application.views.hrm.leaves.views.LeaveGridView;
import com.xpi.application.views.hrm.settings.views.SettingsView;
import com.xpi.application.views.hrm.workdays.views.WorkdayGridView;

public class HRMLayout extends AppLayout {

    private H2 viewTitle;

    public HRMLayout() {
        setPrimarySection(Section.DRAWER);
        addDrawerContent();
        addHeaderContent();
    }

    private void addHeaderContent() {
        DrawerToggle toggle = new DrawerToggle();
        toggle.setAriaLabel("Menu toggle");

        viewTitle = new H2();
        viewTitle.addClassNames(LumoUtility.FontSize.LARGE, LumoUtility.Margin.NONE);

        addToNavbar(true, toggle, viewTitle);
    }

    private void addDrawerContent() {
        H1 appName = new H1("Quản lý nhân viên");
        appName.addClassNames(LumoUtility.FontSize.LARGE, LumoUtility.Margin.NONE);
        Header header = new Header(appName);

        Scroller scroller = new Scroller(createNavigation());

        addToDrawer(header, scroller, createFooter());
    }

    private SideNav createNavigation() {
        SideNav nav = new SideNav();

        nav.addItem(createHomeNavItem());
        nav.addItem(createDashboardNavItem());
        nav.addItem(createEmployeeNavItem());
        nav.addItem(createDepartmentNavItem());
        nav.addItem(createContractNavItem());
        nav.addItem(createInsuranceNavItem());
        nav.addItem(createLeaveLetterNavItem());
        nav.addItem(createHolidaysNavItem());
        nav.addItem(createWorkdayNavItem());
        nav.addItem(createSettingsNavItem());

        return nav;
    }

    private SideNavItem createHomeNavItem() {
        return new SideNavItem("Home", HomeView.class, LineAwesomeIcon.HOME_SOLID.create());
    }

    private SideNavItem createEmployeeNavItem() {
        return new SideNavItem("Nhân viên", EmployeeGridView.class, LineAwesomeIcon.USERS_SOLID.create());
    }

    private SideNavItem createSettingsNavItem() {
        return new SideNavItem("Cài đặt", SettingsView.class, LineAwesomeIcon.COG_SOLID.create());
    }

    private SideNavItem createContractNavItem() {
        return new SideNavItem("Hợp đồng", ContractGridView.class, LineAwesomeIcon.FILE_CONTRACT_SOLID.create());
    }

    private SideNavItem createInsuranceNavItem() {
        return new SideNavItem("Bảo hiểm xã hội", InsuranceGridView.class, LineAwesomeIcon.FILE_SOLID.create());
    }

    private SideNavItem createLeaveLetterNavItem() {
        return new SideNavItem("Đơn nghỉ phép",
                LeaveGridView.class,
                LineAwesomeIcon.FILE.create());
    }

    private SideNavItem createDepartmentNavItem() {
        return new SideNavItem("Phòng ban", DepartmentGridView.class, LineAwesomeIcon.BUILDING_SOLID.create());
    }

    private SideNavItem createWorkdayNavItem() {
        return new SideNavItem("Ngày công", WorkdayGridView.class, LineAwesomeIcon.USER_CLOCK_SOLID.create());
    }

    private SideNavItem createDashboardNavItem() {
        return new SideNavItem("Trang chủ", DashboardView.class, LineAwesomeIcon.COMPASS_SOLID.create());
    }

    private SideNavItem createHolidaysNavItem() {
        return new SideNavItem("Kỳ nghỉ", HolidayGridView.class, LineAwesomeIcon.COCKTAIL_SOLID.create());
    }

    private Footer createFooter() {
        Footer layout = new Footer();
        return layout;
    }

    @Override
    protected void afterNavigation() {
        super.afterNavigation();
        viewTitle.setText(getCurrentPageTitle());
    }

    private String getCurrentPageTitle() {
        PageTitle title = getContent().getClass().getAnnotation(PageTitle.class);
        return title == null ? "" : title.value();
    }
}
