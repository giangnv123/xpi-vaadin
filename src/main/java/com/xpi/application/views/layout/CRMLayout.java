package com.xpi.application.views.layout;

import org.vaadin.lineawesome.LineAwesomeIcon;

import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.applayout.DrawerToggle;
import com.vaadin.flow.component.html.Footer;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.Header;
import com.vaadin.flow.component.orderedlayout.Scroller;
import com.vaadin.flow.component.sidenav.SideNav;
import com.vaadin.flow.component.sidenav.SideNavItem;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.theme.lumo.LumoUtility;
import com.xpi.application.module.crm.dashboards.views.CRMDashboardView;
import com.xpi.application.views.HomeView;
import com.xpi.application.views.crm.contact.views.ContactGridView;
import com.xpi.application.views.crm.interaction.views.InteractionGridView;

public class CRMLayout extends AppLayout {

    private H2 viewTitle;

    public CRMLayout() {
        setPrimarySection(Section.DRAWER);
        addDrawerContent();
        addHeaderContent();
    }

    private void addHeaderContent() {
        DrawerToggle toggle = new DrawerToggle();
        toggle.setAriaLabel("Menu toggle");

        viewTitle = new H2();
        viewTitle.addClassNames(LumoUtility.FontSize.LARGE, LumoUtility.Margin.NONE);

        addToNavbar(true, toggle, viewTitle);
    }

    private void addDrawerContent() {
        H1 appName = new H1("Quản lý khách hàng");
        appName.addClassNames(LumoUtility.FontSize.LARGE, LumoUtility.Margin.NONE);
        Header header = new Header(appName);

        Scroller scroller = new Scroller(createNavigation());

        addToDrawer(header, scroller, createFooter());
    }

    private SideNav createNavigation() {
        SideNav nav = new SideNav();

        nav.addItem(createHomeNavItem());
        nav.addItem(createDashboardItem());
        nav.addItem(createInteractionNavItem());
        nav.addItem(createContactNavItem());

        return nav;
    }

    private SideNavItem createHomeNavItem() {
        return new SideNavItem("Home", HomeView.class, LineAwesomeIcon.HOME_SOLID.create());
    }

    private SideNavItem createDashboardItem() {
        return new SideNavItem("Trang chủ", CRMDashboardView.class, LineAwesomeIcon.TACHOMETER_ALT_SOLID.create());
    }

    private SideNavItem createContactNavItem() {
        return new SideNavItem("Danh bạ", ContactGridView.class,
                LineAwesomeIcon.PHONE_ALT_SOLID.create());
    }

    private SideNavItem createInteractionNavItem() {
        return new SideNavItem("Tương tác", InteractionGridView.class,
                LineAwesomeIcon.HANDSHAKE_SOLID.create());
    }

    private Footer createFooter() {
        Footer layout = new Footer();
        return layout;
    }

    @Override
    protected void afterNavigation() {
        super.afterNavigation();
        viewTitle.setText(getCurrentPageTitle());
    }

    private String getCurrentPageTitle() {
        PageTitle title = getContent().getClass().getAnnotation(PageTitle.class);
        return title == null ? "" : title.value();
    }
}
