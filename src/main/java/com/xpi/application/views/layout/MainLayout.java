package com.xpi.application.views.layout;

import org.vaadin.lineawesome.LineAwesomeIcon;

import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.applayout.DrawerToggle;
import com.vaadin.flow.component.html.Footer;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.Header;
import com.vaadin.flow.component.orderedlayout.Scroller;
import com.vaadin.flow.component.sidenav.SideNav;
import com.vaadin.flow.component.sidenav.SideNavItem;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.theme.lumo.LumoUtility;
import com.xpi.application.module.crm.dashboards.views.CRMDashboardView;
import com.xpi.application.views.HomeView;
import com.xpi.application.views.hrm.dashboard.views.DashboardView;

public class MainLayout extends AppLayout {

    private H2 viewTitle;

    public MainLayout() {
        setPrimarySection(Section.DRAWER);
        addDrawerContent();
        addHeaderContent();
    }

    private void addHeaderContent() {
        DrawerToggle toggle = new DrawerToggle();
        toggle.setAriaLabel("Menu toggle");

        viewTitle = new H2();
        viewTitle.addClassNames(LumoUtility.FontSize.LARGE, LumoUtility.Margin.NONE);

        addToNavbar(true, toggle, viewTitle);
    }

    private void addDrawerContent() {
        H1 appName = new H1("XPI");
        appName.addClassNames(LumoUtility.FontSize.LARGE, LumoUtility.Margin.NONE);
        Header header = new Header(appName);

        Scroller scroller = new Scroller(createNavigation());

        addToDrawer(header, scroller, createFooter());
    }

    private SideNav createNavigation() {
        SideNav nav = new SideNav();

        nav.addItem(createHomeNavItem());
        nav.addItem(createCrmNavItem());
        nav.addItem(createDashboardNavItem());

        return nav;
    }

    private SideNavItem createHomeNavItem() {
        return new SideNavItem("Home", HomeView.class, LineAwesomeIcon.HOME_SOLID.create());
    }

    private SideNavItem createCrmNavItem() {
        return new SideNavItem("CRM", CRMDashboardView.class, LineAwesomeIcon.TACHOMETER_ALT_SOLID.create());
    }

    private SideNavItem createDashboardNavItem() {
        return new SideNavItem("HRM", DashboardView.class, LineAwesomeIcon.COMPASS_SOLID.create());
    }

    private Footer createFooter() {
        Footer layout = new Footer();
        return layout;
    }

    @Override
    protected void afterNavigation() {
        super.afterNavigation();
        viewTitle.setText(getCurrentPageTitle());
    }

    private String getCurrentPageTitle() {
        PageTitle title = getContent().getClass().getAnnotation(PageTitle.class);
        return title == null ? "" : title.value();
    }
}
