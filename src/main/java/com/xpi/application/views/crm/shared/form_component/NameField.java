package com.xpi.application.views.crm.shared.form_component;

import org.vaadin.lineawesome.LineAwesomeIcon;

public class NameField extends BaseTextField {
    public NameField() {
        this(null);
    }

    public NameField(String label) {
        super(label);
    }

    public NameField(String label, String value) {
        super(label, value);
    }

    protected void init() {
        setPrefixComponent(LineAwesomeIcon.USER_SOLID.create());
    }
}
