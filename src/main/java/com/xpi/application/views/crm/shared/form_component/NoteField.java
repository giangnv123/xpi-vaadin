package com.xpi.application.views.crm.shared.form_component;

import org.vaadin.lineawesome.LineAwesomeIcon;

public class NoteField extends BaseTextAreaField {

    public NoteField() {
        this(null, null);
    }

    public NoteField(String label) {
        this(label, null);
    }

    public NoteField(String label, String value) {
        super(label, value);
    }

    @Override
    protected void init() {
        setPlaceholder("Ghi chú...");
        setClearButtonVisible(true);
        setAutoselect(true);
        setPrefixComponent(LineAwesomeIcon.NOTES_MEDICAL_SOLID.create());
    }
}
