package com.xpi.application.views.crm.shared;

import org.vaadin.lineawesome.LineAwesomeIcon;

import com.vaadin.flow.component.icon.SvgIcon;
import com.xpi.application.module.crm.interactions.shared.InteractionType;

public class CRMViewHelper {

    public static SvgIcon getTypeIcon(InteractionType type) {
        switch (type) {
            case CALL:
                return LineAwesomeIcon.PHONE_ALT_SOLID.create();
            case EMAIL:
                return LineAwesomeIcon.ENVELOPE.create();
            case MEETING:
                return LineAwesomeIcon.HANDSHAKE_SOLID.create();
            case CHAT:
                return LineAwesomeIcon.COMMENTS.create();
            default:
                return LineAwesomeIcon.QUESTION_CIRCLE.create();
        }
    }

}
