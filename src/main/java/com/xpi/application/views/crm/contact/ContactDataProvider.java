package com.xpi.application.views.crm.contact;

import java.util.stream.Stream;

import com.vaadin.flow.data.provider.AbstractBackEndDataProvider;
import com.vaadin.flow.data.provider.Query;
import com.xpi.application.base.OffsetSizePageRequest;
import com.xpi.application.module.crm.contacts.ContactService;
import com.xpi.application.module.crm.contacts.shared.ContactFilter;
import com.xpi.application.module.crm.dto.ContactDto;

public class ContactDataProvider
        extends AbstractBackEndDataProvider<ContactDto, ContactFilter> {

    private static final long serialVersionUID = -2772672175400690881L;

    private ContactService service;

    public ContactDataProvider(ContactService service) {
        super();
        this.service = service;
    }

    @Override
    protected Stream<ContactDto> fetchFromBackEnd(Query<ContactDto, ContactFilter> query) {

        OffsetSizePageRequest pageRequest = new OffsetSizePageRequest(query.getOffset(), query.getLimit());

        return service.getAll(pageRequest, query.getFilter().orElseGet(ContactFilter::defaultFilter)).stream();
    }

    @Override
    protected int sizeInBackEnd(Query<ContactDto, ContactFilter> query) {
        return service.count(query.getFilter().orElseGet(ContactFilter::defaultFilter));
    }
}
