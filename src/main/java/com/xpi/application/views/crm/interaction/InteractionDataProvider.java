package com.xpi.application.views.crm.interaction;

import java.util.stream.Stream;

import com.vaadin.flow.data.provider.AbstractBackEndDataProvider;
import com.vaadin.flow.data.provider.Query;
import com.xpi.application.base.OffsetSizePageRequest;
import com.xpi.application.module.crm.dto.InteractionDto;
import com.xpi.application.module.crm.interactions.InteractionService;
import com.xpi.application.module.crm.interactions.shared.InteractionFilter;

public class InteractionDataProvider
        extends AbstractBackEndDataProvider<InteractionDto, InteractionFilter> {

    private static final long serialVersionUID = -2772672175400690881L;

    private InteractionService service;

    public InteractionDataProvider(InteractionService service) {
        super();
        this.service = service;
    }

    @Override
    protected Stream<InteractionDto> fetchFromBackEnd(Query<InteractionDto, InteractionFilter> query) {

        OffsetSizePageRequest pageRequest = new OffsetSizePageRequest(query.getOffset(), query.getLimit());

        return service.getAll(pageRequest, query.getFilter().orElse(null)).stream();

    }

    @Override
    protected int sizeInBackEnd(Query<InteractionDto, InteractionFilter> query) {
        return service.count(query.getFilter().orElseGet(InteractionFilter::defaultFilter));
    }
}
