package com.xpi.application.views.crm.shared.components.topic;

import org.vaadin.lineawesome.LineAwesomeIcon;

import com.vaadin.flow.component.textfield.TextField;
import com.xpi.application.module.crm.dto.subs.TopicDto;
import com.xpi.application.views.base.abs.AbstractForm;
import com.xpi.application.views.base.component.XFormLayout;

public class TopicForm extends AbstractForm<TopicDto> {
    /* Declare components - but do not initialize */
    private TextField nameField;

    public TopicForm() {
        super(TopicDto.class);
        initializeForm();

    }

    protected void preBuild() {
        XFormLayout formLayout = new XFormLayout();

        nameField = new TextField("Tên chủ đề");
        nameField.setPrefixComponent(LineAwesomeIcon.HEADING_SOLID.create());

        formLayout.add(nameField);

        add(formLayout);
    }

    @Override
    protected void setupBinder() {
        binder.forField(nameField)
                .asRequired("Vui lòng nhập tên chủ đề")
                .bind(TopicDto::getName, TopicDto::setName);

    }
}
