package com.xpi.application.views.crm.shared.form_component;

import org.vaadin.lineawesome.LineAwesomeIcon;

public class WebsiteField extends BaseTextField {

    public WebsiteField() {
        this(null);
    }

    public WebsiteField(String label) {
        super(label);
    }

    public WebsiteField(String label, String value) {
        super(label, value);
    }

    @Override
    protected void init() {
        setPrefixComponent(LineAwesomeIcon.GLOBE_SOLID.create());
        setClearButtonVisible(true);
        setPlaceholder("Website");
    }
}
