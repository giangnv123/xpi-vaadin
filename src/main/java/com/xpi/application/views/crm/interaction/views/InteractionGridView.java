package com.xpi.application.views.crm.interaction.views;

import java.util.List;
import java.util.Set;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.xpi.application.module.crm.dto.ContactDto;
import com.xpi.application.module.crm.dto.InteractionDto;
import com.xpi.application.module.crm.dto.subs.SourceDto;
import com.xpi.application.module.crm.dto.subs.TagDto;
import com.xpi.application.module.crm.dto.subs.TopicDto;
import com.xpi.application.views.base.abs.AbstractBasicGridView;
import com.xpi.application.views.base.component.XConfirmDialog;
import com.xpi.application.views.base.utils.CustomApiResponse;

import com.xpi.application.views.crm.interaction.InteractionPresenter;
import com.xpi.application.views.crm.interaction.component.card.InteractionCard;
import com.xpi.application.views.crm.interaction.component.interaction.InteractionDetailsDialog;
import com.xpi.application.views.crm.interaction.component.interaction.InteractionFilterContainer;
import com.xpi.application.views.crm.interaction.component.interaction.InteractionForm;
import com.xpi.application.views.crm.interaction.component.interaction.InteractionGallery;
import com.xpi.application.views.crm.shared.components.InteractionDialog;
import com.xpi.application.views.crm.shared.components.source.SourceSelectField;
import com.xpi.application.views.crm.shared.components.tag.TagSelectField;
import com.xpi.application.views.crm.shared.components.topic.TopicSelectField;
import com.xpi.application.views.layout.CRMLayout;

@PageTitle("Tương tác")
@Route(value = "crm/interactions", layout = CRMLayout.class)
public class InteractionGridView extends AbstractBasicGridView<InteractionDto, String> {

    private InteractionPresenter presenter;

    private InteractionGallery interactionGallery;

    private InteractionDetailsDialog interactionDetailsDialog;

    private ContactDto fetchContact;

    private InteractionCard selectedInteractionCard;

    /* Components */
    private InteractionFilterContainer filterContainer;
    private InteractionDialog interactionDialog;

    private Set<TagDto> cacheTags;

    private TagSelectField tagSelectField;
    private TopicSelectField topicSelectField;
    private SourceSelectField sourceSelectField;

    public InteractionGridView(InteractionPresenter presenter) {
        this.entityName = "Tương tác";
        this.presenter = presenter;
        this.presenter.initView(this);
        addClassName("interaction");
        initilizeSelectField();
        initialSetup();
    }

    private void initilizeSelectField() {
        setupTagSelectField();
        setupTopicSelectField();
        setupSourceSelectField();
    }

    @Override
    protected Div createHeaderSection() {
        this.filterContainer = new InteractionFilterContainer(presenter::refreshGrid, cacheTags);
        Div header = new Div(filterContainer, createAddEntityButton());
        header.addClassName("header");
        return header;
    }


    @Override
    protected Component createContentSection() {

        interactionGallery = new InteractionGallery(this::handleClickDelete,
                this::handleClickEdit,
                this::handleCardSaveInteraction, this::fetchNewTags, this::saveTag);
        refreshGallery(presenter.getAll());
        return interactionGallery;
    }

    /* Handle Topic */
    private void setupTopicSelectField() {
        Set<TopicDto> cacheTopics = presenter.getAlltopics();
        topicSelectField = new TopicSelectField(this::fetchNewTopics, this::saveTopic, cacheTopics);
    }

    private Set<TopicDto> fetchNewTopics(Void v) {
        return presenter.getAlltopics();
    }

    protected CustomApiResponse<TopicDto> saveTopic(TopicDto dto) {
        return presenter.savetopic(dto);
    }

    private void setupSourceSelectField() {
        Set<SourceDto> cacheSources = presenter.getAllSources();
        sourceSelectField = new SourceSelectField(this::fetchNewSources, this::saveSource, cacheSources);
    }

    private Set<SourceDto> fetchNewSources(Void v) {
        return presenter.getAllSources();
    }

    protected CustomApiResponse<SourceDto> saveSource(SourceDto dto) {
        return presenter.saveSource(dto);
    }

    /* Handle tag */

    private void setupTagSelectField() {
        intitialTagcache();
        tagSelectField = new TagSelectField(this::fetchNewTags, this::saveTag, cacheTags);
    }

    private void intitialTagcache() {
        cacheTags = presenter.getAllTags();
    }

    private Set<TagDto> fetchNewTags(Void v) {
        return presenter.getAllTags();
    }

    protected CustomApiResponse<TagDto> saveTag(TagDto dto) {
        return presenter.saveTag(dto);
    }

    /* Handle contact */

    public CustomApiResponse<ContactDto> handleSaveContact(ContactDto contactDto) {
        CustomApiResponse<ContactDto> response = presenter.saveContact(contactDto);
        showMessage(response.getMessage(), response.isSuccess());
        return response;
    }

    /* Handle UI */

    public void refreshGallery(List<InteractionDto> newFetchedInteractions) {
        interactionGallery.refreshGallery(newFetchedInteractions);
    }

    protected void setupDialog() {
        setupControlDialog();
        deleteDialog = new XConfirmDialog(entityName, this::performDelete);
    }

    protected void setupControlDialog() {
        interactionDetailsDialog = new InteractionDetailsDialog(this::handleSaveContact);
    }

    protected void performDelete() {
        CustomApiResponse<Void> response = presenter.delete(currentId);

        Boolean isSuccess = response.isSuccess();

        if (isSuccess)
            interactionGallery.removeInteractionCard(currentId);

        showMessage(response.getMessage(), isSuccess);
    }

    protected void handleClickEdit(InteractionDto dto) {
        /* set select interaction */

        /* fetch target ContactBy contactId */
        CustomApiResponse<ContactDto> response = presenter.getContactById(dto.getContactId());

        if (!response.isSuccess())
            return;

        fetchContact = response.getDto();

        /* reset card */
        selectedInteractionCard = interactionGallery.getCardById(dto.getId());

        interactionDetailsDialog.refreshDialog(selectedInteractionCard,
                fetchContact);

        interactionDetailsDialog.open();
    }

    protected void handleClickDelete(InteractionDto dto) {
        currentId = dto.getId();
        deleteDialog.open();
    }

    protected void handleClickAdd() {
        if (interactionDialog == null)
            setupInteractionDialog();

        interactionDialog.open();
        InteractionForm form = interactionDialog.getTargetForm();
        form.resetForm();
        form.setEntity(new InteractionDto());
    }

    private void setupInteractionDialog() {
        interactionDialog = new InteractionDialog(
                this::handleCardSaveInteraction,
                presenter.getAllContacts(),
                tagSelectField,
                topicSelectField,
                sourceSelectField);
    }

    private void handleCardSaveInteraction(InteractionDto interactionDto) {
        CustomApiResponse<InteractionDto> response = presenter.saveInteraction(interactionDto);

        Boolean isSuccess = response.isSuccess();

        if (isSuccess)
            interactionGallery.addInteractionCard(response.getDto());

        showMessage(response.getMessage(), isSuccess);
    }


}
