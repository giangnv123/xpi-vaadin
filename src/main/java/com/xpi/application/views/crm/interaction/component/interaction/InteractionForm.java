package com.xpi.application.views.crm.interaction.component.interaction;

import java.time.Duration;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.vaadin.lineawesome.LineAwesomeIcon;

import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.combobox.ComboBox.ItemFilter;
import com.vaadin.flow.component.datetimepicker.DateTimePicker;
import com.vaadin.flow.component.details.Details;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.SvgIcon;
import com.vaadin.flow.component.tabs.TabSheet;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.xpi.application.module.crm.dto.ContactDto;
import com.xpi.application.module.crm.dto.InteractionDto;
import com.xpi.application.module.crm.interactions.shared.InteractionType;
import com.xpi.application.views.base.abs.AbstractForm;
import com.xpi.application.views.base.component.XFormLayout;
import com.xpi.application.views.base.utils.ColorUtils;
import com.xpi.application.views.base.views.ViewHelper;
import com.xpi.application.views.crm.shared.components.SimpleContactForm;
import com.xpi.application.views.crm.shared.components.XFlexLayout;
import com.xpi.application.views.crm.shared.components.source.SourceSelectField;
import com.xpi.application.views.crm.shared.components.tag.TagSelectField;
import com.xpi.application.views.crm.shared.components.topic.TopicSelectField;
import com.xpi.application.views.crm.shared.form_component.NoteField;

public class InteractionForm extends AbstractForm<InteractionDto> {

        private boolean isAddingNewContact;

        private static final String ADD_CONTACT = "Thêm lên hệ";
        private static final String SELECT_CONTACT = "Chọn liên hệ có sẵn";

        private List<ContactDto> contacts;

        /* Save tag */

        private TagSelectField tagSelectField;

        /* Declare components - but do not initialize */

        private SimpleContactForm contactForm;

        private ComboBox<ContactDto> contactsField;

        private ComboBox<InteractionType> interactionTypeField;

        private TextField subjectField;
        private DateTimePicker dateTimePicker;
        private IntegerField durationField;
        private NoteField outcomeField;
        private NoteField notesField;

        private TopicSelectField topicSelectField;
        private SourceSelectField sourceSelectField;

        public InteractionForm(List<ContactDto> contacts,
                        TagSelectField tagSelectField,
                        TopicSelectField topicSelectField,
                        SourceSelectField sourceSelectField) {
                super(InteractionDto.class);
                this.tagSelectField = tagSelectField;
                this.topicSelectField = topicSelectField;
                this.sourceSelectField = sourceSelectField;
                this.contacts = contacts;

                initializeForm();
        }

        protected void preBuild() {

                contactForm = new SimpleContactForm(sourceSelectField);

                contactsField = createSelectContactField();

                subjectField = new TextField();
                subjectField.setLabel("Tiêu đề");
                subjectField.setPrefixComponent(LineAwesomeIcon.HEADING_SOLID.create());

                dateTimePicker = new DateTimePicker();
                dateTimePicker.setLabel("Thời gian");
                dateTimePicker.setStep(Duration.ofMinutes(30));

                /* Interaction Type */
                interactionTypeField = new ComboBox<>();
                interactionTypeField.setLabel("Hình thức");
                interactionTypeField.setPrefixComponent(LineAwesomeIcon.HANDSHAKE.create());
                interactionTypeField.setItems(InteractionType.values());
                interactionTypeField.setItemLabelGenerator(item -> item.toReadableString());

                /* Add details component */

                Details detailsDiv = createDetails();

                TabSheet tabSheet = createTabSheet();

                XFormLayout formLayout = new XFormLayout(topicSelectField, subjectField, dateTimePicker,
                                interactionTypeField,
                                tagSelectField);

                formLayout.setColspan(notesField, 2);
                formLayout.setColspan(outcomeField, 2);
                formLayout.setColspan(topicSelectField, 2);
                formLayout.setColspan(detailsDiv, 2);

                add(tabSheet);
                add(formLayout);
                add(detailsDiv);
        }

        private Details createDetails() {
                notesField = new NoteField("Ghi chú");

                durationField = new IntegerField();
                durationField.setLabel("Thời lượng (phút)");
                durationField.setPrefixComponent(LineAwesomeIcon.CLOCK_SOLID.create());

                outcomeField = new NoteField("Kết quả");

                Div detailsContainer = new Div(durationField, outcomeField, notesField);
                detailsContainer.addClassName("flex-col-layout");

                Details detailsDiv = new Details("Chi tiết");
                detailsDiv.setOpened(false);
                detailsDiv.add(detailsContainer);

                return detailsDiv;
        }

        private TabSheet createTabSheet() {
                TabSheet tabSheet = new TabSheet();

                tabSheet.addSelectedChangeListener(event -> {
                        isAddingNewContact = tabSheet.getSelectedTab().getLabel().equals(ADD_CONTACT);
                });

                tabSheet.add(ADD_CONTACT, contactForm);
                tabSheet.add(SELECT_CONTACT, contactsField);
                return tabSheet;
        }

        private ComboBox<ContactDto> createSelectContactField() {
                contactsField = new ComboBox<>("Chọn liên hệ");
                contactsField.setPrefixComponent(LineAwesomeIcon.USER_SOLID.create());
                contactsField.setWidthFull();
                contactsField.setItemLabelGenerator(contact -> contact.getName() + " - " + contact.getPhone());

                ItemFilter<ContactDto> filter = createContactFilter();
                contactsField.setItems(filter, Optional.ofNullable(contacts).orElse(List.of()));
                contactsField.setRenderer(new ComponentRenderer<>(contact -> {
                        return contactRender(contact);
                }));

                return contactsField;
        }

        private Div contactRender(ContactDto contact) {
                SvgIcon userIcon = ViewHelper.createIcon(LineAwesomeIcon.USER, ColorUtils.GRAY, null, "25px");
                Span nameSpan = new Span(contact.getName());
                XFlexLayout nameRender = new XFlexLayout(userIcon, nameSpan);

                SvgIcon phoneIcon = ViewHelper.createIcon(LineAwesomeIcon.PHONE_SOLID, ColorUtils.GRAY, null,
                                "25px");
                Span phoneValue = new Span(contact.getPhone());
                XFlexLayout phoneRender = new XFlexLayout(phoneIcon, phoneValue);

                return new Div(nameRender, phoneRender);
        }

        private ItemFilter<ContactDto> createContactFilter() {
                ItemFilter<ContactDto> filter = (contact,
                                filterString) -> (contact.getName() + " "
                                                + contact.getEmail() + " " + contact.getPhone())
                                                .toLowerCase().indexOf(filterString.toLowerCase()) > -1;
                return filter;
        }

        @Override
        protected void setupBinder() {
                binder.forField(subjectField).asRequired("Vui lòng nhập tiêu đề")
                                .bind(InteractionDto::getSubject, InteractionDto::setSubject);

                binder.forField(dateTimePicker).asRequired("Vui lòng nhập thời gian")
                                .bind(InteractionDto::getDateTime, InteractionDto::setDateTime);

                binder.forField(interactionTypeField)
                                .asRequired("Vui lòng chọn hình thức")
                                .bind(InteractionDto::getType, InteractionDto::setType);

                binder.forField(durationField).bind(InteractionDto::getDuration, InteractionDto::setDuration);
                binder.forField(outcomeField).bind(InteractionDto::getOutcome, InteractionDto::setOutcome);
                binder.forField(notesField).bind(InteractionDto::getNotes, InteractionDto::setNotes);
        }

        public String getContactId() {
                return Optional.ofNullable(contactsField.getValue()).map(ContactDto::getId).orElse(null);
        }

        public Set<Long> getTagIds() {
                return tagSelectField.getTagIds();
        }

        public Long getTopicId() {
                return topicSelectField.getTopicId();
        }

        public Long getSourceId() {
                return sourceSelectField.getSourceId();
        }

        public Boolean isAddingNewContact() {
                return isAddingNewContact;
        }

        public SimpleContactForm getContactForm() {
                return contactForm;
        }
}
