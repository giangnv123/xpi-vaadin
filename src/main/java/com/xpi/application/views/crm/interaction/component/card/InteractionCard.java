package com.xpi.application.views.crm.interaction.component.card;

import java.util.function.Consumer;

import com.vaadin.flow.component.html.Div;
import com.xpi.application.module.crm.dto.InteractionDto;
import com.xpi.application.views.crm.shared.components.tag.TagSelectField;

public class InteractionCard extends Div {

    private InteractionDto dto;
    private CardHeader cardHeader;
    private CardBody cardBody;
    private CardFooter cardFooter;

    private Consumer<InteractionDto> handleSave;
    private Consumer<InteractionDto> handleClickDelete;
    private Consumer<InteractionDto> handleClickEdit;
    private TagSelectField tagSelectField;

    public InteractionCard(InteractionDto dto,
            Consumer<InteractionDto> handleClickDelete,
            Consumer<InteractionDto> handleClickEdit,
            Consumer<InteractionDto> handleSave,
            TagSelectField tagSelectField) {
        this.dto = dto;
        this.handleClickDelete = handleClickDelete;
        this.handleClickEdit = handleClickEdit;
        this.handleSave = handleSave;
        this.tagSelectField = tagSelectField;
        tagSelectField.setValues(dto.getTags());

        addClassName("interaction-card");
        addClassName("card");
        setupComponent();
    }

    public void setupComponent() {
        cardHeader = new CardHeader(dto, handleClickDelete, handleClickEdit);
        cardBody = new CardBody(dto, handleSave, tagSelectField);
        cardFooter = new CardFooter(dto, handleSave);

        add(cardHeader);
        add(cardBody);
        add(cardFooter);
    }

    public void updateInteraction(InteractionDto updatedDto) {
        this.dto = updatedDto;
        removeAll();
        setupComponent();
    }

    public InteractionCard cloneCard() {
        return new InteractionCard(dto, handleClickDelete, handleClickEdit, handleSave, tagSelectField);
    }
}
