package com.xpi.application.views.crm.contact;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.vaadin.flow.data.provider.ConfigurableFilterDataProvider;
import com.xpi.application.base.ApiResponse;
import com.xpi.application.module.crm.contacts.ContactService;
import com.xpi.application.module.crm.contacts.shared.ContactFilter;
import com.xpi.application.module.crm.contacts.shared.ContactStatus;
import com.xpi.application.module.crm.dto.ContactDto;
import com.xpi.application.module.crm.dto.subs.ContactSummaryDto;
import com.xpi.application.module.crm.shared.CRMHelperService;
import com.xpi.application.views.base.utils.CustomApiResponse;
import com.xpi.application.views.crm.contact.views.ContactGridView;

@Component
public class ContactPresenter {

    @Autowired
    private CRMHelperService helperService;

    private ConfigurableFilterDataProvider<ContactDto, Void, ContactFilter> filterDataProvider;

    private ContactDataProvider dataProvider;

    private ContactService service;

    private ContactGridView view;

    private ContactFilter initialFilter = ContactFilter.defaultFilter();

    /* Basic View */
    public ContactPresenter(ContactService service) {
        this.service = service;
        this.dataProvider = new ContactDataProvider(service);
        filterDataProvider = dataProvider.withConfigurableFilter();
    }

    public void initView(ContactGridView view) {
        this.view = view;
        filterDataProvider.setFilter(initialFilter);
        this.view.getGrid().setItems(filterDataProvider);
    }

    public void refreshGrid(ContactFilter filter) {
        filterDataProvider.setFilter(filter);
        updateView();
    }

    private void updateView() {
        if (this.view != null)
            this.view.getGrid().setItems(filterDataProvider);
    }

    public CustomApiResponse<Void> delete(ContactDto dto) {
        ApiResponse<Void> response = service.delete(dto.getId());
        if (response.isSuccess()) {
            updateView();
        }
        return CustomApiResponse.delete(response);
    }

    public CustomApiResponse<ContactDto> save(ContactDto dto) {
        CustomApiResponse<ContactDto> response = service.save(dto);

        if (dto.getFutureInteraction() != null) {
            dto.setStatus(ContactStatus.PENDING);
        }

        if (response.isSuccess()) {
            updateView();
        }
        return response;
    }

    public List<ContactSummaryDto> getContactSummary() {
        return helperService.getContactSummary();
    }

    public CustomApiResponse<ContactDto> changeContactCategory(ContactDto dto) {
        return service.save(dto);
    }
}
