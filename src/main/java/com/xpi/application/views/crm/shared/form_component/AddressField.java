package com.xpi.application.views.crm.shared.form_component;

import org.vaadin.lineawesome.LineAwesomeIcon;

public class AddressField extends BaseTextAreaField {

    public AddressField() {
        this(null);
    }

    public AddressField(String label) {
        super(label);
    }

    public AddressField(String label, String value) {
        super(label, value);
    }

    protected void init() {
        setClearButtonVisible(true);
        setAutoselect(true);
        setPrefixComponent(LineAwesomeIcon.MAP_MARKER_SOLID.create());
    }
}
