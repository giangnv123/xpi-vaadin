package com.xpi.application.views.crm.interaction.component.interaction;

import java.util.function.Function;

import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H3;
import com.xpi.application.module.crm.dto.ContactDto;
import com.xpi.application.views.base.utils.CustomApiResponse;
import com.xpi.application.views.crm.interaction.component.card.InteractionCard;

public class InteractionDetailsDialog extends Dialog {

    private Function<ContactDto, CustomApiResponse<ContactDto>> saveContact;

    /* Components */
    private InteractionCard interactionCard;
    private Div mainInteractionDiv;

    private Div contactInforDiv;

    private final String DEFAULT_WIDTH = "1000px";

    public InteractionDetailsDialog(Function<ContactDto, CustomApiResponse<ContactDto>> saveContact) {
        this.saveContact = saveContact;
        setupUI();
    }

    public void setupUI() {
        setWidth(DEFAULT_WIDTH);
        setDraggable(true);
        setResizable(true);

        add(createInteractionDialogContainer());
    }

    private Div createInteractionDialogContainer() {
        contactInforDiv = createContactInforDiv();

        mainInteractionDiv = createMainInteractionDiv();

        Div container = new Div(mainInteractionDiv, contactInforDiv);
        container.addClassName("interaction-dialog-container");
        return container;
    }

    private Div createContactInforDiv() {
        Div container = new Div();
        container.addClassNames("contact-infor");
        return container;
    }

    private Div createMainInteractionDiv() {
        Div container = new Div();
        container.addClassName("main-interaction");
        return container;
    }

    public void refreshDialog(InteractionCard newCard, ContactDto newContact) {
        resetCard(newCard.cloneCard());
        refreshContactInforDiv(newContact);
    }

    public void resetCard(InteractionCard newCard) {
        this.interactionCard = newCard;
        refreshMainInteractionDiv();
    }

    private void refreshMainInteractionDiv() {
        mainInteractionDiv.removeAll();
        H3 title = new H3("Thông tin tương tác");
        mainInteractionDiv.add(title, interactionCard);
    }

    private void refreshContactInforDiv(ContactDto dto) {
        contactInforDiv.removeAll();
        H3 title = new H3("Thông tin liên lạc");

        ContactCard contactCard = new ContactCard(dto, this::handleSaveContact);

        contactCard.setHeightFull();

        contactInforDiv.add(title, contactCard);
    }

    private void handleSaveContact(ContactDto newContact) {
        CustomApiResponse<ContactDto> response = saveContact.apply(newContact);

        Boolean isSuccess = response.isSuccess();
        if (isSuccess)
            refreshContactInforDiv(response.getDto());

    }

}
