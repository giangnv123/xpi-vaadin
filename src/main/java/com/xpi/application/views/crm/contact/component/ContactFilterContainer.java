package com.xpi.application.views.crm.contact.component;

import java.util.function.Consumer;

import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.select.Select;
import com.xpi.application.module.crm.contacts.shared.ContactFilter;
import com.xpi.application.module.crm.contacts.shared.ContactStatus;
import com.xpi.application.module.crm.contacts.shared.ContactCategory;
import com.xpi.application.views.base.abs.AbstractFilterContainer;

public class ContactFilterContainer extends AbstractFilterContainer<ContactFilter> {
    private Select<ContactCategory> typeSelect;

    private Select<ContactStatus> statusSelect;

    public ContactFilterContainer(Consumer<ContactFilter> applyFilter) {
        super(applyFilter);
        buildLayout();
    }

    @Override
    protected HorizontalLayout createFilterComponent() {
        HorizontalLayout filterComponents = new HorizontalLayout();

        typeSelect = createSelectTypeSelect();
        statusSelect = createStatusSelect();

        filterComponents.add(searchField, typeSelect, statusSelect);
        return filterComponents;
    }

    private Select<ContactStatus> createStatusSelect() {
        statusSelect = new Select<>();
        statusSelect.setItems(ContactStatus.values());
        statusSelect.setPlaceholder("Trạng thái");
        statusSelect.addValueChangeListener(event -> applyFilters());
        statusSelect.setItemLabelGenerator(ContactStatus::toReadableString);
        return statusSelect;
    }

    private Select<ContactCategory> createSelectTypeSelect() {
        typeSelect = new Select<>();
        typeSelect.setItems(ContactCategory.values());
        typeSelect.setPlaceholder("Phân loại");
        typeSelect.addValueChangeListener(event -> applyFilters());
        typeSelect.setItemLabelGenerator(ContactCategory::toReadableString);
        return typeSelect;
    }

    @Override
    protected void clearFilterField() {
        searchField.clear();
        typeSelect.clear();
        statusSelect.clear();
        ;
    }

    @Override
    protected ContactFilter getFilterValue() {
        return new ContactFilter(searchField.getValue(), typeSelect.getValue(), statusSelect.getValue());
    }

    @Override
    protected ContactFilter getDefaultFilter() {
        return ContactFilter.defaultFilter();
    }
}
