package com.xpi.application.views.crm.shared.components.tag;

import org.vaadin.lineawesome.LineAwesomeIcon;

import com.vaadin.flow.component.textfield.TextField;
import com.xpi.application.module.crm.dto.subs.TagDto;
import com.xpi.application.views.base.abs.AbstractForm;
import com.xpi.application.views.base.component.XFormLayout;

public class TagForm extends AbstractForm<TagDto> {
    /* Declare components - but do not initialize */
    private TextField nameField;

    public TagForm() {
        super(TagDto.class);
        initializeForm();

    }

    protected void preBuild() {
        XFormLayout formLayout = new XFormLayout();

        nameField = new TextField("Tên tag");
        nameField.setPrefixComponent(LineAwesomeIcon.TAG_SOLID.create());

        formLayout.add(nameField);

        add(formLayout);
    }

    @Override
    protected void setupBinder() {
        binder.forField(nameField)
                .asRequired("Vui lòng nhập tên tag")
                .bind(TagDto::getName, TagDto::setName);

    }
}
