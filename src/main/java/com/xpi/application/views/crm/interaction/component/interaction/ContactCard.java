package com.xpi.application.views.crm.interaction.component.interaction;

import java.util.function.Consumer;

import org.vaadin.lineawesome.LineAwesomeIcon;

import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.SvgIcon;
import com.xpi.application.module.crm.dto.ContactDto;
import com.xpi.application.views.base.utils.ColorUtils;
import com.xpi.application.views.base.views.EditComponent;
import com.xpi.application.views.base.views.ViewHelper;
import com.xpi.application.views.crm.shared.components.XFlexLayout;
import com.xpi.application.views.crm.shared.form_component.AddressField;
import com.xpi.application.views.crm.shared.form_component.BaseTextField;
import com.xpi.application.views.crm.shared.form_component.NameField;
import com.xpi.application.views.crm.shared.form_component.NoteField;
import com.xpi.application.views.crm.shared.form_component.PhoneField;
import com.xpi.application.views.crm.shared.form_component.WebsiteField;
import com.xpi.application.views.crm.shared.form_component.XEmailField;

public class ContactCard extends Div {

    private final String ICON_SIZE = "25px";
    private final String COLOR = ColorUtils.GRAY_DARK;

    private Consumer<ContactDto> saveContact;

    private ContactDto dto;

    public ContactCard(ContactDto dto, Consumer<ContactDto> saveContact) {
        this.dto = dto;
        this.saveContact = saveContact;
        setClassName("contact-infor");
        createContactInforDiv(dto);
    }

    private void createContactInforDiv(ContactDto dto) {
        Div contactNameDiv = createNameDiv();
        Div contactWebsiteDiv = createWebsiteDiv();
        Div contactPhoneDiv = createPhoneDiv();
        Div contactEmailDiv = createEmailDiv();
        Div contactNoteDiv = createNoteDiv();
        Div contactAddressDiv = createAddressDiv();

        addClassNames("card");

        add(contactNameDiv, contactWebsiteDiv, contactPhoneDiv, contactEmailDiv, contactNoteDiv, contactAddressDiv);
    }

    private Div createNameDiv() {
        NameField nameField = new NameField(null, dto.getName());

        test(nameField);
        XFlexLayout details = createDetailsDiv(dto.getName(), "Tên liên lạc", LineAwesomeIcon.USER);

        Runnable onSave = () -> {
            if (nameField.getValue() == null) {
                return;
            }
            dto.setName(nameField.getValue());
            saveContact.accept(dto);
        };

        return EditComponent.createEditableDiv(details, nameField, onSave);
    }

    private void test(BaseTextField field) {
        field.setValue(COLOR);
    }

    private Div createWebsiteDiv() {
        WebsiteField websiteField = new WebsiteField(null, dto.getWebsite());

        XFlexLayout details = createDetailsDiv(dto.getWebsite(), "Website", LineAwesomeIcon.GLOBE_ASIA_SOLID);

        Runnable onSave = () -> {
            if (websiteField.getValue() == null) {
                return;
            }
            dto.setWebsite(websiteField.getValue());
            saveContact.accept(dto);
        };

        return EditComponent.createEditableDiv(details, websiteField, onSave);
    }

    private Div createPhoneDiv() {
        PhoneField phoneField = new PhoneField(null, dto.getPhone());

        XFlexLayout details = createDetailsDiv(dto.getPhone(), "Điện thoại", LineAwesomeIcon.PHONE_SOLID);

        Runnable onSave = () -> {
            if (phoneField.getValue() == null || !phoneField.isInvalid()) {
                return;
            }
            dto.setPhone(phoneField.getValue());
            saveContact.accept(dto);
        };

        return EditComponent.createEditableDiv(details, phoneField, onSave);
    }

    private Div createEmailDiv() {
        XEmailField emailField = new XEmailField(null, dto.getEmail());
        emailField.setWidthFull();

        XFlexLayout details = createDetailsDiv(dto.getEmail(), "Email", LineAwesomeIcon.ENVELOPE);

        Runnable onSave = () -> {

            if (emailField.getValue() == null) {
                return;
            }
            dto.setEmail(emailField.getValue());
            saveContact.accept(dto);
        };

        return EditComponent.createEditableDiv(details, emailField, onSave);
    }

    private Div createNoteDiv() {
        NoteField noteField = new NoteField(null, dto.getNotes());
        noteField.setWidthFull();

        XFlexLayout details = createDetailsDiv(dto.getNotes(), "Ghi chú", LineAwesomeIcon.NOTES_MEDICAL_SOLID);

        Runnable onSave = () -> {
            if (noteField.getValue() == null) {
                return;
            }
            dto.setNotes(noteField.getValue());
            saveContact.accept(dto);
        };

        return EditComponent.createEditableDiv(details, noteField, onSave);
    }

    private Div createAddressDiv() {
        /* Edit field */
        AddressField addressField = new AddressField(null, dto.getAddress());
        addressField.setWidthFull();

        XFlexLayout details = createDetailsDiv(dto.getAddress(), "Địa Chỉ", LineAwesomeIcon.MAP_MARKER_SOLID);

        Runnable onSave = () -> {
            if (addressField.getValue() == null) {
                return;
            }
            dto.setAddress(addressField.getValue());
            saveContact.accept(dto);
        };

        return EditComponent.createEditableDiv(details, addressField, onSave);
    }

    private XFlexLayout createDetailsDiv(String value, String toolTip, LineAwesomeIcon icon) {

        Span name = new Span(value);
        SvgIcon fieldIcon = ViewHelper.createIcon(icon, COLOR, toolTip, ICON_SIZE);

        return new XFlexLayout(fieldIcon, name);
    }

    public void refreshContactCard(ContactDto newContact) {
        this.dto = newContact;
        removeAll();
        createContactInforDiv(newContact);
    }
}
