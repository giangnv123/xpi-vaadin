package com.xpi.application.views.crm.interaction.component.card;

import java.util.Set;
import java.util.function.Consumer;

import org.vaadin.lineawesome.LineAwesomeIcon;

import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.SvgIcon;
import com.vaadin.flow.component.textfield.TextField;
import com.xpi.application.module.crm.dto.InteractionDto;
import com.xpi.application.module.crm.dto.subs.TagDto;
import com.xpi.application.views.base.utils.ColorUtils;
import com.xpi.application.views.base.views.EditComponent;
import com.xpi.application.views.base.views.ViewHelper;
import com.xpi.application.views.crm.shared.components.XFlexLayout;
import com.xpi.application.views.crm.shared.components.tag.TagSelectField;

public class CardBody extends Div {

    private TagSelectField tagSelectField;

    private Consumer<InteractionDto> handleSave;

    private InteractionDto dto;

    public CardBody(InteractionDto dto, Consumer<InteractionDto> handleSave, TagSelectField tagSelectField) {
        this.dto = dto;
        this.handleSave = handleSave;
        this.tagSelectField = tagSelectField;

        addClassName("flex-col-layout");
        addClassName("gap-2");
        add(createTitleDiv());
        add(createOutcomeDiv());
        add(createTagsDiv());
    }

    private Div createTitleDiv() {

        String subject = dto.getSubject();

        /* Details div */
        Div details = new Div(subject);
        details.addClassName("title");

        TextField editField = createEditTextField(subject);

        Runnable onSave = () -> {
            if (editField.getValue() == null || editField.getValue().isEmpty()) {
                return;
            }
            dto.setSubject(editField.getValue());
            handleSave.accept(dto);
        };

        return EditComponent.createEditableDiv(details, editField, onSave);
    }

    public void updateBody(InteractionDto updatedDto) {
        this.dto = updatedDto;

    }

    private TextField createEditTextField(String value) {
        TextField editField = new TextField();
        editField.setAutofocus(true);
        editField.setWidthFull();
        editField.setValue(value);
        return editField;
    }

    private Div createOutcomeDiv() {

        String outcome = dto.getOutcome();

        Span outcomeSpan = new Span(outcome);

        SvgIcon outcomeIcon = ViewHelper.createIcon(LineAwesomeIcon.MEDAL_SOLID, ColorUtils.GRAY, "Kết quả", "20px");

        Div details = new Div(outcomeIcon, outcomeSpan);
        details.addClassName("flex-layout");
        details.addClassName(ColorUtils.GRAY_DARK);

        TextField editField = createEditTextField(outcome);

        Runnable onSave = () -> {
            if (editField.getValue() == null) {
                return;
            }
            dto.setOutcome(editField.getValue());
            handleSave.accept(dto);
        };

        return EditComponent.createEditableDiv(details, editField, onSave);
    }

    private Div createTagsDiv() {
        XFlexLayout tagContainer = createTagContainer(dto.getTags());

        Runnable onSave = () -> {

            Set<Long> tagIds = tagSelectField.getTagIds();

            dto.setTagIds(tagIds);

            handleSave.accept(dto);
        };

        return EditComponent.createEditableDiv(tagContainer, tagSelectField, onSave);
    }

    private XFlexLayout createTagContainer(Set<TagDto> tagItems) {

        SvgIcon tagIcon = ViewHelper.createIcon(LineAwesomeIcon.TAGS_SOLID, ColorUtils.GRAY, "Tag", "20px");

        XFlexLayout tagContainer = new XFlexLayout();
        tagContainer.add(tagIcon);

        if (tagItems != null) {
            tagItems.forEach(tag -> {
                Span tagSpan = new Span(tag.getName());
                tagSpan.addClassName("tag");
                tagContainer.add(tagSpan);
            });
        }

        return tagContainer;
    }

}
