package com.xpi.application.views.crm.contact.component;

import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.function.Consumer;

import org.vaadin.lineawesome.LineAwesomeIcon;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.xpi.application.module.crm.contacts.shared.ContactPriority;
import com.xpi.application.module.crm.dto.ContactDto;
import com.xpi.application.views.base.abs.AbstractGrid;
import com.xpi.application.views.base.utils.XUtils;
import com.xpi.application.views.base.views.EditComponent;
import com.xpi.application.views.base.views.ViewHelper;
import com.xpi.application.views.crm.shared.form_component.NameField;
import com.xpi.application.views.crm.shared.form_component.NoteField;
import com.xpi.application.views.crm.shared.form_component.PhoneField;
import com.xpi.application.views.crm.shared.form_component.TypeField;

public class ContactGrid extends AbstractGrid<ContactDto> {

    private static final String NAME_TEXT = "Tên liên hệ";
    private static final String PHONE_TEXT = "Điện thoại";
    private static final String EMAIL_TEXT = "Email";
    private static final String NOTES_TEXT = "Ghi chú";
    private static final String PRIORITY_TEXT = "Ưu tiên";
    private static final String FUTURE_INTEACTION_TEXT = "Tái tương tác";
    private static final String INTERACTION_TEXT = "Tương tác";
    private static final String STATUS_TEXT = "Trạng thái";
    private static final String CATEGORY_TEXT = "Tệp";
    // private static final String ACTION_TEXT = "Thao tác";

    private Consumer<ContactDto> clickAddInteraction;

    private Consumer<ContactDto> saveContact;

    public ContactGrid(Consumer<ContactDto> clickEdit, Consumer<ContactDto> clickDelete,
            Consumer<ContactDto> clickAddInteraction, Consumer<ContactDto> saveContact) {
        super(clickEdit, clickDelete);
        this.clickAddInteraction = clickAddInteraction;
        this.saveContact = saveContact;

        initialSetup();
    }

    @Override
    protected void setupGrid() {

        addColumnWithIcon(contactNameDisplay(), NAME_TEXT, LineAwesomeIcon.USER_SOLID);
        addColumnWithIcon(phoneDisplay(), PHONE_TEXT, LineAwesomeIcon.PHONE_ALT_SOLID);
        // addColumnWithIcon(emailDisplay(), EMAIL_TEXT,
        // LineAwesomeIcon.ENVELOPE_SOLID);
        addColumnWithIcon(displayNotes(), NOTES_TEXT, LineAwesomeIcon.EDIT_SOLID);
        addColumnWithIcon(displayPriority(), PRIORITY_TEXT, LineAwesomeIcon.MEDAL_SOLID);
        addColumnWithIcon(displayFutureInteraction(), FUTURE_INTEACTION_TEXT, LineAwesomeIcon.CLOCK_SOLID);
        addColumnWithIcon(displayInteraction(), INTERACTION_TEXT, LineAwesomeIcon.HANDSHAKE_SOLID);
        addColumnWithIcon(displayCategory(), CATEGORY_TEXT, LineAwesomeIcon.USERS_SOLID);
        addColumnWithIcon(displayStatus(), STATUS_TEXT, LineAwesomeIcon.USER_SOLID);
        // addComponentColumn(this::createActionComponents).setHeader(ACTION_TEXT).setAutoWidth(true);
    }


    private ComponentRenderer<Component, ContactDto> contactNameDisplay() {
        return new ComponentRenderer<>(dto -> {

            String targetValue = dto.getName();
            String placeHolder = NAME_TEXT;

            NameField editField = new NameField();
            editField.setValue(targetValue);
            editField.setWidthFull();

            Runnable onSave = () -> {
                dto.setName(editField.getValue());
                saveContact.accept(dto);
            };

            return createEditableFieldComponent(targetValue, placeHolder, editField, onSave);
        });
    }

    private ComponentRenderer<Component, ContactDto> displayNotes() {
        return new ComponentRenderer<>(dto -> {

            String targetValue = dto.getNotes();
            String placeHolder = NOTES_TEXT;

            NoteField editField = new NoteField();
            editField.setWidthFull();
            editField.setValue(targetValue);

            Runnable onSave = () -> {
                dto.setNotes(editField.getValue());
                saveContact.accept(dto);
            };

            return createEditableFieldComponent(targetValue, placeHolder, editField, onSave);
        });
    }

    private ComponentRenderer<Component, ContactDto> displayFutureInteraction() {
        return new ComponentRenderer<>(dto -> {

            if (dto.getFutureInteraction() == null)
                return new Span();

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yy HH:mm");
            Span time = new Span(formatter.format(dto.getFutureInteraction()));

            return time;
        });
    }


    private ComponentRenderer<Component, ContactDto> phoneDisplay() {
        return new ComponentRenderer<>(dto -> {

            String targetValue = dto.getPhone();
            String placeHolder = PHONE_TEXT;

            PhoneField editField = new PhoneField();
            editField.setValue(targetValue);

            Runnable onSave = () -> {
                if (!XUtils.isValidPhone(editField.getValue())) {
                    editField.setErrorMessage(XUtils.INVALID_PHONE_MESSAGE);
                    return;
                }

                dto.setPhone(editField.getValue());

                saveContact.accept(dto);
            };

            return createEditableFieldComponent(targetValue, placeHolder, editField, onSave);
        });
    }

    private ComponentRenderer<Component, ContactDto> displayCategory() {
        return new ComponentRenderer<>(dto -> {

            Span status = ViewHelper.createStatusComponent(dto.getType().toReadableString(),
                    dto.getType().toString().toLowerCase());

            Div detailsField = new Div(status);

            TypeField editField = new TypeField();
            editField.setValue(dto.getType());

            Runnable onSave = () -> {
                dto.setType(editField.getValue());
                saveContact.accept(dto);

            };

            return EditComponent.createEditableDiv(detailsField, editField, onSave);
        });
    }



    private Component createEditableFieldComponent(String value, String placeHolder, Component editField,
            Runnable onSave) {
        Span name = new Span(value);
        name.addClassName("break-line-cell");
        return editableField(name, editField, onSave);
    }

    private TextField createEditTextField(String value, String placeholder) {
        TextField textField = new TextField();
        textField.setPlaceholder(placeholder);
        textField.setValue(value != null ? value : "");
        textField.setAutofocus(true);
        return textField;
    }

    private Component editableField(Component details, Component editField, Runnable onSave) {
        return EditComponent.createEditableDiv(details, editField, onSave);
    }

    private ComponentRenderer<Component, ContactDto> displayInteraction() {
        return new ComponentRenderer<>(dto -> {
            HorizontalLayout layout = new HorizontalLayout();
            layout.setAlignItems(FlexComponent.Alignment.CENTER);

            int interactionCount = Optional.ofNullable(dto.getInteraction()).map(interactions -> interactions.size())
                    .orElse(0);
            Button addInteractionButton = new Button(LineAwesomeIcon.PLUS_SOLID.create());
            addInteractionButton.addClickListener(event -> clickAddInteraction.accept(dto));
            addInteractionButton.addThemeVariants(ButtonVariant.LUMO_SMALL);

            layout.add(new Span(String.valueOf(interactionCount)), addInteractionButton);
            return layout;
        });
    }

    private ComponentRenderer<Component, ContactDto> displayStatus() {
        return new ComponentRenderer<>(dto -> ViewHelper.createStatusComponent(dto.getStatus().toReadableString(),
                dto.getStatus().toString().toLowerCase()));
    }

    private ComponentRenderer<Component, ContactDto> displayPriority() {
        return new ComponentRenderer<>(dto -> {
            Div layout = new Div();
            layout.addClassNames("flex-layout");

            int priorityLength = ContactPriority.values().length;

            for (int i = 0; i < priorityLength; i++) {
                layout.add(createSlash(i, dto.getPriority(), dto));
            }

            return layout;
        });
    }

    private Div createSlash(int i, ContactPriority contactProperties, ContactDto dto) {

        int priorityLevel = contactProperties.getNumber();

        Div slash = new Div();
        slash.setId(String.valueOf(contactProperties));
        slash.addClassNames("slash");
        if (i <= priorityLevel)
            slash.addClassName("active-slash");
        slash.addClickListener(event -> handleClickSlash(dto, i));

        return slash;
    }

    private void handleClickSlash(ContactDto dto, int index) {
        dto.setPriority(ContactPriority.getPriorityByNumber(index));
        saveContact.accept(dto);
    }

    @SuppressWarnings("unused")
    private ComponentRenderer<Component, ContactDto> emailDisplay() {
        return new ComponentRenderer<>(dto -> {

            String targetValue = dto.getEmail();
            String placeHolder = EMAIL_TEXT;

            TextField editField = createEditTextField(targetValue, placeHolder);

            Runnable onSave = () -> {
                if (!XUtils.isValidEmail(editField.getValue())) {
                    editField.setErrorMessage(XUtils.INVALID_EMAIL_MESSAGE);
                    return;
                }
                dto.setEmail(editField.getValue());
                saveContact.accept(dto);
            };

            return createEditableFieldComponent(targetValue, placeHolder, editField, onSave);
        });
    }
}
