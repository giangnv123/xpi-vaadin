package com.xpi.application.views.crm.interaction.component.card;

import java.util.function.Consumer;

import org.vaadin.lineawesome.LineAwesomeIcon;

import com.vaadin.flow.component.details.Details;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.SvgIcon;
import com.vaadin.flow.component.textfield.IntegerField;
import com.xpi.application.module.crm.contacts.shared.ContactCategory;
import com.xpi.application.module.crm.dto.InteractionDto;
import com.xpi.application.views.base.utils.ColorUtils;
import com.xpi.application.views.base.views.EditComponent;
import com.xpi.application.views.base.views.ViewHelper;
import com.xpi.application.views.crm.shared.form_component.NoteField;
import com.xpi.application.views.crm.shared.form_component.TypeField;

public class CardFooter extends Details {
    private InteractionDto dto;
    private Consumer<InteractionDto> handleSave;

    public CardFooter(InteractionDto dto, Consumer<InteractionDto> handleSave) {
        super("Chi tiết", new Div());

        this.handleSave = handleSave;
        this.dto = dto;
        setOpened(false);
        add(createNotesDiv(), createDurationDive(),
                createContactTypeDiv());
    }

    private Div createNotesDiv() {
        Div details = createDetailItem(LineAwesomeIcon.NOTES_MEDICAL_SOLID, ColorUtils.GRAY, "Ghi chú", "20px",
                dto.getNotes());

        NoteField editField = new NoteField();
        editField.setWidthFull();

        if (dto.getNotes() != null)
            editField.setValue(dto.getNotes());

        Runnable onSave = () -> {
            if (editField.getValue() == null) {
                return;
            }
            dto.setNotes(editField.getValue());
            handleSave.accept(dto);
        };

        return EditComponent.createEditableDiv(details, editField, onSave);
    }

    private Div createDurationDive() {

        Integer duration = dto.getDuration();
        String durationText = duration != null ? duration + " phút" : "...";

        Div details = createDetailItem(LineAwesomeIcon.CLOCK, ColorUtils.GRAY, "Thời gian", "20px", durationText);

        IntegerField editField = new IntegerField();
        editField.setAutofocus(true);
        editField.setPrefixComponent(LineAwesomeIcon.CLOCK.create());
        editField.setWidthFull();

        if (duration != null)
            editField.setValue(duration);

        Runnable onSave = () -> {
            if (editField.getValue() == null) {
                return;
            }
            dto.setDuration(editField.getValue());
            handleSave.accept(dto);
        };

        return EditComponent.createEditableDiv(details, editField, onSave);
    }

    private Div createContactTypeDiv() {
        ContactCategory contactType = dto.getContactType();
        String contactTypeText = contactType != null ? contactType.toReadableString() : "...";

        Div details = createDetailItem(LineAwesomeIcon.USER_SOLID, ColorUtils.GRAY, "Tệp danh bạ", "20px",
                contactTypeText);

        TypeField editField = new TypeField();
        editField.setWidthFull();
        editField.setValue(contactType);

        Runnable onSave = () -> {
            if (editField.getValue() == null) {
                return;
            }
            dto.setContactType(editField.getValue());
            handleSave.accept(dto);
        };

        return EditComponent.createEditableDiv(details, editField, onSave);
    }

    private Div createDetailItem(LineAwesomeIcon iconCode, String color,
            String tooltip, String size, String text) {
        SvgIcon icon = ViewHelper.createIcon(iconCode, color, tooltip, size);

        String textValue = text != null && !text.isEmpty() ? text : "...";

        Span value = new Span(textValue);
        value.addClassName(color);

        Div detailDiv = new Div();
        detailDiv.addClassNames("flex-layout", "detail-item");
        detailDiv.add(icon, value);
        return detailDiv;
    }

}