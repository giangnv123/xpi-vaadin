package com.xpi.application.views.crm.shared.form_component;

import org.vaadin.lineawesome.LineAwesomeIcon;

import com.xpi.application.views.base.utils.XUtils;

public class PhoneField extends BaseTextField {

    public PhoneField() {
        this(null, null);
    }

    public PhoneField(String label) {
        super(label);
    }

    public PhoneField(String label, String value) {
        super(label, value);
    }

    protected void init() {
        setPrefixComponent(LineAwesomeIcon.PHONE_SOLID.create());
        setAllowedCharPattern("[0-9()+-]");
        setMinLength(9);
        setMaxLength(12);
        setPattern(XUtils.PHONE_PATTERN);
    }
}
