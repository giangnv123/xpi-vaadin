package com.xpi.application.views.crm.shared.form_component;

import org.vaadin.lineawesome.LineAwesomeIcon;

import com.xpi.application.views.base.utils.XUtils;

public class XEmailField extends BaseTextField {

    public XEmailField() {
        this(null);
    }

    public XEmailField(String label) {
        super(label);
    }

    public XEmailField(String label, String value) {
        super(label, value);
    }

    protected void init() {
        setPlaceholder("Email");
        setClearButtonVisible(true);
        setPattern(XUtils.EMAIL_PATTERN);
        setPrefixComponent(LineAwesomeIcon.ENVELOPE_SOLID.create());
        setErrorMessage(XUtils.INVALID_EMAIL_MESSAGE);
    }

}
