package com.xpi.application.views.crm.contact.component;

import org.vaadin.lineawesome.LineAwesomeIcon;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.datetimepicker.DateTimePicker;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.xpi.application.module.crm.dto.ContactDto;
import com.xpi.application.views.base.abs.AbstractForm;
import com.xpi.application.views.base.component.XFormLayout;
import com.xpi.application.views.base.utils.XUtils;
import com.xpi.application.views.crm.shared.form_component.NameField;
import com.xpi.application.views.crm.shared.form_component.NoteField;
import com.xpi.application.views.crm.shared.form_component.PhoneField;
import com.xpi.application.views.crm.shared.form_component.StatusField;
import com.xpi.application.views.crm.shared.form_component.TypeField;
import com.xpi.application.views.crm.shared.form_component.WebsiteField;
import com.xpi.application.views.crm.shared.form_component.XEmailField;

public class ContactForm extends AbstractForm<ContactDto> {

        private NameField nameField;
        private PhoneField phoneField;
        private XEmailField emailField;
        private WebsiteField websiteField;
        private NoteField noteField;

        private TypeField typeField;
        private StatusField statusField;
        private DateTimePicker futureInteractionPicker;

        public ContactForm() {
                super(ContactDto.class);
                initializeForm();
        }

        protected void preBuild() {
                XFormLayout formLayout = new XFormLayout();

                nameField = new NameField("Tên liên lạc");

                phoneField = new PhoneField("Số điện thoại chính");

                websiteField = new WebsiteField("Website");

                emailField = new XEmailField("Email");

                noteField = new NoteField("Ghi chú");

                typeField = new TypeField("Tệp danh bạ");

                statusField = new StatusField("Trạng thái");

                futureInteractionPicker = new DateTimePicker("Thời gian tái tương tác");
                futureInteractionPicker.setWidthFull();
                Button removeButton = new Button(LineAwesomeIcon.TIMES_SOLID.create(),
                                e -> futureInteractionPicker.clear());

                HorizontalLayout futureInteractionLayout = new HorizontalLayout(futureInteractionPicker, removeButton);
                futureInteractionLayout.setAlignItems(FlexComponent.Alignment.END);

                formLayout.setColspan(noteField, 2);
                formLayout.setColspan(futureInteractionLayout, 2);

                formLayout.add(nameField, phoneField, emailField, websiteField, typeField,
                                statusField, futureInteractionLayout, noteField);
                add(formLayout);
        }

        @Override
        protected void setupBinder() {
                binder.forField(nameField)
                                .asRequired("Vui lòng nhập tên liên lạc")
                                .bind(ContactDto::getName, ContactDto::setName);

                binder.forField(phoneField)
                                .bind(ContactDto::getPhone, ContactDto::setPhone);

                binder.forField(emailField)
                                .withValidator(value -> value.isEmpty()
                                                || XUtils.isValidEmail(value),
                                                "Địa chỉ email không hợp lệ")
                                .bind(ContactDto::getEmail, ContactDto::setEmail);

                binder.forField(websiteField)
                                .bind(ContactDto::getWebsite, ContactDto::setWebsite);

                binder.forField(noteField)
                                .bind(ContactDto::getNotes, ContactDto::setNotes);

                binder.forField(typeField)
                                .bind(ContactDto::getType, ContactDto::setType);

                binder.forField(statusField)
                                .bind(ContactDto::getStatus, ContactDto::setStatus);

                binder.forField(futureInteractionPicker)
                                .bind(ContactDto::getFutureInteraction, ContactDto::setFutureInteraction);
        }
}
