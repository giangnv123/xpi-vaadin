package com.xpi.application.views.crm.shared.components;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Div;

public class Circle extends Div {

    public Circle(Component... components) {
        add(components);
        setWidth("40px");
        setHeight("40px");
        getStyle().set("border-radius", "50%");
        getStyle().set("display", "grid");
        getStyle().set("place-items", "center");
        getStyle().set("border", "2px solid #EAEDED");
    }
}
