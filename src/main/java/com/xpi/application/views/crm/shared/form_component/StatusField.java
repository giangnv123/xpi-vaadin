package com.xpi.application.views.crm.shared.form_component;

import org.vaadin.lineawesome.LineAwesomeIcon;
import com.vaadin.flow.component.combobox.ComboBox;
import com.xpi.application.module.crm.contacts.shared.ContactStatus;

public class StatusField extends ComboBox<ContactStatus> {

    public StatusField() {
        this(null);
    }

    public StatusField(String label) {
        if (label != null) {
            setLabel(label);
        }
        init();
    }

    private void init() {
        setPrefixComponent(LineAwesomeIcon.USER.create());
        setItems(ContactStatus.values());
        setItemLabelGenerator(ContactStatus::toReadableString);
        setValue(ContactStatus.ACTIVE);
    }
}
