package com.xpi.application.views.crm.interaction.component.card;

import java.time.format.DateTimeFormatter;
import java.util.function.Consumer;

import org.vaadin.lineawesome.LineAwesomeIcon;

import com.vaadin.flow.component.contextmenu.ContextMenu;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.SvgIcon;
import com.xpi.application.module.crm.dto.InteractionDto;
import com.xpi.application.module.crm.interactions.shared.InteractionType;
import com.xpi.application.views.base.utils.ColorUtils;
import com.xpi.application.views.base.views.ViewHelper;
import com.xpi.application.views.crm.shared.components.Circle;
import com.xpi.application.views.crm.shared.components.XFlexLayout;

public class CardHeader extends Div {

    private Consumer<InteractionDto> handleClickDelete;
    private Consumer<InteractionDto> handleClickEdit;

    public CardHeader(InteractionDto dto,
            Consumer<InteractionDto> handleClickDelete,
            Consumer<InteractionDto> handleClickEdit) {

        this.handleClickDelete = handleClickDelete;
        this.handleClickEdit = handleClickEdit;

        addClassName("header");
        add(createLeftSideHeader(dto.getContactName(), dto.getType()));
        add(createRightSideHeader(dto));
    }

    private XFlexLayout createLeftSideHeader(String contactName, InteractionType type) {
        SvgIcon icon = createInteractionTypeIcon(type);
        Div circle = new Circle(icon);

        Span nameText = new Span(contactName);
        nameText.getStyle().set("font-weight", "500");
        nameText.getStyle().set("color", ColorUtils.GRAY_DARK);

        XFlexLayout leftSide = new XFlexLayout(circle, nameText);
        leftSide.getStyle().set("margin-right", "12.5px");

        return leftSide;
    }

    private XFlexLayout createRightSideHeader(InteractionDto dto) {
        SvgIcon timeIcon = ViewHelper.createIcon(LineAwesomeIcon.CLOCK_SOLID, ColorUtils.GRAY, null, "15px");

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yy HH:mm");
        Span time = new Span(formatter.format(dto.getDateTime()));
        time.getStyle().setColor(ColorUtils.GRAY);
        time.getStyle().setFontSize("12px");

        SvgIcon dotIcon = ViewHelper.createIcon(LineAwesomeIcon.ELLIPSIS_V_SOLID, ColorUtils.GRAY, null, "20px");
        dotIcon.getStyle().set("cursor", "pointer");

        ContextMenu menu = createContextMenu(dto);
        menu.setTarget(dotIcon);

        XFlexLayout rightSide = new XFlexLayout();
        rightSide.getStyle().set("gap", "2.5px");
        rightSide.add(timeIcon, time, dotIcon);

        return rightSide;
    }

    private ContextMenu createContextMenu(InteractionDto dto) {
        ContextMenu menu = new ContextMenu();
        menu.setOpenOnClick(true);

        XFlexLayout details = new XFlexLayout();
        SvgIcon eyeIcon = ViewHelper.createIcon(LineAwesomeIcon.EYE, ColorUtils.PRIMARY, null, "20px");
        details.add(eyeIcon, new Span("Chi tiết"));

        XFlexLayout delete = new XFlexLayout();
        SvgIcon trashIcon = ViewHelper.createIcon(LineAwesomeIcon.TRASH_ALT_SOLID, ColorUtils.DANGER, null, "20px");
        delete.add(trashIcon, new Span("Xóa"));

        menu.addItem(details, e -> {
            handleClickEdit.accept(dto);
        });
        menu.addItem(delete, e -> {
            handleClickDelete.accept(dto);
        });

        return menu;
    }

    private SvgIcon createInteractionTypeIcon(InteractionType type) {
        if (type == null)
            return ViewHelper.createIcon(LineAwesomeIcon.PHONE_SOLID, ColorUtils.PURPLE, null, "25px");

        LineAwesomeIcon icon = switch (type) {
            case CALL -> LineAwesomeIcon.PHONE_SOLID;
            case MEETING -> LineAwesomeIcon.HANDSHAKE;
            case CHAT -> LineAwesomeIcon.COMMENT_SOLID;
            case EMAIL -> LineAwesomeIcon.ENVELOPE_SOLID;
            default -> LineAwesomeIcon.PHONE_SOLID;
        };
        return ViewHelper.createIcon(icon, ColorUtils.PURPLE, type.toReadableString(), "25px");
    }
}
