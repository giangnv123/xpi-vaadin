package com.xpi.application.views.crm.interaction;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.flow.data.provider.ConfigurableFilterDataProvider;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.xpi.application.base.ApiResponse;
import com.xpi.application.base.OffsetSizePageRequest;
import com.xpi.application.module.crm.dto.ContactDto;
import com.xpi.application.module.crm.dto.InteractionDto;
import com.xpi.application.module.crm.dto.subs.ItemDto;
import com.xpi.application.module.crm.dto.subs.SourceDto;
import com.xpi.application.module.crm.dto.subs.TagDto;
import com.xpi.application.module.crm.dto.subs.TopicDto;
import com.xpi.application.module.crm.interactions.InteractionService;
import com.xpi.application.module.crm.interactions.shared.InteractionFilter;
import com.xpi.application.module.crm.shared.CRMHelperService;
import com.xpi.application.views.base.utils.CustomApiResponse;
import com.xpi.application.views.crm.interaction.views.InteractionGridView;

@SpringComponent
public class InteractionPresenter {

    @Autowired
    private CRMHelperService helperService;

    private ConfigurableFilterDataProvider<InteractionDto, Void, InteractionFilter> filterDataProvider;

    private InteractionDataProvider dataProvider;

    private InteractionService service;

    private InteractionFilter initialFilter = InteractionFilter.defaultFilter();

    public InteractionPresenter(InteractionService service) {
        this.service = service;
        this.dataProvider = new InteractionDataProvider(service);
        filterDataProvider = dataProvider.withConfigurableFilter();
    }

    public void initView(InteractionGridView view) {
        filterDataProvider.setFilter(initialFilter);
    }

    public void refreshGrid(InteractionFilter filter) {
        filterDataProvider.setFilter(filter);
        initialFilter = filter;
    }

    public CustomApiResponse<Void> delete(String id) {
        ApiResponse<Void> response = service.delete(id);
        return CustomApiResponse.delete(response);
    }

    public CustomApiResponse<InteractionDto> saveInteraction(InteractionDto dto) {
        CustomApiResponse<InteractionDto> response = helperService.saveInteraction(dto);
        return response;
    }

    public List<InteractionDto> getAll() {
        OffsetSizePageRequest pageRequest = new OffsetSizePageRequest(0, Integer.MAX_VALUE);
        return service.getAll(pageRequest, initialFilter);
    }

    /* Additional methods */

    /* Contact */

    public List<ContactDto> getAllContacts() {
        return helperService.getAllContact();
    }

    public CustomApiResponse<ContactDto> getContactById(String id) {
        return helperService.getContactById(id);
    }

    public CustomApiResponse<ContactDto> saveContact(ContactDto dto) {
        return helperService.saveContact(dto);
    }

    /* Tag */
    public Set<TagDto> getAllTags() {
        return helperService.getAllTags();
    }

    public CustomApiResponse<TagDto> saveTag(TagDto dto) {
        return helperService.saveTag(dto);
    }

    /* Items */

    public Set<ItemDto> getAllItems() {
        return helperService.getAllItems();
    }

    public CustomApiResponse<ItemDto> saveItem(ItemDto dto) {
        return helperService.saveItem(dto);
    }

    /* Source */

    public Set<SourceDto> getAllSources() {
        return helperService.getAllSources();
    }

    public CustomApiResponse<SourceDto> saveSource(SourceDto dto) {
        return helperService.saveSource(dto);
    }

    /* topic */

    public Set<TopicDto> getAlltopics() {
        return helperService.getAllTopics();
    }

        public CustomApiResponse<TopicDto> savetopic(TopicDto dto) {
            return helperService.saveTopic(dto);
        }

}