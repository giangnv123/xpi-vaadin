package com.xpi.application.views.crm.shared.components.source;

import java.util.Set;
import java.util.function.Function;

import org.vaadin.lineawesome.LineAwesomeIcon;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.xpi.application.module.crm.dto.subs.SourceDto;
import com.xpi.application.views.base.component.XDialog;
import com.xpi.application.views.base.utils.CustomApiResponse;
import com.xpi.application.views.base.views.ViewHelper;

public class SourceSelectField extends HorizontalLayout {

    private SourceDto currentValues;

    private Function<Void, Set<SourceDto>> getSources;
    private Function<SourceDto, CustomApiResponse<SourceDto>> saveSource;
    private Set<SourceDto> sourceCache;

    private ComboBox<SourceDto> selectSourceField;
    private SourceForm form;
    private XDialog dialog;

    public SourceSelectField(Function<Void, Set<SourceDto>> getSources,
            Function<SourceDto, CustomApiResponse<SourceDto>> saveSource,
            Set<SourceDto> sourceCache) {
        this.getSources = getSources;
        this.saveSource = saveSource;
        this.sourceCache = sourceCache;
        initialSetup();
    }

    private void initialSetup() {
        setupDialogAndForm();
        setupSelectSourceField();

        Button button = createAddButton();

        add(selectSourceField, button);
        setWidthFull();
        setAlignItems(FlexComponent.Alignment.END);
    }

    private ComboBox<SourceDto> setupSelectSourceField() {
        selectSourceField = new ComboBox<>();
        refreshSourceField();
        selectSourceField.setItemLabelGenerator(SourceDto::getName);
        selectSourceField.setWidthFull();
        selectSourceField.setLabel("Chọn nguồn");
        selectSourceField.setPlaceholder("Chọn nguồn");
        return selectSourceField;
    }

    private Button createAddButton() {
        Button addButton = new Button(LineAwesomeIcon.PLUS_SOLID.create());
        addButton.addClickListener(event -> dialog.open());
        return addButton;
    }

    private void setupDialogAndForm() {
        form = new SourceForm();
        dialog = new XDialog("Thêm Nguồn", this::performSaveSource);
        dialog.add(form);
    }

    private void performSaveSource() {
        if (!form.validateAndWrite())
            return;

        SourceDto dto = form.getEntity();

        CustomApiResponse<SourceDto> response = saveSource.apply(dto);

        Runnable runOnSuccess = () -> {
            refreshSourceCache();
            refreshSourceField();
            selectSourceField.setValue(response.getDto());
        };

        ViewHelper.handleResponse(response.isSuccess(), response.getMessage(), dialog, form, runOnSuccess);
    }

    public void setLabel(String label) {
        selectSourceField.setLabel(label);
    }

    public void setValues(SourceDto sources) {
        currentValues = sources;
        selectSourceField.setValue(currentValues);
    }

    public Long getSourceId() {
        if (selectSourceField.getValue() == null)
            return null;

        return selectSourceField.getValue().getId();
    }

    private void refreshSourceField() {
        selectSourceField.setItems(sourceCache);
    }

    public void refreshSourceCache() {
        sourceCache = getSources.apply(null);
        refreshSourceField();
    }
}
