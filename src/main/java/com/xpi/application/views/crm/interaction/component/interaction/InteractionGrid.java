package com.xpi.application.views.crm.interaction.component.interaction;

import java.util.function.Consumer;

import org.vaadin.lineawesome.LineAwesomeIcon;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.SvgIcon;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.xpi.application.module.crm.dto.InteractionDto;
import com.xpi.application.views.base.abs.AbstractGrid;

public class InteractionGrid extends AbstractGrid<InteractionDto> {

    public InteractionGrid(Consumer<InteractionDto> clickEdit, Consumer<InteractionDto> clickDelete) {
        super(clickEdit, clickDelete);
        initialSetup();
    }

    @Override
    protected void setupGrid() {
        addColumn(displayCustomer()).setHeader("Khách hàng").setAutoWidth(true);
        addColumn(dto -> dto.getSubject()).setHeader("Tiêu đề").setAutoWidth(true);
        addColumn(dto -> dto.getDuration()).setHeader("Thời lượng").setAutoWidth(true);
        addComponentColumn(this::createActionComponents).setHeader("Chức năng").setAutoWidth(true);
    }

    private ComponentRenderer<Component, InteractionDto> displayCustomer() {
        return new ComponentRenderer<>(dto -> {
            HorizontalLayout layout = new HorizontalLayout();
            layout.setAlignItems(FlexComponent.Alignment.CENTER);
            String nameValue = dto.getContactName();
            SvgIcon icon = LineAwesomeIcon.USER.create();
            layout.add(icon, new Span(nameValue));
            return layout;
        });
    }

}
