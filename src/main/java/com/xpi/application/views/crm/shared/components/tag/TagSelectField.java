package com.xpi.application.views.crm.shared.components.tag;

import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.vaadin.lineawesome.LineAwesomeIcon;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.MultiSelectComboBox;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.xpi.application.module.crm.dto.subs.TagDto;
import com.xpi.application.views.base.component.XDialog;
import com.xpi.application.views.base.utils.CustomApiResponse;
import com.xpi.application.views.base.views.ViewHelper;

public class TagSelectField extends HorizontalLayout {

    private Set<TagDto> currentValues;
    private TagForm tagForm;
    private XDialog dialog;
    private MultiSelectComboBox<TagDto> selectTagField;
    private Function<Void, Set<TagDto>> getTags;
    private Function<TagDto, CustomApiResponse<TagDto>> saveTag;

    private Set<TagDto> tagCache;

    public TagSelectField(Function<Void, Set<TagDto>> getTags, Function<TagDto, CustomApiResponse<TagDto>> saveTag,
            Set<TagDto> tagCache) {
        this.getTags = getTags;
        this.saveTag = saveTag;
        this.tagCache = tagCache;
        initialSetup();
    }

    private void initialSetup() {
        setupDialogAndForm();
        setupSelectTagField();

        Button button = createAddButton();

        add(selectTagField, button);
        setWidthFull();
        setAlignItems(FlexComponent.Alignment.END);
    }

    private MultiSelectComboBox<TagDto> setupSelectTagField() {
        selectTagField = new MultiSelectComboBox<>();
        refreshTagField();
        selectTagField.setItemLabelGenerator(TagDto::getName);
        selectTagField.setWidthFull();
        selectTagField.setPlaceholder("Chọn tags");
        selectTagField.setLabel("Tags");
        return selectTagField;
    }

    private Button createAddButton() {
        Button addButton = new Button(LineAwesomeIcon.PLUS_SOLID.create());
        addButton.addClickListener(event -> dialog.open());
        return addButton;
    }

    private void setupDialogAndForm() {
        tagForm = new TagForm();
        dialog = new XDialog("Thêm tag", this::performSaveTag);
        dialog.add(tagForm);
    }

    private void performSaveTag() {
        if (!tagForm.validateAndWrite())
            return;

        TagDto dto = tagForm.getEntity();

        CustomApiResponse<TagDto> response = saveTag.apply(dto);

        Runnable runOnSuccess = () -> {
            refreshTagCache();
            refreshTagField();
            selectTagField.setValue(currentValues);
        };

        ViewHelper.handleResponse(response.isSuccess(), response.getMessage(), dialog, tagForm, runOnSuccess);
    }

    public void setLabel(String label) {
        selectTagField.setLabel(label);
    }

    public void setValues(Set<TagDto> tags) {
        currentValues = tags;
        selectTagField.setValue(currentValues);
    }

    public Set<Long> getTagIds() {
        if (selectTagField.getValue() == null)
            return null;
        return selectTagField.getValue().stream().map(TagDto::getId).collect(Collectors.toSet());
    }

    private void refreshTagField() {
        selectTagField.setItems(tagCache);
    }

    public void refreshTagCache() {
        tagCache = getTags.apply(null);
        refreshTagField();
    }
}
