package com.xpi.application.views.crm.shared.components;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Div;

public class XFlexLayout extends Div {

    public XFlexLayout() {
        addClassName("flex-layout");
    }

    public XFlexLayout(Component... components) {
        addClassName("flex-layout");
        add(components);
    }

    public XFlexLayout(String className, Component... components) {
        addClassName(className);
        add(components);
    }
}
