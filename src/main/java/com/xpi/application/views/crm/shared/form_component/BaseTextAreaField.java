package com.xpi.application.views.crm.shared.form_component;

import com.vaadin.flow.component.textfield.TextArea;

public abstract class BaseTextAreaField extends TextArea {

    public BaseTextAreaField(String label) {
        this(label, null);
    }

    public BaseTextAreaField(String label, String value) {
        if (label != null) {
            setLabel(label);
        }
        if (value != null) {
            setValue(value);
        }
        init();
    }

    @Override
    public void setValue(String value) {
        super.setValue(value != null ? value : "");
    }

    protected abstract void init();
}
