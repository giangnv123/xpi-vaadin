package com.xpi.application.views.crm.shared.components.topic;

import java.util.Set;
import java.util.function.Function;

import org.vaadin.lineawesome.LineAwesomeIcon;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.xpi.application.module.crm.dto.subs.TopicDto;
import com.xpi.application.views.base.component.XDialog;
import com.xpi.application.views.base.utils.CustomApiResponse;
import com.xpi.application.views.base.views.ViewHelper;

public class TopicSelectField extends HorizontalLayout {

    private TopicDto currentValues;

    private Function<Void, Set<TopicDto>> getTopics;
    private Function<TopicDto, CustomApiResponse<TopicDto>> saveTopic;
    private Set<TopicDto> topicCache;

    private ComboBox<TopicDto> selectTopicField;
    private TopicForm form;
    private XDialog dialog;

    public TopicSelectField(Function<Void, Set<TopicDto>> getTopics,
            Function<TopicDto, CustomApiResponse<TopicDto>> saveTopic,
            Set<TopicDto> topicCache) {
        this.getTopics = getTopics;
        this.saveTopic = saveTopic;
        this.topicCache = topicCache;
        initialSetup();
    }

    private void initialSetup() {
        setupDialogAndForm();
        setupSelectTopicField();

        Button button = createAddButton();

        add(selectTopicField, button);
        setWidthFull();
        setAlignItems(FlexComponent.Alignment.END);
    }

    private ComboBox<TopicDto> setupSelectTopicField() {
        selectTopicField = new ComboBox<>();
        refreshTopicField();
        selectTopicField.setItemLabelGenerator(TopicDto::getName);
        selectTopicField.setWidthFull();
        selectTopicField.setLabel("Chủ đề");
        selectTopicField.setPlaceholder("Chọn chủ đề");
        return selectTopicField;
    }

    private Button createAddButton() {
        Button addButton = new Button(LineAwesomeIcon.PLUS_SOLID.create());
        addButton.addClickListener(event -> dialog.open());
        return addButton;
    }

    private void setupDialogAndForm() {
        form = new TopicForm();
        dialog = new XDialog("Thêm chủ đề mới", this::performSaveTopic);
        dialog.add(form);
    }

    private void performSaveTopic() {
        if (!form.validateAndWrite())
            return;

        TopicDto dto = form.getEntity();

        CustomApiResponse<TopicDto> response = saveTopic.apply(dto);

        Runnable runOnSuccess = () -> {
            refreshTopicCache();
            refreshTopicField();
            selectTopicField.setValue(response.getDto());
        };

        ViewHelper.handleResponse(response.isSuccess(), response.getMessage(), dialog, form, runOnSuccess);
    }

    public void setLabel(String label) {
        selectTopicField.setLabel(label);
    }

    public void setValues(TopicDto topics) {
        currentValues = topics;
        selectTopicField.setValue(currentValues);
    }

    public Long getTopicId() {
        if (selectTopicField.getValue() == null)
            return null;

        return selectTopicField.getValue().getId();
    }

    private void refreshTopicField() {
        selectTopicField.setItems(topicCache);
    }

    public void refreshTopicCache() {
        topicCache = getTopics.apply(null);
        refreshTopicField();
    }
}
