package com.xpi.application.views.crm.shared.form_component;

import org.vaadin.lineawesome.LineAwesomeIcon;

import com.vaadin.flow.component.combobox.ComboBox;
import com.xpi.application.module.crm.contacts.shared.ContactCategory;

public class TypeField extends ComboBox<ContactCategory> {
    public TypeField() {
        this(null);
    }

    public TypeField(String label) {
        if (label != null)
            setLabel(label);
        init();
    }

    private void init() {
        setPrefixComponent(LineAwesomeIcon.USERS_SOLID.create());
        setItems(ContactCategory.values());
        setItemLabelGenerator(ContactCategory::toReadableString);
    }

}
