package com.xpi.application.views.crm.shared.components;

import com.xpi.application.module.crm.dto.ContactDto;
import com.xpi.application.views.base.abs.AbstractForm;
import com.xpi.application.views.base.component.XFormLayout;
import com.xpi.application.views.crm.shared.components.source.SourceSelectField;
import com.xpi.application.views.crm.shared.form_component.NameField;
import com.xpi.application.views.crm.shared.form_component.NoteField;
import com.xpi.application.views.crm.shared.form_component.PhoneField;
import com.xpi.application.views.crm.shared.form_component.WebsiteField;

public class SimpleContactForm extends AbstractForm<ContactDto> {

        private final NameField nameField = new NameField("Tên liên lạc");
        private final PhoneField phoneField = new PhoneField("Số điện thoại chính");
        private final WebsiteField websiteField = new WebsiteField("Website");
        private final NoteField noteField = new NoteField("Ghi chú");

        private SourceSelectField sourceSelectField;

        public SimpleContactForm(SourceSelectField sourceSelectField) {
                super(ContactDto.class);
                this.sourceSelectField = sourceSelectField;
                initializeForm();
                buildLayout();
        }

        private void buildLayout() {
                XFormLayout formLayout = new XFormLayout();
                formLayout.setColspan(noteField, 2);
                formLayout.add(nameField, phoneField, sourceSelectField, websiteField, noteField);
                add(formLayout);
        }

        @Override
        protected void setupBinder() {
                binder.forField(nameField)
                                .asRequired("Vui lòng nhập tên liên lạc")
                                .bind(ContactDto::getName, ContactDto::setName);

                binder.forField(websiteField)
                                .bind(ContactDto::getWebsite, ContactDto::setWebsite);

                binder.forField(phoneField)
                                .bind(ContactDto::getPhone, ContactDto::setPhone);

                binder.forField(noteField)
                                .bind(ContactDto::getNotes, ContactDto::setNotes);
        }

        public Long getSourceId() {
                return sourceSelectField.getSourceId();
        }
}
