package com.xpi.application.views.crm.interaction.component.interaction;

import java.util.Collections;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import com.vaadin.flow.component.combobox.MultiSelectComboBox;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.xpi.application.module.crm.dto.subs.TagDto;
import com.xpi.application.module.crm.interactions.shared.InteractionFilter;
import com.xpi.application.views.base.abs.AbstractFilterContainer;

public class InteractionFilterContainer extends AbstractFilterContainer<InteractionFilter> {

    private Set<TagDto> tags;

    /* Components */
    private MultiSelectComboBox<TagDto> tagSelect;

    public InteractionFilterContainer(Consumer<InteractionFilter> applyFilter, Set<TagDto> tags) {
        super(applyFilter);
        this.tags = tags;
        buildLayout();
    }

    @Override
    protected HorizontalLayout createFilterComponent() {
        HorizontalLayout filterComponents = new HorizontalLayout();

        tagSelect = createTagSelect();
        filterComponents.add(searchField, tagSelect);

        return filterComponents;
    }

    private MultiSelectComboBox<TagDto> createTagSelect() {
        tagSelect = new MultiSelectComboBox<>();
        tagSelect.setItems(tags);
        tagSelect.setPlaceholder("Tags");
        tagSelect.setItemLabelGenerator(TagDto::getName);
        tagSelect.addValueChangeListener(event -> applyFilters());
        return tagSelect;
    }

    @Override
    protected void clearFilterField() {
        searchField.clear();
        tagSelect.clear();
    }

    @Override
    protected InteractionFilter getFilterValue() {

        Set<Long> tagIds = Optional.ofNullable(tagSelect)
                .map(select -> select.getValue())
                .orElse(Collections.emptySet())
                .stream()
                .filter(Objects::nonNull)
                .map(TagDto::getId)
                .collect(Collectors.toSet());

        return new InteractionFilter(searchField.getValue(), tagIds);
    }

    @Override
    protected InteractionFilter getDefaultFilter() {
        return InteractionFilter.defaultFilter();
    }
}
