package com.xpi.application.views.crm.shared.form_component;

import com.vaadin.flow.component.textfield.TextField;

public abstract class BaseTextField extends TextField {

    public BaseTextField(String label) {
        this(label, null);
    }

    public BaseTextField(String label, String value) {
        if (label != null)
            setLabel(label);
        if (value != null)
            setValue(value);
        init();
    }

    @Override
    public void setValue(String value) {
        super.setValue(value != null ? value : "");
    }

    protected abstract void init();
}