package com.xpi.application.views.crm.contact.views;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.xpi.application.module.crm.dto.ContactDto;
import com.xpi.application.views.base.abs.AbstractBasicGridView;
import com.xpi.application.views.base.component.XDialog;
import com.xpi.application.views.base.utils.CustomApiResponse;
import com.xpi.application.views.crm.contact.ContactPresenter;
import com.xpi.application.views.crm.contact.component.ContactFilterContainer;
import com.xpi.application.views.crm.contact.component.ContactForm;
import com.xpi.application.views.crm.contact.component.ContactGrid;
import com.xpi.application.views.layout.CRMLayout;

@PageTitle("Danh bạ")
@Route(value = "crm/contacts", layout = CRMLayout.class)
public class ContactGridView extends AbstractBasicGridView<ContactDto, String> {

    private ContactPresenter presenter;

    private ContactForm contactForm;

    private ContactDto currentDto;

    /* Components */
    private ContactFilterContainer filterContainer;

    private ContactGrid grid;

    public ContactGrid getGrid() {
        return this.grid;
    }

    public ContactGridView(ContactPresenter presenter) {
        this.entityName = "Danh bạ";
        this.presenter = presenter;
        grid = new ContactGrid(this::handleClickEdit, this::handleClickDelete,
                this::handleClickAddInteraction, this::editContact);
        this.presenter.initView(this);
        initialSetup();
    }

    @Override
    protected Div createHeaderSection() {
        filterContainer = new ContactFilterContainer(presenter::refreshGrid);
        Div header = new Div(filterContainer, createAddEntityButton());
        header.addClassName("header");
        return header;
    }

    @Override
    protected Component createContentSection() {
        grid.setSizeFull();
        return grid;
    }

    private void editContact(ContactDto dto) {
        CustomApiResponse<ContactDto> response = presenter.save(dto);
        handleResponse(response.isSuccess(), response.getMessage(), controlDialog, contactForm, null);
    }

    protected void setupControlDialog() {
        contactForm = new ContactForm();
        controlDialog = new XDialog(entityName, this::performSave);
        controlDialog.add(contactForm);
        controlDialog.setWidth("650px");
    }

    /* To create contact */

    private void performSave() {
        if (!contactForm.validateAndWrite())
            return;

        ContactDto dto = contactForm.getEntity();

        CustomApiResponse<ContactDto> response = presenter.save(dto);

        handleResponse(response.isSuccess(), response.getMessage(), controlDialog, contactForm, null);
    }

    protected void performDelete() {
        CustomApiResponse<Void> response = presenter.delete(currentDto);
        showMessage(response.getMessage(), response.isSuccess());
    }

    protected void handleClickEdit(ContactDto dto) {
        contactForm.setEntity(dto);
        controlDialog.open();
    }

    protected void handleClickDelete(ContactDto dto) {
        currentDto = dto;
        deleteDialog.open();
    }

    protected void handleClickAdd() {
        controlDialog.open();
        contactForm.resetForm();
        contactForm.setEntity(new ContactDto());
    }

    private void handleClickAddInteraction(ContactDto dto) {
    }
}
