package com.xpi.application.views.crm.shared.components;

import java.util.List;
import java.util.function.Consumer;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.xpi.application.module.crm.dto.ContactDto;
import com.xpi.application.module.crm.dto.InteractionDto;
import com.xpi.application.views.base.component.XDialog;
import com.xpi.application.views.crm.interaction.component.interaction.InteractionForm;
import com.xpi.application.views.crm.shared.components.source.SourceSelectField;
import com.xpi.application.views.crm.shared.components.tag.TagSelectField;
import com.xpi.application.views.crm.shared.components.topic.TopicSelectField;

public class InteractionDialog extends XDialog {

    private List<ContactDto> contacts;

    private Consumer<InteractionDto> save;


    private InteractionForm interactionForm;

    private TagSelectField tagSelectField;
    private TopicSelectField topicSelectField;
    private SourceSelectField sourceSelectField;

    public InteractionDialog(Consumer<InteractionDto> save,
            List<ContactDto> contacts,
            TagSelectField tagSelectField,
            TopicSelectField topicSelectField,
            SourceSelectField sourceSelectField) {
        super("Tương tác", null);
        this.save = save;
        this.tagSelectField = tagSelectField;
        this.contacts = contacts;
        this.topicSelectField = topicSelectField;
        this.sourceSelectField = sourceSelectField;
        initialSetup();
    }

    private void initialSetup() {
        setupForm();
        add(interactionForm);
        setWidth("650px");

        getFooter().removeAll();

        Button saveButton = new Button("Lưu", event -> performSave());
        saveButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);

        Button cancelButton = createCancelButton();
        getFooter().add(cancelButton, saveButton);
    }

    private void setupForm() {
        interactionForm = new InteractionForm(contacts, tagSelectField, topicSelectField, sourceSelectField);
    }

    private void performSave() {
        if (!interactionForm.validateAndWrite())
            return;

        InteractionDto interactionDto = interactionForm.getEntity();
        interactionDto.setTagIds(interactionForm.getTagIds());
        interactionDto.setTopicId(interactionForm.getTopicId());

        if (interactionForm.isAddingNewContact()) {
            SimpleContactForm contactForm = interactionForm.getContactForm();

            if (!contactForm.validateAndWrite())
                return;

            interactionDto.populateContact(contactForm.getEntity());
            interactionDto.setSourceId(contactForm.getSourceId());
        } else {
            interactionDto.setContactId(interactionForm.getContactId());
        }

        save.accept(interactionDto);

        close();

    }

    private Button createCancelButton() {
        return new Button("Hủy", event -> close());
    }

    public InteractionForm getTargetForm() {
        return interactionForm;
    }

}