package com.xpi.application.views.crm.interaction.component.interaction;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.vaadin.flow.component.html.Div;
import com.xpi.application.module.crm.dto.InteractionDto;
import com.xpi.application.module.crm.dto.subs.TagDto;
import com.xpi.application.views.base.utils.CustomApiResponse;
import com.xpi.application.views.crm.interaction.component.card.InteractionCard;
import com.xpi.application.views.crm.shared.components.tag.TagSelectField;

public class InteractionGallery extends Div {

    private Map<String, InteractionCard> cards = new HashMap<>();
    private Consumer<InteractionDto> handleSave;
    private Consumer<InteractionDto> handleClickDelete;
    private Consumer<InteractionDto> handleClickEdit;

    private Function<Void, Set<TagDto>> getTags;
    private Function<TagDto, CustomApiResponse<TagDto>> saveTag;

    private Set<TagDto> tagCache;

    /* create new tag select field */

    public InteractionGallery(
            Consumer<InteractionDto> handleClickDelete,
            Consumer<InteractionDto> handleClickEdit,
            Consumer<InteractionDto> handleSave,
            Function<Void, Set<TagDto>> getTags,
            Function<TagDto, CustomApiResponse<TagDto>> saveTag) {
        this.handleSave = handleSave;
        this.handleClickDelete = handleClickDelete;
        this.handleClickEdit = handleClickEdit;
        this.getTags = getTags;
        this.saveTag = saveTag;
        refreshTagCache();
        addClassName("grid-container");
    }

    private void refreshTagCache() {
        tagCache = getTags.apply(null);
    }

    public void addInteractionCard(InteractionDto interaction) {
        TagSelectField tagSelectField = new TagSelectField(getTags, saveTag, tagCache);

        Boolean isUpdate = cards.containsKey(interaction.getId());

        if (isUpdate)
            cards.get(interaction.getId()).updateInteraction(interaction);
        else {
            InteractionCard card = new InteractionCard(interaction, handleClickDelete, handleClickEdit, handleSave,
                    tagSelectField);
            cards.put(interaction.getId(), card);
            addComponentAsFirst(card);
        }
    }

    private void addComponentAsFirst(Div component) {
        getElement().insertChild(0, component.getElement());
    }

    public void removeInteractionCard(String interactionId) {
        if (cards.containsKey(interactionId)) {
            InteractionCard cardToRemove = cards.get(interactionId);
            remove(cardToRemove);
            cards.remove(interactionId);
        }
    }

    public InteractionCard getCardById(String id) {
        return cards.get(id);
    }

    public void refreshGallery(List<InteractionDto> newFetchedInteractions) {
        List<String> currentIds = newFetchedInteractions.stream().map(InteractionDto::getId)
                .collect(Collectors.toList());

        new HashSet<>(cards.keySet()).forEach(id -> {
            if (!currentIds.contains(id)) {
                removeInteractionCard(id);
            }
        });

        newFetchedInteractions.forEach(this::addInteractionCard);
    }
}
