package com.xpi.application.views.crm.shared.components.source;

import org.vaadin.lineawesome.LineAwesomeIcon;

import com.vaadin.flow.component.textfield.TextField;
import com.xpi.application.module.crm.dto.subs.SourceDto;
import com.xpi.application.views.base.abs.AbstractForm;
import com.xpi.application.views.base.component.XFormLayout;

public class SourceForm extends AbstractForm<SourceDto> {
    /* Declare components - but do not initialize */
    private TextField nameField;

    public SourceForm() {
        super(SourceDto.class);
        initializeForm();

    }

    protected void preBuild() {
        XFormLayout formLayout = new XFormLayout();

        nameField = new TextField("Tên nguồn");
        nameField.setPrefixComponent(LineAwesomeIcon.GLOBE_SOLID.create());

        formLayout.add(nameField);

        add(formLayout);
    }

    @Override
    protected void setupBinder() {
        binder.forField(nameField)
                .asRequired("Vui lòng nhập tên nguồn")
                .bind(SourceDto::getName, SourceDto::setName);

    }
}
