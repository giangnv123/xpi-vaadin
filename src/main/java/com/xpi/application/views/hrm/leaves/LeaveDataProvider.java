package com.xpi.application.views.hrm.leaves;

import java.util.stream.Stream;

import com.vaadin.flow.data.provider.AbstractBackEndDataProvider;
import com.vaadin.flow.data.provider.Query;
import com.xpi.application.base.OffsetSizePageRequest;
import com.xpi.application.module.hrm.dto.LeaveDto;
import com.xpi.application.module.hrm.leave.LeaveService;
import com.xpi.application.module.hrm.leave.shared.LeaveFilter;

public class LeaveDataProvider extends AbstractBackEndDataProvider<LeaveDto, LeaveFilter> {

    private static final long serialVersionUID = -2772672175400690881L;

    private LeaveService service;

    public LeaveDataProvider(LeaveService service) {
        super();
        this.service = service;
    }

    @Override
    protected Stream<LeaveDto> fetchFromBackEnd(Query<LeaveDto, LeaveFilter> query) {

        OffsetSizePageRequest pageRequest = new OffsetSizePageRequest(query.getOffset(), query.getLimit());

        return service.getAll(pageRequest, query.getFilter().orElse(null)).stream();

    }

    @Override
    protected int sizeInBackEnd(Query<LeaveDto, LeaveFilter> query) {
        return service.count(query.getFilter().orElse(null));
    }
}
