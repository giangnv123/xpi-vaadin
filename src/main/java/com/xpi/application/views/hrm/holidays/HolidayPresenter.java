package com.xpi.application.views.hrm.holidays;

import com.vaadin.flow.data.provider.ConfigurableFilterDataProvider;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.xpi.application.base.ApiResponse;
import com.xpi.application.module.hrm.dto.HolidayDto;
import com.xpi.application.module.hrm.holidays.HolidayService;
import com.xpi.application.module.hrm.holidays.shared.HolidayFilter;
import com.xpi.application.views.base.utils.CustomApiResponse;
import com.xpi.application.views.hrm.holidays.views.HolidayGridView;

@SpringComponent
public class HolidayPresenter {

    private ConfigurableFilterDataProvider<HolidayDto, Void, HolidayFilter> filterDataProvider;

    HolidayDataProvider dataProvider;

    HolidayService service;

    private HolidayGridView view;

    HolidayFilter initialFilter = HolidayFilter.defaultFilter();

    /* Basic View */
    public HolidayPresenter(HolidayService service) {
        this.service = service;
        this.dataProvider = new HolidayDataProvider(service);
        filterDataProvider = dataProvider.withConfigurableFilter();
    }

    public void initView(HolidayGridView view) {
        this.view = view;
        filterDataProvider.setFilter(initialFilter);
        this.view.getGrid().setItems(filterDataProvider);
    }

    public void refreshGrid(HolidayFilter filter) {
        filterDataProvider.setFilter(filter);
        updateView();
    }

    private void updateView() {
        if (this.view != null)
            this.view.getGrid().setItems(filterDataProvider);
    }

    /* Basic CRUD */
    public CustomApiResponse<Void> delete(Long id) {
        ApiResponse<Void> response = service.delete(id);
        if (response.isSuccess())
            updateView();

        return CustomApiResponse.delete(response);
    }

    public CustomApiResponse<HolidayDto> save(HolidayDto dto) {
        CustomApiResponse<HolidayDto> response = service.save(dto);

        if (response.isSuccess())
            updateView();

        return response;
    }
}