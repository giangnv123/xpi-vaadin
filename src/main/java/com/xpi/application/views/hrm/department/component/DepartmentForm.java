package com.xpi.application.views.hrm.department.component;

import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.xpi.application.module.hrm.dto.DepartmentDto;
import com.xpi.application.views.base.abs.AbstractForm;
import com.xpi.application.views.base.component.XFormLayout;

public class DepartmentForm extends AbstractForm<DepartmentDto> {
        /* Declare components - but do not initialize */
        private TextField nameField;
        private TextField manager;
        private TextArea descriptionField;

        public DepartmentForm() {
                super(DepartmentDto.class);
                initializeForm();

        }

        protected void preBuild() {
                /* Initialize */
                XFormLayout formLayout = new XFormLayout();
                nameField = new TextField("Tên phòng ban");

                manager = new TextField("Tên quản lý");

                descriptionField = new TextArea("Mô tả");

                formLayout.add(nameField, descriptionField, manager);

                add(formLayout);
        }

        @Override
        protected void setupBinder() {
                binder.forField(nameField)
                                .asRequired("Bạn cần nhập tên phòng ban")
                                .bind(DepartmentDto::getName, DepartmentDto::setName);

                binder.forField(descriptionField)
                                .bind(DepartmentDto::getDescription, DepartmentDto::setDescription);

                binder.forField(manager)
                                .bind(DepartmentDto::getManager, DepartmentDto::setManager);
        }
}
