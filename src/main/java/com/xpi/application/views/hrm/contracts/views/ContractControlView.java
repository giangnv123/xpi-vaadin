package com.xpi.application.views.hrm.contracts.views;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.xpi.application.module.hrm.dto.ContractDto;
import com.xpi.application.views.base.abs.AbstractControlView;
import com.xpi.application.views.base.utils.CustomApiResponse;
import com.xpi.application.views.base.utils.SharedProperties;
import com.xpi.application.views.hrm.contracts.ContractPresenter;
import com.xpi.application.views.hrm.contracts.component.form_content.CreateFormLayout;
import com.xpi.application.views.hrm.contracts.shared.ContractProperties;
import com.xpi.application.views.layout.HRMLayout;

@PageTitle("Quản lý hợp đồng")
@Route(value = "hrm/contracts", layout = HRMLayout.class)
public class ContractControlView extends AbstractControlView<ContractDto> {

    private ContractProperties properties = new ContractProperties();

    private ContractPresenter presenter;

    private CreateFormLayout formLayout;

    private boolean isUpdateMode = false;

    @Override
    protected void parameterHandle(String parameter) {
        if (parameter.equals(SharedProperties.NEW_MAIN_VIEW)) {
            isUpdateMode = false;
        } else {
            isUpdateMode = true;
            fetchEntity(parameter);
        }
        showContentResult();
    }

    private void showContentResult() {
        if (currentDto == null && isUpdateMode) {
            handleFetchFailedCase();
        } else if (currentDto != null && isUpdateMode) {
            handleFetchSuccessCase();
        } else {
            setupForm();
        }
    }

    private void handleFetchSuccessCase() {
        this.entityName = properties.getEntityName();
        setupForm();
    }

    private void fetchEntity(String id) {
        currentDto = presenter.get(id);
    }

    public ContractControlView(ContractPresenter presenter) {
        this.entityName = properties.getEntityName();
        this.presenter = presenter;
        initialLayout();
    }

    private void initialLayout() {
        setSizeFull();
    }

    private void setupForm() {
        formLayout = new CreateFormLayout(this::performSaveEntity, currentDto, presenter.getAllEmployees());
        add(formLayout);
    }

    private void performSaveEntity(ContractDto dto) {
        CustomApiResponse<ContractDto> response = presenter.save(dto);

        boolean success = response.isSuccess();

        if (success) {
            UI.getCurrent().navigate(properties.getMainView());
        }

        showMessage(response.getMessage(), success);
    }

    @Override
    protected String getMainViewUrl() {
        return properties.getMainView();
    }

}
