package com.xpi.application.views.hrm.settings.views.components;

import java.time.DayOfWeek;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;

import com.vaadin.flow.component.checkbox.CheckboxGroup;
import com.vaadin.flow.component.checkbox.CheckboxGroupVariant;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H4;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.xpi.application.module.hrm.settings.WeekendsDto;

public class WeekendsSetting extends VerticalLayout {

    private Consumer<WeekendsDto> createOrUpdateWeekends;

    private WeekendsDto weekends;

    public WeekendsSetting(WeekendsDto weekends, Consumer<WeekendsDto> createOrUpdateWeekends) {
        this.createOrUpdateWeekends = createOrUpdateWeekends;
        this.weekends = weekends;
        setupLayout();
    }

    private void setupLayout() {
        add(createContainer());
    }

    private Div createContainer() {
        Div container = new Div();

        H4 title = new H4("Cài đặt ngày nghỉ cuối tuần");

        container.add(title);

        container.add(textExplain());

        container.add(setWeekends());

        return container;
    }

    private CheckboxGroup<DayOfWeek> setWeekends() {
        CheckboxGroup<DayOfWeek> weekendsField = new CheckboxGroup<>();
        weekendsField.setLabel("Ngày nghỉ cuối tuần");
        weekendsField.setItems(List.of(DayOfWeek.SATURDAY, DayOfWeek.SUNDAY));
        weekendsField.setValue(weekends.getDaysOff());
        weekendsField.addThemeVariants(CheckboxGroupVariant.LUMO_VERTICAL);
        weekendsField.addThemeVariants(CheckboxGroupVariant.LUMO_HELPER_ABOVE_FIELD);

        weekendsField.addValueChangeListener(event -> handleValueChangeWeekendsField(event.getValue()));
        return weekendsField;
    }

    private Paragraph textExplain() {
        Paragraph text = new Paragraph(
                "Chọn ngày nghỉ cuối tuần cho công ty của bạn. Ngày nghỉ sẽ tác động đến tổng số ngày công trong tháng. ");

        // text.addClassName("description");
        return text;
    }

    private void handleValueChangeWeekendsField(Set<DayOfWeek> value) {
        weekends.setDaysOff(value);
        createOrUpdateWeekends.accept(weekends);
    }

}
