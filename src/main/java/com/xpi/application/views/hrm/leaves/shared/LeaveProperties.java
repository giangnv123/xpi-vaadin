package com.xpi.application.views.hrm.leaves.shared;

import lombok.Getter;

@Getter
public class LeaveProperties {
    private final String entityName = "Đơn nghỉ phép";
    private final String navigateToCreateNewEntity = "hrm/leaves/new";
    private final String navigateToUpdateEntity = "hrm/leaves/";
    private final String mainView = "hrm/leaves";
}
