package com.xpi.application.views.hrm.insurances.component;

import java.util.function.Consumer;

import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.xpi.application.module.hrm.insurances.shared.InsuranceFilter;
import com.xpi.application.views.base.abs.AbstractFilterContainer;

public class InsuranceFilterContainer extends AbstractFilterContainer<InsuranceFilter> {
    public InsuranceFilterContainer(Consumer<InsuranceFilter> applyFilter) {
        super(applyFilter);
        buildLayout();
    }

    @Override
    protected HorizontalLayout createFilterComponent() {
        HorizontalLayout filterComponents = new HorizontalLayout();
        filterComponents.add(searchField);
        return filterComponents;
    }

    @Override
    protected void clearFilterField() {
        searchField.clear();
    }

    @Override
    protected InsuranceFilter getFilterValue() {
        return new InsuranceFilter(searchField.getValue());
    }

    @Override
    protected InsuranceFilter getDefaultFilter() {
        return InsuranceFilter.defaultFilter();
    }
}
