package com.xpi.application.views.hrm.holidays;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import com.vaadin.flow.data.provider.AbstractBackEndDataProvider;
import com.vaadin.flow.data.provider.Query;
import com.xpi.application.base.OffsetSizePageRequest;
import com.xpi.application.module.hrm.dto.HolidayDto;
import com.xpi.application.module.hrm.holidays.HolidayService;
import com.xpi.application.module.hrm.holidays.shared.HolidayFilter;

public class HolidayDataProvider extends AbstractBackEndDataProvider<HolidayDto, HolidayFilter> {

    private static final long serialVersionUID = -2772672175400690881L;

    private HolidayService service;

    public HolidayDataProvider(HolidayService service) {
        super();
        this.service = service;
    }

    @Override
    protected Stream<HolidayDto> fetchFromBackEnd(Query<HolidayDto, HolidayFilter> query) {

        OffsetSizePageRequest pageRequest = new OffsetSizePageRequest(query.getOffset(), query.getLimit());

        return Optional.ofNullable(service.getAll(pageRequest, query.getFilter().get()))
                .orElse(List.of()).stream();
    }

    @Override
    protected int sizeInBackEnd(Query<HolidayDto, HolidayFilter> query) {
        return service.count(query.getFilter().orElse(null));
    }
}
