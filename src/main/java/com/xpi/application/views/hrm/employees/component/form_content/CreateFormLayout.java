package com.xpi.application.views.hrm.employees.component.form_content;

import java.util.function.Consumer;

import com.vaadin.flow.component.html.Div;
import com.xpi.application.module.hrm.dto.EmployeeControlDto;
import com.xpi.application.module.hrm.dto.EmployeeDto;
import com.xpi.application.views.base.abs.AbstractFormControl;
import com.xpi.application.views.hrm.employees.shared.EmployeeProperties;
import com.xpi.application.views.hrm.shared.SelectDepartment;

public class CreateFormLayout extends AbstractFormControl<EmployeeDto, EmployeeControlDto> {
    private EmployeeProperties properties = new EmployeeProperties();

    private EmployeeForm employeeForm;
    private AvatarComponent avatarComponent;

    private SelectDepartment selectDepartment;

    public CreateFormLayout(Consumer<EmployeeControlDto> save,
            EmployeeDto currentDto, SelectDepartment selectDepartment) {
        super(save, currentDto);
        this.selectDepartment = selectDepartment;
        setupLayout();
    }

    /* Functionality */

    public void resetMultipartFile() {
        avatarComponent.resetMultipartFile();
    }

    @Override
    protected void populateDto() {
        employeeForm.setEntity(currentDto);
        employeeForm.setDepartmentValue(currentDto.getDepartment());

        avatarComponent.setExistingPhoto(currentDto.getPhoto());
    }

    @Override
    protected Div createContentSection() {
        Div content = new Div();
        content.addClassName("employee-form-container");
        content.add(employeeForm, avatarComponent);
        return content;
    }

    @Override
    protected void intitializeComponent() {
        employeeForm = new EmployeeForm(currentDto != null, selectDepartment);
        avatarComponent = new AvatarComponent();
    }

    @Override
    protected String getNavigatoMainView() {
        return properties.getMainViewUrl();
    }

    @Override
    protected void onSave() {
        if (!employeeForm.validateAndWrite())
            return;

        EmployeeDto employeeInforDto = employeeForm.getEntity();

        employeeInforDto.setDepartmentId(employeeForm.getDepartmentId());

        EmployeeControlDto employeeCreateDto = new EmployeeControlDto();

        employeeCreateDto.populateData(employeeInforDto, avatarComponent.getMultipartFile());

        if (currentDto != null) {
            employeeCreateDto.setId(currentDto.getId());
        }

        save.accept(employeeCreateDto);
    }
}
