package com.xpi.application.views.hrm.contracts.views;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.xpi.application.module.hrm.dto.ContractDto;
import com.xpi.application.views.base.abs.AbstractGridView;
import com.xpi.application.views.base.utils.CustomApiResponse;
import com.xpi.application.views.hrm.contracts.ContractPresenter;
import com.xpi.application.views.hrm.contracts.component.ContractFilterContainer;
import com.xpi.application.views.hrm.contracts.component.ContractGrid;
import com.xpi.application.views.hrm.contracts.shared.ContractProperties;
import com.xpi.application.views.layout.HRMLayout;

@PageTitle("Hợp đồng lao động")
@Route(value = "hrm/contracts", layout = HRMLayout.class)
public class ContractGridView extends AbstractGridView<ContractDto> {

    private ContractProperties properties = new ContractProperties();

    private ContractPresenter presenter;

    /* Components */
    private ContractFilterContainer filterContainer;
    private ContractGrid grid;

    public ContractGrid getGrid() {
        return this.grid;
    }

    public ContractGridView(ContractPresenter presenter) {
        this.entityName = properties.getEntityName();
        this.grid = new ContractGrid(this::handleClickEdit, this::handleClickDelete);
        this.presenter = presenter;
        this.presenter.initView(this);
        initialSetup();
    }

    @Override
    protected void setupLayout() {
        this.filterContainer = new ContractFilterContainer(presenter::refreshGrid);

        Div header = new Div(filterContainer, createAddEntityButton());
        header.addClassName("header");
        add(header, grid);
    }

    @Override
    protected void performDelete() {
        CustomApiResponse<Void> response = presenter.delete(currentEntity.getId());
        showMessage(response.getMessage(), response.isSuccess());
    }

    @Override
    protected void handleClickEdit(ContractDto dto) {
        currentEntity = dto;
        UI.getCurrent().navigate(properties.getNavigateToUpdateEntity() + dto.getId());
    }

    @Override
    protected void handleClickDelete(ContractDto dto) {
        currentEntity = dto;
        deleteDialog.open();
    }

    @Override
    protected void handleClickAdd() {
        UI.getCurrent().navigate(properties.getNavigateToCreateNewEntity());
    }
}
