package com.xpi.application.views.hrm.department;

import java.util.List;

import com.vaadin.flow.data.provider.ConfigurableFilterDataProvider;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.xpi.application.base.ApiResponse;
import com.xpi.application.module.hrm.department.DepartmentService;
import com.xpi.application.module.hrm.department.shared.DepartmentFilter;
import com.xpi.application.module.hrm.dto.DepartmentDto;
import com.xpi.application.views.base.utils.CustomApiResponse;
import com.xpi.application.views.hrm.department.views.DepartmentGridView;

@SpringComponent
public class DepartmentPresenter {

    private ConfigurableFilterDataProvider<DepartmentDto, Void, DepartmentFilter> filterDataProvider;

    DepartmentDataProvider dataProvider;

    DepartmentService service;

    private DepartmentGridView view;

    DepartmentFilter initialFilter = DepartmentFilter.defaultFilter();

    /* Basic View */
    public DepartmentPresenter(DepartmentService service) {
        this.service = service;
        this.dataProvider = new DepartmentDataProvider(service);
        filterDataProvider = dataProvider.withConfigurableFilter();
    }

    public void initView(DepartmentGridView view) {
        this.view = view;
        filterDataProvider.setFilter(initialFilter);
        this.view.getGrid().setItems(filterDataProvider);
    }

    public void refreshGrid(DepartmentFilter filter) {
        filterDataProvider.setFilter(filter);
        updateView();
    }

    private void updateView() {
        if (this.view != null)
            this.view.getGrid().setItems(filterDataProvider);
    }

    /* Basic CRUD */
    public CustomApiResponse<Void> delete(Long id) {
        ApiResponse<Void> response = service.delete(id);
        if (response.isSuccess())
            updateView();

        return CustomApiResponse.delete(response);
    }

    public CustomApiResponse<DepartmentDto> save(DepartmentDto dto) {
        CustomApiResponse<DepartmentDto> response = service.save(dto);

        if (response.isSuccess())
            updateView();

        return response;
    }

    /* Additional methods */
    public List<DepartmentDto> getDepartments(DepartmentFilter filter) {
        List<DepartmentDto> list = service.getAll(null, filter);
        if (list != null) {
            return list;
        }
        return List.of();
    }
}