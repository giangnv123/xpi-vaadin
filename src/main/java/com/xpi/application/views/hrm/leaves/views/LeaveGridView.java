package com.xpi.application.views.hrm.leaves.views;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.xpi.application.module.hrm.dto.LeaveDto;
import com.xpi.application.views.base.abs.AbstractGridView;
import com.xpi.application.views.base.utils.CustomApiResponse;
import com.xpi.application.views.hrm.leaves.LeavePresenter;
import com.xpi.application.views.hrm.leaves.component.LeaveFilterContainer;
import com.xpi.application.views.hrm.leaves.component.LeaveGrid;
import com.xpi.application.views.hrm.leaves.shared.LeaveProperties;
import com.xpi.application.views.layout.HRMLayout;

@PageTitle("Đơn nghỉ phép")
@Route(value = "hrm/leaves", layout = HRMLayout.class)
public class LeaveGridView extends AbstractGridView<LeaveDto> {

    private LeaveProperties properties = new LeaveProperties();

    private LeavePresenter presenter;

    /* Components */
    private LeaveFilterContainer filterContainer;
    private LeaveGrid grid;

    public LeaveGrid getGrid() {
        return this.grid;
    }

    public LeaveGridView(LeavePresenter presenter) {
        this.entityName = properties.getEntityName();
        this.grid = new LeaveGrid(this::handleClickEdit, this::handleClickDelete);
        this.presenter = presenter;
        this.presenter.initView(this);
        initialSetup();
    }

    @Override
    protected void setupLayout() {
        this.filterContainer = new LeaveFilterContainer(presenter::refreshGrid);

        Div header = new Div(filterContainer, createAddEntityButton());
        header.addClassName("header");
        add(header, grid);
    }

    @Override
    protected void performDelete() {
        CustomApiResponse<Void> response = presenter.delete(currentEntity.getId());
        showMessage(response.getMessage(), response.isSuccess());
    }

    @Override
    protected void handleClickEdit(LeaveDto dto) {
        currentEntity = dto;
        UI.getCurrent().navigate(properties.getNavigateToUpdateEntity() + dto.getId());
    }

    @Override
    protected void handleClickDelete(LeaveDto dto) {
        currentEntity = dto;
        deleteDialog.open();
    }

    @Override
    protected void handleClickAdd() {
        UI.getCurrent().navigate(properties.getNavigateToCreateNewEntity());
    }
}
