package com.xpi.application.views.hrm.workdays.views;

import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.xpi.application.module.hrm.workday.shared.EmployeeWorkdayDetailsDto;
import com.xpi.application.module.hrm.workday.shared.WorkingDayOfMonthOverView;
import com.xpi.application.views.base.abs.AbstractView;
import com.xpi.application.views.base.component.XDialog;
import com.xpi.application.views.base.utils.CustomApiResponse;
import com.xpi.application.views.hrm.workdays.WorkdayPresenter;
import com.xpi.application.views.hrm.workdays.component.Form;
import com.xpi.application.views.hrm.workdays.component.WorkdayFilterContainer;
import com.xpi.application.views.hrm.workdays.component.WorkdayGrid;
import com.xpi.application.views.layout.HRMLayout;

@PageTitle("Ngày công")
@Route(value = "hrm/workdays", layout = HRMLayout.class)
public class WorkdayGridView extends AbstractView {

    private XDialog dialog;
    private Form form = new Form();

    private EmployeeWorkdayDetailsDto currentEntity;

    private WorkdayPresenter presenter;

    /* Components */
    private WorkdayFilterContainer filterContainer;
    private WorkdayGrid grid;

    public WorkdayGrid getGrid() {
        return this.grid;
    }

    public WorkdayGridView(WorkdayPresenter presenter) {
        this.grid = new WorkdayGrid(this::handleClickEdit,
                presenter.getWorkingDaysSummary());
        this.presenter = presenter;
        this.presenter.initView(this);
        initialSetup();
    }

    public void refreshOverView(WorkingDayOfMonthOverView newOverView) {
        this.grid.refreshOverView(newOverView);
    }

    private void initialSetup() {
        setSizeFull();
        setupLayout();
    }

    protected void setupLayout() {
        setupControlDialog();

        this.filterContainer = new WorkdayFilterContainer(presenter::refreshGrid);

        Div header = new Div(filterContainer);
        header.addClassName("header");
        add(header, grid);
    }

    private void handleClickEdit(EmployeeWorkdayDetailsDto dto) {
        currentEntity = dto;
        form.setEntity(dto);
        dialog.open();
    }

    protected void setupControlDialog() {
        dialog = new XDialog("Chỉnh sửa ngày công", this::performSave);
        dialog.add(form);
    }

    private void performSave() {
        if (!form.validateAndWrite())
            return;

        EmployeeWorkdayDetailsDto dto = form.getEntity();

        dto.setDate(currentEntity.getDate());
        dto.setEmployeeId(currentEntity.getEmployeeId());
        dto.setId(currentEntity.getId());

        if (dto.getEmployeeId() == null) {
            showErrorMessage("EmployeeId is missing");
            return;
        }

        CustomApiResponse<EmployeeWorkdayDetailsDto> response = presenter.saveWorkdayDetails(dto);

        if (response.isSuccess()) {
            dialog.close();
        }
        showMessage(response.getMessage(), response.isSuccess());
    }


}
