package com.xpi.application.views.hrm.insurances.component;

import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.function.Consumer;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.xpi.application.module.hrm.dto.InsuranceDto;
import com.xpi.application.views.base.abs.AbstractGrid;
import com.xpi.application.views.base.views.AvatarHelper;
import com.xpi.application.views.base.views.ViewHelper;

public class InsuranceGrid extends AbstractGrid<InsuranceDto> {

    public InsuranceGrid(Consumer<InsuranceDto> clickEdit, Consumer<InsuranceDto> clickDelete) {
        super(clickEdit, clickDelete);
        initialSetup();
    }

    @Override
    protected void setupGrid() {
        addColumn(displayBasicEmployeeInforWithAvatar()).setHeader("Nhân viên").setAutoWidth(true);
        addColumn(dto -> dto.getInsuranceNumber()).setHeader("Mã số bảo hiểm").setAutoWidth(true);
        addColumn(dto -> dto.getInsuranceCompany()).setHeader("Công ty bảo hiểm").setAutoWidth(true);
        addColumn(displayStatus()).setHeader("Loại bảo hiểm").setAutoWidth(true);
        addColumn(dto -> dto.getPlaceOfProvision()).setHeader("Nơi cấp").setAutoWidth(true);
        addColumn(dto -> dto.getContributionRate() + "%").setHeader("Phần trăm lương").setAutoWidth(true);
        addColumn(dto -> ViewHelper.formatSalary(dto.getContributeAmount()) + " VND")
                .setHeader("Lương tham gia")
                .setAutoWidth(true);
        addColumn(displayContributeAmoount()).setHeader("Phí bảo hiểm").setAutoWidth(true);
        addColumn(dateDisplay()).setHeader("Ngày tham gia").setAutoWidth(true);
    }

    private ComponentRenderer<Component, InsuranceDto> displayBasicEmployeeInforWithAvatar() {
        return new ComponentRenderer<>(dto -> {
            return AvatarHelper.displayBasicEmployeeInforWithAvatar(dto.getEmployee());
        });
    }

    private ComponentRenderer<Component, InsuranceDto> displayStatus() {
        return new ComponentRenderer<>(dto -> {
            Span status = new Span(dto.getType().toReadableString());
            status.addClassName("status");
            status.addClassName(dto.getType().toString().toLowerCase());
            return status;
        });
    }

    private ComponentRenderer<Component, InsuranceDto> displayContributeAmoount() {
        return new ComponentRenderer<>(dto -> {
            String value = Optional.ofNullable(dto.getContributeAmount())
                    .map(s -> ViewHelper.formatSalary(s * dto.getContributionRate() / 100)).orElse("");
            return new Span(value + " VND");
        });
    }

    private ComponentRenderer<Component, InsuranceDto> dateDisplay() {
        return new ComponentRenderer<>(dto -> {
            Div container = new Div();
            container.addClassName("flex-col-layout");

            container.add(new Span(dto.getIssueDate().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))));
            return container;
        });
    }

}
