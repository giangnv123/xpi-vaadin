package com.xpi.application.views.hrm.workdays;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.flow.data.provider.ConfigurableFilterDataProvider;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.xpi.application.base.ApiResponse;
import com.xpi.application.module.hrm.shared.HelperService;
import com.xpi.application.module.hrm.workday.WorkDayService;
import com.xpi.application.module.hrm.workday.shared.EmployeeWorkdayDetailsDto;
import com.xpi.application.module.hrm.workday.shared.EmployeeWorkdayDto;
import com.xpi.application.module.hrm.workday.shared.WorkdayFilter;
import com.xpi.application.module.hrm.workday.shared.WorkingDayOfMonthOverView;
import com.xpi.application.views.base.utils.CustomApiResponse;
import com.xpi.application.views.hrm.workdays.views.WorkdayGridView;

import lombok.Data;

@Data
@SpringComponent
public class WorkdayPresenter {
    private static WorkdayFilter defaultFilter = WorkdayFilter.defaultFilter();

    private ConfigurableFilterDataProvider<EmployeeWorkdayDto, Void, WorkdayFilter> filterDataProvider;

    private WorkdayFilter currentFilter;

    WorkDayProvider dataProvider;

    WorkDayService service;

    @Autowired
    private HelperService helperService;

    private WorkdayGridView view;

    private final String entityName = "Workday";

    /* Basic View */
    public WorkdayPresenter(WorkDayService service) {
        this.service = service;
        this.dataProvider = new WorkDayProvider(service);
        filterDataProvider = dataProvider.withConfigurableFilter();
    }

    public void initView(WorkdayGridView view) {
        this.view = view;
        currentFilter = defaultFilter;
        filterDataProvider.setFilter(defaultFilter);
        this.view.getGrid().setItems(filterDataProvider);
    }

    public void refreshGrid(WorkdayFilter filter) {
        filterDataProvider.setFilter(filter);
        currentFilter = filter;
        updateView(filter);
    }

    private void updateView(WorkdayFilter filter) {
        if (this.view != null) {
            this.view.refreshOverView(getWorkingDaysSummary());
            this.view.getGrid().setDataProvider(filterDataProvider);
        }
    }

    public WorkingDayOfMonthOverView getWorkingDaysSummary() {
        return service.getWorkingDaysOverView(currentFilter);
    }

    public CustomApiResponse<EmployeeWorkdayDetailsDto> saveWorkdayDetails(EmployeeWorkdayDetailsDto dto) {
        ApiResponse<EmployeeWorkdayDetailsDto> response;
        Boolean isUpdated = dto.getId() != null;
        if (isUpdated) {
            response = service.updateWorkdayDetails(dto.getId(), dto);
        } else {
            response = service.createWorkdayDetails(dto);
        }
        if (response.isSuccess())
            updateView(currentFilter);
        return CustomApiResponse.save(response, isUpdated, response.getData());
    }

}