package com.xpi.application.views.hrm.employees.component.form_content;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import org.vaadin.lineawesome.LineAwesomeIcon;

import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.validator.EmailValidator;
import com.xpi.application.module.hrm.dto.DepartmentDto;
import com.xpi.application.module.hrm.dto.EmployeeDto;
import com.xpi.application.module.hrm.employees.shared.EmpStatus;
import com.xpi.application.module.hrm.employees.shared.Gender;
import com.xpi.application.views.base.abs.AbstractForm;
import com.xpi.application.views.base.component.XFormLayout;
import com.xpi.application.views.base.utils.ColorUtils;
import com.xpi.application.views.base.views.ViewHelper;
import com.xpi.application.views.hrm.shared.SelectDepartment;

public class EmployeeForm extends AbstractForm<EmployeeDto> {

        private Boolean updateMode = true;

        /* Personal Infor */
        private TextField fullnameField;
        private TextField emailField;
        private TextField phoneField;
        private TextField idNumber;
        private DatePicker birthdatePicker;
        private TextArea address;
        private Select<Gender> selectGender = new Select<>();

        /* Company infor */
        private TextField designation;
        private NumberField salaryField;
        private IntegerField daysOffField;
        private TextField manager;
        private DatePicker dateStart;
        private DatePicker dateEnd;
        private Select<EmpStatus> selectStatus;

        private SelectDepartment selectDepartment;

        public EmployeeForm(Boolean updateMode, SelectDepartment selectDepartment) {
                super(EmployeeDto.class);
                this.updateMode = updateMode;
                this.selectDepartment = selectDepartment;
                initializeForm();
        }

        protected void preBuild() {
                Div personalInforContainer = createPersonalInforContainer();
                Div companyInforContainer = createCompanyInforContainer();
                addClassName("employee-main-layout");
                add(personalInforContainer, companyInforContainer);
        }

        private Div createPersonalInforContainer() {

                Div employeeContainer = new Div();
                employeeContainer.add(ViewHelper.titleWithIcon("Thông tin cá nhân", LineAwesomeIcon.USER,
                                ColorUtils.WHITE, ColorUtils.BG_SUCCESS));
                employeeContainer.addClassName("card");

                XFormLayout formLayout = new XFormLayout();

                fullnameField = new TextField("Họ và tên");
                emailField = new TextField("Địa chỉ email");
                phoneField = new TextField("Số điện thoại");
                idNumber = new TextField("Số căn cước công dân");

                birthdatePicker = new DatePicker();
                birthdatePicker.setMax(LocalDate.now());

                address = new TextArea("Địa chỉ");
                selectGender = new Select<>();

                address.setPlaceholder("Quận, Xã, Phường,...");
                birthdatePicker.setLabel("Ngày sinh");
                birthdatePicker.setPlaceholder("mm/dd/yyyy");

                selectGender.setPlaceholder("Giới tính");
                selectGender.setLabel("Giới tính");
                selectGender.setItems(Gender.values());
                selectGender.setItemLabelGenerator(gender -> gender.toReadableString());

                formLayout.add(fullnameField, emailField, phoneField, idNumber,
                                birthdatePicker, selectGender, address);

                formLayout.setColspan(address, 2);

                employeeContainer.add(formLayout);
                return employeeContainer;
        }

        private Div createCompanyInforContainer() {
                Div companyInforContainer = new Div();
                companyInforContainer.addClassName("card");
                companyInforContainer.add(ViewHelper.titleWithIcon("Thông tin công việc",
                                LineAwesomeIcon.BRIEFCASE_SOLID,
                                ColorUtils.WHITE, ColorUtils.BG_SUCCESS));

                XFormLayout formLayout = new XFormLayout();

                salaryField = new NumberField("Mức lương");
                salaryField.setSuffixComponent(new Span("VND"));
                salaryField.addValueChangeListener(event -> {
                        salaryField.setHelperText(ViewHelper.formatSalary(event.getValue()) + " VND");
                });

                daysOffField = new IntegerField("Số ngày nghỉ phép trong năm");
                daysOffField.setValue(12);
                daysOffField.setStepButtonsVisible(true);
                daysOffField.setMax(50);

                selectStatus = new Select<>();
                List<EmpStatus> statuses = List.of(EmpStatus.values()).stream()
                                .filter(status -> status != EmpStatus.Deleted)
                                .collect(Collectors.toList());
                selectStatus.setItems(statuses);
                selectStatus.setItemLabelGenerator(EmpStatus::toReadableString);
                selectStatus.setLabel("Trạng thái");

                designation = new TextField("Chức danh");
                designation.setPlaceholder("Chuyên viên bán hàng....");

                manager = new TextField("Quản lý");
                manager.setReadOnly(true);

                dateStart = new DatePicker();
                dateStart.setPlaceholder("mm/dd/yyyy");
                dateStart.setLabel("Ngày bắt đầu");

                dateEnd = new DatePicker();
                dateEnd.setPlaceholder("mm/dd/yyyy");
                dateEnd.setLabel("Ngày kết thúc");

                formLayout.add(selectDepartment, designation, salaryField, daysOffField, dateStart);

                if (updateMode) {
                        formLayout.add(dateEnd);
                }

                formLayout.add(manager);

                if (updateMode)
                        formLayout.add(selectStatus);

                companyInforContainer.add(formLayout);

                return companyInforContainer;

        }

        @Override
        protected void setupBinder() {
                setupPersonalInforBinder();
                setupCompanyInforBinder();

        }

        private void setupPersonalInforBinder() {

                binder.forField(fullnameField)
                                .asRequired("Điền họ và tên")
                                // .withValidator(value -> value.matches("[a-zA-Z]+"),
                                // "Lastname must contain only letters")
                                .bind(EmployeeDto::getFullname, EmployeeDto::setFullname);

                binder.forField(emailField)
                                .asRequired("Hãy nhập email")
                                .withValidator(new EmailValidator("Hãy nhập đúng địa chỉ email"))
                                .bind(EmployeeDto::getEmail, EmployeeDto::setEmail);

                binder.forField(phoneField)
                                .asRequired("Hãy nhập số điện thoại")
                                .withValidator(value -> value.matches("\\d+"), "Số điện thoại không hợp lệ")
                                .withValidator(value -> value.length() > 9 && value.length() < 11,
                                                "Số điện thoại phải bằng 10 hoặc 11 số")
                                .bind(EmployeeDto::getPhone, EmployeeDto::setPhone);

                binder.forField(idNumber)
                                .asRequired("Hãy nhập số căn cước công dân")
                                .withValidator(value -> value.matches("\\d+"), "Hãy nhập đúng căn cước công dân")
                                .withValidator(value -> value.length() == 12, "Căn cước công dân phải có 12 số")
                                .bind(EmployeeDto::getIdNumber, EmployeeDto::setIdNumber);

                binder.forField(address)
                                .asRequired("Hãy nhập địa chỉ")
                                .bind(EmployeeDto::getAddress, EmployeeDto::setAddress);

                binder.forField(selectGender)
                                .asRequired("Chọn giới tính")
                                .bind(EmployeeDto::getGender, EmployeeDto::setGender);

                binder.forField(birthdatePicker)
                                .asRequired("Chọn ngày sinh")
                                .bind(EmployeeDto::getDateOfBirth, EmployeeDto::setDateOfBirth);
        }

        private void setupCompanyInforBinder() {
                binder.forField(designation).asRequired("Hãy nhập chức vụ")
                                .bind(EmployeeDto::getDesignation, EmployeeDto::setDesignation);

                binder.forField(dateStart).asRequired("Hãy chọn ngày bắt đầu")
                                .bind(EmployeeDto::getDateStart, EmployeeDto::setDateStart);

                binder.forField(salaryField)
                                .bind(EmployeeDto::getSalary, EmployeeDto::setSalary);

                binder.forField(daysOffField)
                                .bind(EmployeeDto::getDaysOff, EmployeeDto::setDaysOff);

                if (updateMode) {
                        binder.forField(selectStatus).bind(EmployeeDto::getStatus, EmployeeDto::setStatus);
                        binder.forField(dateEnd).bind(EmployeeDto::getDateEnd, EmployeeDto::setDateEnd);
                }
        }

        public Long getDepartmentId() {
                return selectDepartment.getDepartmentId();
        }

        public void setDepartmentValue(DepartmentDto dto) {
                selectDepartment.setValue(dto);
        }
}
