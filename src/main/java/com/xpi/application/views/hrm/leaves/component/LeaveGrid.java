package com.xpi.application.views.hrm.leaves.component;

import java.time.temporal.ChronoUnit;
import java.util.function.Consumer;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H5;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.xpi.application.module.hrm.dto.LeaveDto;
import com.xpi.application.views.base.abs.AbstractGrid;
import com.xpi.application.views.base.views.AvatarHelper;
import com.xpi.application.views.base.views.ViewHelper;

public class LeaveGrid extends AbstractGrid<LeaveDto> {

    public LeaveGrid(Consumer<LeaveDto> clickEdit, Consumer<LeaveDto> clickDelete) {
        super(clickEdit, clickDelete);
        initialSetup();
    }

    @Override
    protected void setupGrid() {
        addColumn(displayEmployeeInfor()).setHeader("Nhân viên").setAutoWidth(true);
        addColumn(createTitleRenderer()).setHeader("Ghi chú").setAutoWidth(true);
        addColumn(dto -> dto.getDateStart()).setHeader("Từ ngày").setAutoWidth(true);
        addColumn(dto -> dto.getDateEnd()).setHeader("Đến ngày").setAutoWidth(true);
        addColumn(dayDisplay()).setHeader("Số ngày nghỉ").setAutoWidth(true);
        addColumn(displayPaidStatus()).setHeader("Nghỉ phép").setAutoWidth(true);
        addColumn(displayStatus()).setHeader("Trạng thái").setAutoWidth(true);
    }

    private ComponentRenderer<Component, LeaveDto> displayEmployeeInfor() {
        return new ComponentRenderer<>(dto -> {
            return AvatarHelper.displayBasicEmployeeInforWithAvatar(dto.getEmployee());
        });
    }

    private ComponentRenderer<Component, LeaveDto> createTitleRenderer() {
        return new ComponentRenderer<>(dto -> {
            H5 title = new H5(dto.getTitle());
            title.addClassName("break-line-cell");

            Span description = new Span(dto.getDescription());
            description.addClassName("break-line-cell");

            Div container = new Div();
            container.addClassName("flex-col-layout");
            container.add(title, description);
            return container;
        });
    }

    private ComponentRenderer<Component, LeaveDto> dayDisplay() {
        return new ComponentRenderer<>(dto -> {
            long daysOff = ChronoUnit.DAYS.between(dto.getDateStart(), dto.getDateEnd()) + 1;

            Div container = new Div();
            container.add(new Span(String.valueOf(daysOff)));
            return container;
        });
    }

    private ComponentRenderer<Component, LeaveDto> displayStatus() {
        return new ComponentRenderer<>(dto -> {
            return ViewHelper.createStatusComponent(dto.getStatus().toReadableString(),
                    dto.getStatus().name().toLowerCase());
        });
    }

    private ComponentRenderer<Component, LeaveDto> displayPaidStatus() {
        return new ComponentRenderer<>(dto -> {
            String className = dto.getPaidLeave() ? "Paid" : "Unpaid";
            String text = dto.getPaidLeave() ? "Có phép" : "Không phép";
            return ViewHelper.createStatusComponent(text, className.toLowerCase());
        });
    }

}
