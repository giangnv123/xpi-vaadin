package com.xpi.application.views.hrm.employees.component.form_content;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

import org.springframework.web.multipart.MultipartFile;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.upload.SucceededEvent;
import com.vaadin.flow.component.upload.Upload;
import com.vaadin.flow.component.upload.receivers.FileData;
import com.vaadin.flow.component.upload.receivers.MultiFileBuffer;
import com.xpi.application.views.base.utils.SharedProperties;
import com.xpi.application.views.base.views.AvatarHelper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AvatarComponent extends Div {

    private final MultiFileBuffer multiFileBuffer = new MultiFileBuffer();

    private CustomMultipartFile multipartFile;
    private String existingPhoto = "";

    private Image defaultAvatar = new Image(SharedProperties.DEFAULT_AVATAR, "Default Avatar");
    private Upload upload = new Upload(multiFileBuffer);
    private Div avatarContainer = new Div();

    public AvatarComponent() {
        setupComponent();
    }

    private void setupComponent() {
        avatarContainer.addClassNames("avatar-side", "card");
        defaultAvatar.setClassName("employee-avatar-create");
        configureUpload();
        avatarContainer.add(defaultAvatar, upload);
        add(avatarContainer);
    }

    private void configureUpload() {
        Button uploadButton = new Button("Cập nhật avatar");
        upload.setUploadButton(uploadButton);
        upload.setDropAllowed(true);
        upload.addSucceededListener(event -> handleFileUpload(event));
    }

    private void handleFileUpload(SucceededEvent event) {
        FileData savedFileData = multiFileBuffer.getFileData(event.getFileName());
        File file = savedFileData.getFile();
        try {
            multipartFile = new CustomMultipartFile(savedFileData.getFileName(), file);
            updateAvatarContainer(savedFileData);
        } catch (IOException e) {
            log.error("Error converting file to MultipartFile: {}", e.getMessage(), e);
        }
    }

    public void resetMultipartFile() {
        multipartFile = null; // Clear the current reference
        Collection<String> fileNames = multiFileBuffer.getFiles();

        if (!fileNames.isEmpty()) {
            String recentFileName = fileNames.stream().reduce((first, second) -> second).orElse(null);
            if (recentFileName != null) {
                FileData fileData = multiFileBuffer.getFileData(recentFileName);
                if (fileData != null && fileData.getFile().exists()) {
                    try {
                        multipartFile = new CustomMultipartFile(fileData.getFileName(), fileData.getFile());
                    } catch (IOException e) {
                        log.error("Error resetting MultipartFile: {}", e.getMessage(), e);
                    }
                }
            }
        }
    }

    private void updateAvatarContainer(FileData savedFileData) throws IOException {
        Image uploadedImage = AvatarHelper.createPreviewImage(savedFileData.getFile());
        if (uploadedImage != null) {
            avatarContainer.removeAll();
            avatarContainer.add(uploadedImage, upload);
        }
    }

    public void setExistingPhoto(String photo) {
        existingPhoto = photo;
        if (existingPhoto == null)
            return;
        if (!existingPhoto.isEmpty()) {
            defaultAvatar.setSrc(SharedProperties.API_SERVE_FILE + existingPhoto);
        }
    }

    public MultipartFile getMultipartFile() {
        return multipartFile;
    }
}
