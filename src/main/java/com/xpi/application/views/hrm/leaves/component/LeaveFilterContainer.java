package com.xpi.application.views.hrm.leaves.component;

import java.util.function.Consumer;

import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.orderedlayout.FlexComponent.Alignment;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.xpi.application.module.hrm.leave.shared.LeaveFilter;
import com.xpi.application.module.hrm.leave.shared.LeaveStatus;
import com.xpi.application.views.base.abs.AbstractFilterContainer;

public class LeaveFilterContainer extends AbstractFilterContainer<LeaveFilter> {
    private static final String LEAVE_STATUS_PLACEHOLDER = "Trạng thái";
    private static final String PAID_CHECKBOX_LABEL = "Tính phép";

    private ComboBox<LeaveStatus> leaveStatusField;
    private Checkbox isPaidField;

    public LeaveFilterContainer(Consumer<LeaveFilter> applyFilter) {
        super(applyFilter);
        buildLayout();
    }

    @Override
    protected HorizontalLayout createFilterComponent() {
        initializeLeaveStatusField();
        initializeIsPaidField();

        HorizontalLayout filterComponents = new HorizontalLayout(searchField, leaveStatusField, isPaidField);
        filterComponents.setVerticalComponentAlignment(Alignment.END, searchField, leaveStatusField, isPaidField);
        return filterComponents;
    }

    private void initializeLeaveStatusField() {
        leaveStatusField = new ComboBox<>();
        leaveStatusField.setItems(LeaveStatus.values());
        leaveStatusField.setItemLabelGenerator(LeaveStatus::toReadableString);
        leaveStatusField.setPlaceholder(LEAVE_STATUS_PLACEHOLDER);
        leaveStatusField.addValueChangeListener(e -> applyFilters());
    }

    private void initializeIsPaidField() {
        isPaidField = new Checkbox(PAID_CHECKBOX_LABEL);
        isPaidField.addValueChangeListener(e -> applyFilters());
    }

    @Override
    protected void clearFilterField() {
        searchField.clear();
        leaveStatusField.clear();
        isPaidField.clear();
    }

    @Override
    protected LeaveFilter getFilterValue() {
        return new LeaveFilter(searchField.getValue(), leaveStatusField.getValue(), isPaidField.getValue());
    }

    @Override
    protected LeaveFilter getDefaultFilter() {
        return LeaveFilter.defaultFilter();
    }
}
