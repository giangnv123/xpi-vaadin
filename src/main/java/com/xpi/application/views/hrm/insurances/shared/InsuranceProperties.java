package com.xpi.application.views.hrm.insurances.shared;

import lombok.Getter;

@Getter
public class InsuranceProperties {
    private final String entityName = "Bảo hiểm xã hội";
    private final String navigateToCreateNewEntity = "hrm/insurances/new";
    private final String navigateToUpdateEntity = "hrm/insurances/";
    private final String mainView = "hrm/insurances";
}
