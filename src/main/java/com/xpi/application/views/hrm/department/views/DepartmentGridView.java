package com.xpi.application.views.hrm.department.views;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.xpi.application.module.hrm.dto.DepartmentDto;
import com.xpi.application.views.base.abs.AbstractBasicGridView;
import com.xpi.application.views.base.component.XConfirmDialog;
import com.xpi.application.views.base.component.XDialog;
import com.xpi.application.views.base.utils.CustomApiResponse;
import com.xpi.application.views.hrm.department.DepartmentPresenter;
import com.xpi.application.views.hrm.department.component.DepartmentFilterContainer;
import com.xpi.application.views.hrm.department.component.DepartmentGrid;
import com.xpi.application.views.hrm.department.component.DepartmentForm;
import com.xpi.application.views.layout.HRMLayout;

@PageTitle("Phòng ban")
@Route(value = "hrm/departments", layout = HRMLayout.class)
public class DepartmentGridView extends AbstractBasicGridView<DepartmentDto, Long> {

    private DepartmentPresenter presenter;

    /* Components */
    private DepartmentFilterContainer filterContainer;
    private DepartmentGrid grid;
    /* Components */

    private DepartmentForm form = new DepartmentForm();

    public DepartmentGrid getGrid() {
        return this.grid;
    }

    public DepartmentGridView(DepartmentPresenter presenter) {
        this.entityName = "Phòng ban";
        grid = new DepartmentGrid(this::handleClickEdit, this::handleClickDelete);
        this.presenter = presenter;
        this.presenter.initView(this);
        initialSetup();
    }

    @Override
    protected Div createHeaderSection() {
        filterContainer = new DepartmentFilterContainer(presenter::refreshGrid);
        Div header = new Div(filterContainer, createAddEntityButton());
        header.addClassName("header");
        return header;
    }

    @Override
    protected Component createContentSection() {
        return grid;
    }

    private void performSave() {
        if (!form.validateAndWrite())
            return;

        DepartmentDto dto = form.getEntity();

        Boolean isSuccess = false;

        isSuccess = save(dto);

        if (isSuccess) {
            controlDialog.close();
            form.resetForm();
        }
    }

    protected Boolean save(DepartmentDto dto) {
        CustomApiResponse<DepartmentDto> response = presenter.save(dto);
        Boolean isSuccess = response.isSuccess();
        showMessage(response.getMessage(), isSuccess);
        return isSuccess;
    }

    protected void setupDialog() {
        setupControlDialog();
        deleteDialog = new XConfirmDialog(entityName, this::performDelete);
    }

    protected void setupControlDialog() {
        controlDialog = new XDialog(entityName, this::performSave);
        controlDialog.add(form);
    }

    protected void performDelete() {
        CustomApiResponse<Void> response = presenter.delete(currentId);
        showMessage(response.getMessage(), response.isSuccess());
    }

    protected void handleClickEdit(DepartmentDto dto) {
        form.setEntity(dto);
        controlDialog.open();
    }

    protected void handleClickDelete(DepartmentDto dto) {
        currentId = dto.getId();
        deleteDialog.open();
    }

    protected void handleClickAdd() {
        form.resetForm();
        controlDialog.open();
        currentId = null;
    }
}
