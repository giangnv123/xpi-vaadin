package com.xpi.application.views.hrm.contracts;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.flow.data.provider.ConfigurableFilterDataProvider;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.xpi.application.base.ApiResponse;
import com.xpi.application.module.hrm.contracts.ContractService;
import com.xpi.application.module.hrm.contracts.shared.ContractFilter;
import com.xpi.application.module.hrm.dto.ContractDto;
import com.xpi.application.module.hrm.dto.EmployeeDto;
import com.xpi.application.module.hrm.shared.HelperService;
import com.xpi.application.views.base.utils.CustomApiResponse;
import com.xpi.application.views.hrm.contracts.views.ContractGridView;

import lombok.Data;

@Data
@SpringComponent
public class ContractPresenter {
    private static ContractFilter defaultFilter = ContractFilter.defaultFilter();

    private ConfigurableFilterDataProvider<ContractDto, Void, ContractFilter> filterDataProvider;

    ContractDataProvider dataProvider;

    ContractService service;

    @Autowired
    private HelperService helperService;

    private ContractGridView view;

    private final String entityName = "Contract";

    /* Basic View */
    public ContractPresenter(ContractService service) {
        this.service = service;
        this.dataProvider = new ContractDataProvider(service);
        filterDataProvider = dataProvider.withConfigurableFilter();
    }

    public void initView(ContractGridView view) {
        this.view = view;
        filterDataProvider.setFilter(defaultFilter);
        this.view.getGrid().setItems(filterDataProvider);
    }

    public void refreshGrid(ContractFilter filter) {
        filterDataProvider.setFilter(filter);
    }

    private void updateView() {
        if (this.view != null)
            this.view.getGrid().setDataProvider(filterDataProvider);
    }

    /* Basic CRUD */
    public CustomApiResponse<Void> delete(String id) {
        ApiResponse<Void> response = service.delete(id);
        Boolean isSuccess = response.isSuccess();
        if (isSuccess)
            updateView();

        return CustomApiResponse.delete(response);
    }

    public CustomApiResponse<ContractDto> save(ContractDto dto) {
        CustomApiResponse<ContractDto> response = service.save(dto);
        if (response.isSuccess())
            updateView();
        return response;
    }

    public ContractDto get(String id) {
        return service.get(id);
    }

    /* Additional methods */

    public List<EmployeeDto> getAllEmployees() {
        return helperService.getAllEmployees();
    }
}