package com.xpi.application.views.hrm.workdays.component;


import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.data.binder.ValidationResult;
import com.xpi.application.module.hrm.workday.shared.EmployeeWorkdayDetailsDto;
import com.xpi.application.module.hrm.workday.shared._enum.WorkDayType;
import com.xpi.application.views.base.abs.AbstractForm;
import com.xpi.application.views.base.component.XFormLayout;

public class Form extends AbstractForm<EmployeeWorkdayDetailsDto> {
    /* Declare components - but do not initialize */
    private TextArea note;

    private NumberField count;

    private ComboBox<WorkDayType> selectType;

    public Form() {
        super(EmployeeWorkdayDetailsDto.class);
        initializeForm();

    }

    protected void preBuild() {

        /* Initialize */
        XFormLayout formLayout = new XFormLayout();

        selectType = new ComboBox<>();
        selectType.setItems(WorkDayType.getEditableTypes());
        selectType.setItemLabelGenerator(WorkDayType::toString);

        selectType.addValueChangeListener(event -> {
            handleSelectType(event.getValue());
        });

        count = new NumberField("Ngày công");
        count.setMin(0);
        count.setReadOnly(false);
        count.setMax(5);
        count.setHelperText("Ngày công chỉ có giá trị từ 0 - 5");
        count.setStepButtonsVisible(true);

        note = new TextArea("Ghi chú");

        formLayout.add(selectType, count, note);

        add(formLayout);
    }

    private void handleSelectType(WorkDayType type) {
        if (note.getValue() == null)
            note.setValue(type.toString());

        if (WorkDayType.getUnChangeTypes().contains(type))
            count.setReadOnly(true);

        if (type == WorkDayType.PaidLeaveDaysOff)
            count.setValue(1.0);

        if (type == WorkDayType.WorkingDay) {
            enableAndValidateCount();
        } else if (WorkDayType.getUncountTypes().contains(type)) {
            resetCount();
        }
    }

    private void enableAndValidateCount() {
        count.setReadOnly(false);
        count.setValue(1.0);
        binder.forField(count)
                .asRequired("Hãy nhập ngày công")
                .bind(EmployeeWorkdayDetailsDto::getCount, EmployeeWorkdayDetailsDto::setCount);
    }

    private void resetCount() {
        count.setValue(0.0);
        binder.forField(count)
                .withValidator((value, context) -> ValidationResult.ok())
                .bind(EmployeeWorkdayDetailsDto::getCount, EmployeeWorkdayDetailsDto::setCount);
    }

    @Override
    protected void setupBinder() {
        binder.forField(count)
                .bind(EmployeeWorkdayDetailsDto::getCount, EmployeeWorkdayDetailsDto::setCount);

        binder.forField(selectType)
                .asRequired()
                .bind(EmployeeWorkdayDetailsDto::getType, EmployeeWorkdayDetailsDto::setType);

        binder.forField(note)
                .bind(EmployeeWorkdayDetailsDto::getNote, EmployeeWorkdayDetailsDto::setNote);
    }
}
