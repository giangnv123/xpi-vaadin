package com.xpi.application.views.hrm.employees.shared;

import lombok.Getter;

@Getter
public class EmployeeProperties {

    private final String entityName = "nhân viên";
    private final String newViewUrl = "hrm/employees/new";
    private final String updateViewUrl = "hrm/employees/";
    private final String mainViewUrl = "hrm/employees";
}
