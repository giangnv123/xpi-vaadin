package com.xpi.application.views.hrm.leaves.views;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.xpi.application.module.hrm.dto.LeaveDto;
import com.xpi.application.views.base.abs.AbstractControlView;
import com.xpi.application.views.base.utils.CustomApiResponse;
import com.xpi.application.views.hrm.leaves.LeavePresenter;
import com.xpi.application.views.hrm.leaves.component.form_content.CreateFormLayout;
import com.xpi.application.views.hrm.leaves.shared.LeaveProperties;
import com.xpi.application.views.layout.HRMLayout;

@PageTitle("Quản lý đơn nghỉ phép")
@Route(value = "hrm/leaves", layout = HRMLayout.class)
public class LeaveControlView extends AbstractControlView<LeaveDto> {

    private LeaveProperties properties = new LeaveProperties();

    private LeavePresenter presenter;
    private CreateFormLayout formLayout;
    private boolean isUpdateMode = false;

    @Override
    protected void parameterHandle(String parameter) {
        if (parameter.equals("new")) {
            isUpdateMode = false;
        } else {
            isUpdateMode = true;
            fetchEntity(parameter);
        }
        showContentResult();
    }

    private void showContentResult() {
        if (currentDto == null && isUpdateMode) {
            handleFetchFailedCase();
        } else if (currentDto != null && isUpdateMode) {
            handleFetchSuccessCase();
        } else {
            setupForm();
        }
    }

    private void handleFetchSuccessCase() {
        this.entityName = properties.getEntityName();
        setupForm();
    }

    private void fetchEntity(String id) {
        currentDto = presenter.get(id);
    }

    public LeaveControlView(LeavePresenter presenter) {
        this.presenter = presenter;
        initialLayout();
    }

    private void initialLayout() {
        setSizeFull();
    }

    private void setupForm() {
        formLayout = new CreateFormLayout(this::performSaveEntity, currentDto, presenter.getAllEmployees());
        add(formLayout);
    }

    private void performSaveEntity(LeaveDto dto) {
        CustomApiResponse<LeaveDto> response = presenter.save(dto);

        boolean success = response.isSuccess();
        if (success) {
            UI.getCurrent().navigate(properties.getMainView());
        }

        showMessage(response.getMessage(), success);
    }

    @Override
    protected String getMainViewUrl() {
        return properties.getMainView();
    }
}
