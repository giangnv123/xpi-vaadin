package com.xpi.application.views.hrm.contracts.component;

import java.util.function.Consumer;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.xpi.application.module.hrm.dto.ContractDto;
import com.xpi.application.views.base.abs.AbstractGrid;
import com.xpi.application.views.base.views.AvatarHelper;
import com.xpi.application.views.base.views.ViewHelper;

public class ContractGrid extends AbstractGrid<ContractDto> {

    public ContractGrid(Consumer<ContractDto> clickEdit, Consumer<ContractDto> clickDelete) {
        super(clickEdit, clickDelete);
        initialSetup();
    }

    @Override
    protected void setupGrid() {
        addColumn(displayEmployeeInfor()).setHeader("Nhân viên").setAutoWidth(true);
        addColumn(displaySallary()).setHeader("Lương").setAutoWidth(true);
        addColumn(displayJob()).setHeader("Công việc").setAutoWidth(true);
        addColumn(createTitleRenderer()).setHeader("Tiêu đề").setAutoWidth(true);
        addColumn(dateDisplay()).setHeader("Ngày bắt đầu").setAutoWidth(true);
        addColumn(displayType()).setHeader("Loại hợp đồng").setAutoWidth(true);
        addColumn(displayStatus()).setHeader("Trạng thái").setAutoWidth(true);
    }

    private ComponentRenderer<Component, ContractDto> displayEmployeeInfor() {
        return new ComponentRenderer<>(dto -> {
            return AvatarHelper.displayBasicEmployeeInforWithAvatar(dto.getEmployee());
        });
    }

    private ComponentRenderer<Component, ContractDto> displayJob() {
        return new ComponentRenderer<>(dto -> {
            Div container = new Div();
            Span job = ViewHelper.createLimitTextComponent(dto.getJobTitle());
            job.addClassName("title");
            Span description = ViewHelper.createLimitTextComponent(dto.getJobDescription());
            container.add(job, description);
            return container;
        });
    }

    private ComponentRenderer<Component, ContractDto> displaySallary() {
        return new ComponentRenderer<>(dto -> {
            Div container = new Div(new Span(ViewHelper.formatSalary(dto.getSalary()) + " VND"));
            container.addClassName("flex-col-layout");
            return container;
        });
    }

    private ComponentRenderer<Component, ContractDto> createTitleRenderer() {
        return new ComponentRenderer<>(dto -> {
            Span title = new Span(dto.getTitle());
            title.addClassName("break-line-cell");
            return title;
        });
    }

    private ComponentRenderer<Component, ContractDto> dateDisplay() {
        return new ComponentRenderer<>(dto -> {
            Div container = new Div(new Span(String.valueOf(dto.getStartDate())));
            container.addClassName("flex-col-layout");
            return container;
        });
    }

    private ComponentRenderer<Component, ContractDto> displayStatus() {
        return new ComponentRenderer<>(dto -> {
            return ViewHelper.createStatusComponent(dto.getStatus().toReadableString(),
                    dto.getStatus().toString().toLowerCase());
        });
    }

    private ComponentRenderer<Component, ContractDto> displayType() {
        return new ComponentRenderer<>(dto -> {
            return ViewHelper.createStatusComponent(dto.getContractType().toReadableString(),
                    dto.getContractType().toString().toLowerCase());
        });
    }

}
