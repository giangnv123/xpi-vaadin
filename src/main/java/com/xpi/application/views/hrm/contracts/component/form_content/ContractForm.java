package com.xpi.application.views.hrm.contracts.component.form_content;

import java.time.Duration;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

import org.vaadin.lineawesome.LineAwesomeIcon;

import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.combobox.ComboBox.ItemFilter;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.ValidationResult;
import com.xpi.application.module.hrm.contracts.shared.ContractStatus;
import com.xpi.application.module.hrm.contracts.shared.ContractType;
import com.xpi.application.module.hrm.dto.ContractDto;
import com.xpi.application.module.hrm.dto.EmployeeDto;
import com.xpi.application.views.base.abs.AbstractForm;
import com.xpi.application.views.base.component.XFormLayout;
import com.xpi.application.views.base.utils.ColorUtils;
import com.xpi.application.views.base.views.AvatarHelper;
import com.xpi.application.views.base.views.ViewHelper;

public class ContractForm extends AbstractForm<ContractDto> {

        private List<EmployeeDto> employees;
        private Consumer<EmployeeDto> setCurrentEmployee;
        private Boolean updateMode;

        private LocalDate today = LocalDate.now();

        /* Job Detail */

        /* Contract Details */
        private ComboBox<EmployeeDto> selectEmployeeField;
        private TextField titleField;
        private TextArea noteField;
        private Select<ContractStatus> statusField;
        private Select<ContractType> contractTypeField;
        private DatePicker signDate;
        private DatePicker startDate;
        private DatePicker endDate;

        /* Job Details */
        private NumberField salaryField;
        private TextField jobTitleField;
        private TextArea jobDescriptionField;

        public ContractForm(List<EmployeeDto> employees, Consumer<EmployeeDto> setCurrentEmployee, Boolean updateMode) {
                super(ContractDto.class);
                this.setCurrentEmployee = setCurrentEmployee;
                this.employees = employees;
                this.updateMode = updateMode;
                initializeForm();
        }

        @Override
        protected void preBuild() {
                Div contractDetailsContainer = createContractDetailsContainer();

                Div jobDetailsContainer = createJobDetailsContainer();

                addClassNames("flex-col-layout", "gap-4");
                add(contractDetailsContainer, jobDetailsContainer);
        }

        private Div createContractDetailsContainer() {
                Div container = new Div();
                container.add(ViewHelper.titleWithIcon("Thông tin hợp đồng", LineAwesomeIcon.HANDSHAKE_SOLID,
                                ColorUtils.WHITE, ColorUtils.BG_SUCCESS));

                container.addClassName("card");

                XFormLayout layout = new XFormLayout();

                titleField = new TextField("Tiêu đề");
                noteField = new TextArea("Ghi chú");

                statusField = new Select<>();
                statusField.setLabel("Trạng thái");
                statusField.setItems(ContractStatus.getValuesExceptEmpty());
                statusField.setValue(ContractStatus.Running);
                statusField.setItemLabelGenerator(item -> item.toReadableString());

                contractTypeField = new Select<>();
                contractTypeField.setLabel("Loại hợp đồng");
                contractTypeField.setItems(ContractType.values());
                contractTypeField.setItemLabelGenerator(item -> item.toReadableString());
                contractTypeField.addValueChangeListener(event -> {
                        handleContractTypeChangeEvent(event.getValue());
                });

                if (!updateMode)
                        setupEmployeeField();

                signDate = new DatePicker("Ngày ký hợp đồng");
                signDate.setValue(today);

                startDate = new DatePicker("Ngày bắt đầu hợp đồng");
                startDate.setValue(today);
                startDate.addValueChangeListener(event -> {
                        handleStartDateChange(event.getValue());
                });

                endDate = new DatePicker("Ngày hết hạn hợp đồng");
                endDate.setReadOnly(true);
                endDate.setMin(today);
                endDate.addValueChangeListener(event -> {
                        handleEndDateChange(event.getValue());
                });

                layout.add(titleField, statusField);
                if (selectEmployeeField != null)
                        layout.add(selectEmployeeField);
                layout.add(contractTypeField, signDate, startDate, endDate, noteField);

                layout.setColspan(noteField, 2);
                layout.setColspan(titleField, 2);

                container.add(layout);

                return container;
        }

        private void handleContractTypeChangeEvent(ContractType contractType) {
                if (contractType.equals(ContractType.Temporary)) {
                        endDate.setReadOnly(false);
                        binder.forField(endDate)
                                        .asRequired("Hãy nhập ngày hết hạn hợp đồng")
                                        .bind(ContractDto::getEndDate, ContractDto::setEndDate);
                } else if (contractType.equals(ContractType.Permanent)) {
                        endDate.setReadOnly(true);

                        binder.forField(endDate)
                                        .withValidator((value, context) -> ValidationResult.ok())
                                        .bind(ContractDto::getEndDate, ContractDto::setEndDate);
                }
        }

        private void handleStartDateChange(LocalDate date) {
                endDate.setMin(date);
                binder.forField(endDate)
                                .withValidator(endDate -> endDate.isAfter(date),
                                                "Ngày hết hạn không thể trước ngày bắt đầu");
        }

        private void handleEndDateChange(LocalDate date) {
                endDate.setHelperText("Tổng số ngày làm việc: "
                                + Duration.between(startDate.getValue().atStartOfDay(), date.atStartOfDay()).toDays()
                                + " ngày");
        }

        private Div createJobDetailsContainer() {
                Div container = new Div();
                container.addClassName("card");
                container.add(ViewHelper.titleWithIcon("Thông tin công việc", LineAwesomeIcon.BRIEFCASE_SOLID,
                                ColorUtils.WHITE, ColorUtils.BG_SUCCESS));

                XFormLayout layout = new XFormLayout();

                salaryField = new NumberField("Mức lương");
                salaryField.setSuffixComponent(new Span("VND"));
                salaryField.addValueChangeListener(event -> {
                        salaryField.setHelperText(ViewHelper.formatSalary(event.getValue()) + " VND");
                });

                jobTitleField = new TextField("Tên công việc");
                jobDescriptionField = new TextArea("Mô tả công việc");

                layout.add(salaryField, jobTitleField, jobDescriptionField);
                layout.setColspan(jobDescriptionField, 2);

                container.add(layout);
                return container;
        }

        private void setupEmployeeField() {
                selectEmployeeField = new ComboBox<>("Chọn nhân viên");
                ItemFilter<EmployeeDto> filter = (employee,
                                filterString) -> (employee.getFullname() + " "
                                                + employee.getDesignation() + " " + employee.getPhone())
                                                .toLowerCase().indexOf(filterString.toLowerCase()) > -1;

                selectEmployeeField.setItems(filter, Optional.ofNullable(employees).orElse(List.of()));
                selectEmployeeField.setItemLabelGenerator(employee -> employee.getFullname());
                selectEmployeeField.setRenderer(AvatarHelper.createEmployeeRender());
                selectEmployeeField.addValueChangeListener(event -> setCurrentEmployee(event.getValue().getSalary()));
        }

        public void setCurrentEmployee(Double salary) {
                setCurrentEmployee.accept(selectEmployeeField.getValue());
                if (salary != null) {
                        salaryField.setValue(salary);
                        salaryField.setHelperText(
                                        "Mức lương hiện tại: " + ViewHelper.formatSalary(salary) + " VND");
                } else {
                        salaryField.setValue(null);
                        salaryField.setHelperText(null);
                }
        }

        @Override
        protected void setupBinder() {
                setupContractDetailsBinder();
                setupJobDetailsBinder();
        }

        private void setupContractDetailsBinder() {
                binder.forField(titleField)
                                .asRequired("Bạn cần nhập tên hợp đồng")
                                .bind(ContractDto::getTitle, ContractDto::setTitle);

                binder.forField(noteField)
                                .bind(ContractDto::getNote, ContractDto::setNote);

                binder.forField(statusField)
                                .asRequired("Hãy chọn trạng thái")
                                .bind(ContractDto::getStatus, ContractDto::setStatus);

                binder.forField(contractTypeField)
                                .asRequired("Hãy chọn loại hợp đồng")
                                .bind(ContractDto::getContractType, ContractDto::setContractType);

                binder.forField(signDate)
                                .asRequired("Hãy chọn ngày ký hợp đồng")
                                .bind(ContractDto::getSignedDate, ContractDto::setSignedDate);

                binder.forField(startDate)
                                .asRequired("Hãy chọn ngày bắt đầu công việc")
                                .bind(ContractDto::getStartDate, ContractDto::setStartDate);

                binder.forField(endDate)
                                .bind(ContractDto::getEndDate, ContractDto::setEndDate);

                if (selectEmployeeField != null) {
                        binder.forField(selectEmployeeField)
                                        .asRequired("Hãy chọn nhân viên cho hợp đồng")
                                        .bind(ContractDto::getEmployee, ContractDto::setEmployee);
                }
        }

        private void setupJobDetailsBinder() {
                binder.forField(salaryField)
                                .asRequired("Hãy nhập mức lương")
                                .bind(ContractDto::getSalary, ContractDto::setSalary);

                binder.forField(jobTitleField)
                                .asRequired("Hãy nhập tên công việc")
                                .bind(ContractDto::getJobTitle, ContractDto::setJobTitle);

                binder.forField(jobDescriptionField)
                                .asRequired("Hãy nhập mô tả công việc")
                                .bind(ContractDto::getJobDescription, ContractDto::setJobDescription);
        }

        public String getEmployeeId() {
                if (selectEmployeeField == null)
                        return null;
                return selectEmployeeField.getValue().getId();
        }

}
