package com.xpi.application.views.hrm.dashboard.views.components;

import java.util.List;

import org.vaadin.lineawesome.LineAwesomeIcon;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.avatar.Avatar;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H5;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.xpi.application.module.hrm.dto.EmployeeDto;
import com.xpi.application.views.base.views.AvatarHelper;
import com.xpi.application.views.base.views.ViewHelper;

public class EmployeeDashboardGrid extends Grid<EmployeeDto> {

    private List<EmployeeDto> employees;

    public EmployeeDashboardGrid(List<EmployeeDto> employees) {
        this.employees = employees;
        setupGrid();
    }

    private void setupGrid() {
        setItems(employees);

        addThemeVariants(GridVariant.LUMO_NO_BORDER, GridVariant.LUMO_ROW_STRIPES,
                GridVariant.LUMO_COLUMN_BORDERS, GridVariant.LUMO_COMPACT);

        addColumn(basicInfor()).setHeader("Nhân viên").setAutoWidth(true);
        addColumn(displayDesignation()).setHeader("Chức danh").setAutoWidth(true);
        addColumn(displayStatus()).setHeader("Trạng thái").setAutoWidth(true);
    }

    private ComponentRenderer<Component, EmployeeDto> basicInfor() {
        // common name,
        return new ComponentRenderer<>(dto -> {
            Div info = new Div();
            info.addClassNames("flex-col-layout");

            info.add(new H5(dto.getFullname()));
            info.add(ViewHelper.createInfoItemWithIcon(LineAwesomeIcon.BUILDING_SOLID,
                    new Span(dto.getDepartment().getName()), null));

            Avatar image = AvatarHelper.createDefaultAvatar(dto.getPhoto(), dto.getFullname());
            Div basicInfo = new Div();
            basicInfo.addClassName("flex-layout");
            basicInfo.add(image, info);
            return basicInfo;
        });
    }

    private ComponentRenderer<Component, EmployeeDto> displayDesignation() {
        return new ComponentRenderer<>(dto -> {
            return ViewHelper.createLimitTextComponent(dto.getDesignation());
        });
    }

    private ComponentRenderer<Component, EmployeeDto> displayStatus() {
        return new ComponentRenderer<>(dto -> {
            Span status = new Span(dto.getStatus().toString());
            status.addClassName("status");
            status.addClassName(dto.getStatus().toString().toLowerCase());
            return status;
        });
    }

}
