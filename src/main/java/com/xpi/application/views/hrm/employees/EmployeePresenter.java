package com.xpi.application.views.hrm.employees;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.flow.data.provider.ConfigurableFilterDataProvider;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.xpi.application.base.ApiResponse;
import com.xpi.application.module.hrm.dto.DepartmentDto;
import com.xpi.application.module.hrm.dto.EmployeeControlDto;
import com.xpi.application.module.hrm.dto.EmployeeDto;
import com.xpi.application.module.hrm.employees.EmployeeService;
import com.xpi.application.module.hrm.employees.shared.EmployeeFilter;
import com.xpi.application.module.hrm.shared.HelperService;
import com.xpi.application.views.base.utils.CustomApiResponse;
import com.xpi.application.views.hrm.employees.views.EmployeeGridView;

@SpringComponent
public class EmployeePresenter {

    private ConfigurableFilterDataProvider<EmployeeDto, Void, EmployeeFilter> filterDataProvider;

    private EmployeeDataProvider dataProvider;

    private EmployeeService service;

    @Autowired
    private HelperService helperService;

    private EmployeeGridView view;

    private static EmployeeFilter defaultFilter = EmployeeFilter.defaultFilter();

    /* Basic View */
    public EmployeePresenter(EmployeeService service) {
        this.service = service;
        this.dataProvider = new EmployeeDataProvider(service);
        filterDataProvider = dataProvider.withConfigurableFilter();
    }

    public void initView(EmployeeGridView view) {
        this.view = view;
        filterDataProvider.setFilter(defaultFilter);
        this.view.getGrid().setItems(filterDataProvider);
    }

    public void refreshGrid(EmployeeFilter filter) {
        filterDataProvider.setFilter(filter);
    }

    private void updateView() {
        if (this.view != null)
            this.view.getGrid().setDataProvider(filterDataProvider);
    }

    public CustomApiResponse<EmployeeDto> delete(String id, EmployeeDto dto) {
        ApiResponse<EmployeeDto> response = service.deleteEmployee(id, dto);
        Boolean isSuccess = response.isSuccess();
        if (isSuccess)
            updateView();
        return CustomApiResponse.delete(response);
    }

    public CustomApiResponse<EmployeeDto> save(EmployeeControlDto dto) {

        ApiResponse<EmployeeDto> response;
        Boolean isUpdated = dto.getId() != null;
        if (isUpdated) {
            response = service.updateEmployee(dto.getId(), dto);
        } else {
            response = service.createEntity(dto);
        }
        if (response.isSuccess())
            updateView();

        return CustomApiResponse.save(response, isUpdated, response.getData());
    }

    public CustomApiResponse<DepartmentDto> saveDepartment(DepartmentDto dto) {
        ApiResponse<DepartmentDto> response = helperService.createDepartment(dto);
        return CustomApiResponse.save(response, false, response.getData());
    }

    public EmployeeDto get(String id) {
        return service.get(id);
    }

    /* Additional methods */

    public List<DepartmentDto> getDepartments() {
        return helperService.getAllDepartments();
    }

    public Boolean createDepartment(DepartmentDto dto) {
        ApiResponse<DepartmentDto> response = helperService.createDepartment(dto);
        return response.isSuccess();
    }

}