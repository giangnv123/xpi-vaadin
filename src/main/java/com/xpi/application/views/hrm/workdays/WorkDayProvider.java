package com.xpi.application.views.hrm.workdays;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import com.vaadin.flow.data.provider.AbstractBackEndDataProvider;
import com.vaadin.flow.data.provider.Query;
import com.xpi.application.module.hrm.workday.WorkDayService;
import com.xpi.application.module.hrm.workday.shared.EmployeeWorkdayDto;
import com.xpi.application.module.hrm.workday.shared.WorkdayFilter;

public class WorkDayProvider extends AbstractBackEndDataProvider<EmployeeWorkdayDto, WorkdayFilter> {

    private static final long serialVersionUID = -2772672175400690881L;

    private WorkDayService service;

    public WorkDayProvider(WorkDayService service) {
        super();
        this.service = service;
    }

    @Override
    protected Stream<EmployeeWorkdayDto> fetchFromBackEnd(Query<EmployeeWorkdayDto, WorkdayFilter> query) {

        Optional<List<EmployeeWorkdayDto>> optionalData = Optional
                .ofNullable(service.getEntities(query.getFilter().orElse(null)));

        if (optionalData.isEmpty())
            return Stream.empty();
        return optionalData.get().stream();
    }

    @Override
    protected int sizeInBackEnd(Query<EmployeeWorkdayDto, WorkdayFilter> query) {
        Optional<List<EmployeeWorkdayDto>> optionalData = Optional
                .ofNullable(service.getEntities(query.getFilter().orElse(null)));
        if (optionalData.isEmpty())
            return 0;
        return optionalData.get().size();
    }
}
