package com.xpi.application.views.hrm.shared;

import java.util.List;
import java.util.function.Function;

import org.vaadin.lineawesome.LineAwesomeIcon;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.xpi.application.module.hrm.dto.DepartmentDto;
import com.xpi.application.views.base.component.XDialog;
import com.xpi.application.views.base.utils.CustomApiResponse;
import com.xpi.application.views.base.views.ViewHelper;
import com.xpi.application.views.hrm.department.component.DepartmentForm;

public class SelectDepartment extends HorizontalLayout {

    private DepartmentDto currentValue;

    private DepartmentForm form;

    private XDialog dialog;

    private ComboBox<DepartmentDto> selectDepartmentField;

    private Function<Void, List<DepartmentDto>> getDepartments;
    private Function<DepartmentDto, CustomApiResponse<DepartmentDto>> saveDepartment;

    public SelectDepartment(Function<Void, List<DepartmentDto>> getDepartments,
            Function<DepartmentDto, CustomApiResponse<DepartmentDto>> saveDepartment) {
        this.getDepartments = getDepartments;
        this.saveDepartment = saveDepartment;
        initialListup();
    }

    private void initialListup() {
        setupDialogAndForm();
        setupSelectDepartmentField();

        Button button = createAddButton();

        add(selectDepartmentField, button);
        setWidthFull();
        setAlignItems(FlexComponent.Alignment.END);
    }

    private ComboBox<DepartmentDto> setupSelectDepartmentField() {
        selectDepartmentField = new ComboBox<>();
        selectDepartmentField.setItems(getDepartments.apply(null));
        selectDepartmentField.setItemLabelGenerator(DepartmentDto::getName);
        selectDepartmentField.setWidthFull();
        selectDepartmentField.setLabel("Phòng ban");
        selectDepartmentField.setPlaceholder("Chọn phòng ban");
        return selectDepartmentField;
    }

    private Button createAddButton() {
        Button addButton = new Button(LineAwesomeIcon.PLUS_SOLID.create());
        addButton.addClickListener(event -> dialog.open());
        return addButton;

    }

    private void setupDialogAndForm() {
        form = new DepartmentForm();
        dialog = new XDialog("Thêm Phòng Ban", this::performSaveDepartment);
        dialog.add(form);
    }

    private void performSaveDepartment() {
        if (!form.validateAndWrite())
            return;

        DepartmentDto dto = form.getEntity();

        CustomApiResponse<DepartmentDto> response = saveDepartment.apply(dto);

        Runnable runOnSuccess = () -> {
            selectDepartmentField.setItems(getDepartments.apply(null));
            selectDepartmentField.setValue(currentValue);
        };

        ViewHelper.handleResponse(response.isSuccess(), response.getMessage(), dialog, form, runOnSuccess);
    }

    public void setLabel(String label) {
        selectDepartmentField.setLabel(label);
    }

    public void setValue(DepartmentDto dto) {
        currentValue = dto;
        selectDepartmentField.setValue(currentValue);
    }

    public Long getDepartmentId() {
        if (selectDepartmentField.getValue() == null)
            return null;
        return selectDepartmentField.getValue().getId();
    }
}
