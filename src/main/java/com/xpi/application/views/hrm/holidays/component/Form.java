package com.xpi.application.views.hrm.holidays.component;

import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.textfield.TextArea;
import com.xpi.application.module.hrm.dto.HolidayDto;
import com.xpi.application.views.base.abs.AbstractForm;
import com.xpi.application.views.base.component.XFormLayout;

public class Form extends AbstractForm<HolidayDto> {
    /* Declare components - but do not initialize */
    private DatePicker dateField;

    private TextArea descriptionField;

    public Form() {
        super(HolidayDto.class);
        initializeForm();

    }

    protected void preBuild() {
        /* Initialize */
        XFormLayout formLayout = new XFormLayout();

        dateField = new DatePicker("Ngày");

        descriptionField = new TextArea("Ghi chú");
        descriptionField.setPlaceholder("Nghỉ quốc khánh, nghỉ lễ,...");

        formLayout.add(dateField, descriptionField);

        add(formLayout);
    }

    @Override
    protected void setupBinder() {
        binder.forField(dateField)
                .asRequired("Hãy chọn ngày nghỉ")
                .bind(HolidayDto::getDate, HolidayDto::setDate);

        binder.forField(descriptionField)
                .asRequired("Hãy nhập ghi chú")
                .bind(HolidayDto::getDescription, HolidayDto::setDescription);

    }
}
