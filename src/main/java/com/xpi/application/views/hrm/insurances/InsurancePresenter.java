package com.xpi.application.views.hrm.insurances;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.flow.data.provider.ConfigurableFilterDataProvider;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.xpi.application.base.ApiResponse;
import com.xpi.application.module.hrm.dto.EmployeeDto;
import com.xpi.application.module.hrm.dto.InsuranceDto;
import com.xpi.application.module.hrm.insurances.InsuranceService;
import com.xpi.application.module.hrm.insurances.shared.InsuranceFilter;
import com.xpi.application.module.hrm.shared.HelperService;
import com.xpi.application.views.base.utils.CustomApiResponse;
import com.xpi.application.views.hrm.insurances.views.InsuranceGridView;

@SpringComponent
public class InsurancePresenter {

    private static InsuranceFilter defaultFilter = InsuranceFilter.defaultFilter();

    private ConfigurableFilterDataProvider<InsuranceDto, Void, InsuranceFilter> filterDataProvider;

    InsuranceDataProvider dataProvider;

    InsuranceService service;

    @Autowired
    private HelperService helperService;

    private InsuranceGridView view;

    /* Basic View */
    public InsurancePresenter(InsuranceService service) {
        this.service = service;
        this.dataProvider = new InsuranceDataProvider(service);
        filterDataProvider = dataProvider.withConfigurableFilter();
    }

    public void initView(InsuranceGridView view) {
        this.view = view;
        filterDataProvider.setFilter(defaultFilter);
        this.view.getGrid().setItems(filterDataProvider);
    }

    public void refreshGrid(InsuranceFilter filter) {
        filterDataProvider.setFilter(filter);
    }

    private void updateView() {
        if (this.view != null)
            this.view.getGrid().setDataProvider(filterDataProvider);
    }

    /* Basic CRUD */
    public CustomApiResponse<Void> delete(String id) {
        ApiResponse<Void> response = service.delete(id);

        if (response.isSuccess())
            updateView();

        return CustomApiResponse.delete(response);
    }

    public CustomApiResponse<InsuranceDto> save(InsuranceDto dto) {
        CustomApiResponse<InsuranceDto> response = service.save(dto);

        if (response.isSuccess())
            updateView();

        return response;
    }

    public InsuranceDto get(String id) {
        return service.get(id);
    }

    /* Additional methods */

    public List<EmployeeDto> getAllEmployeesDoNotHaveInsurance() {
        return helperService.getAllEmployees().stream().filter(emp -> emp.getInsurance() == null).toList();
    }
}