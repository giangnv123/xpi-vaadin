package com.xpi.application.views.hrm.employees.component.form_content;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.nio.file.Files;

import javax.imageio.ImageIO;

import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class CustomMultipartFile implements MultipartFile, Serializable {
    private final String name;
    private final InputStream inputStream;
    private final String contentType;
    private final long size;
    private final int width;
    private final int height;

    // Constructor now explicitly requires the file name
    @JsonIgnore
    public CustomMultipartFile(String fileName, File file) throws IOException {
        this.name = fileName; // Use the provided file name
        this.inputStream = new FileInputStream(file);
        this.size = file.length();
        this.contentType = Files.probeContentType(file.toPath());

        // Attempt to read image dimensions if the file is an image
        BufferedImage image = ImageIO.read(file);
        if (image != null) {
            this.width = image.getWidth();
            this.height = image.getHeight();
        } else {
            this.width = 0;
            this.height = 0;
        }
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getOriginalFilename() {
        return this.name;
    }

    @Override
    public String getContentType() {
        return this.contentType;
    }

    @Override
    public boolean isEmpty() {
        return this.size == 0;
    }

    @Override
    public long getSize() {
        return this.size;
    }

    @Override
    public byte[] getBytes() throws IOException {
        return this.inputStream.readAllBytes();
    }

    @Override
    @JsonIgnore
    public InputStream getInputStream() {
        return this.inputStream;
    }

    @Override
    public void transferTo(File dest) throws IOException, IllegalStateException {
        try (OutputStream out = new FileOutputStream(dest)) {
            out.write(this.getBytes());
        }
    }

    public int getWidth() {
        return this.width;
    }

    public int getHeight() {
        return this.height;
    }

}
