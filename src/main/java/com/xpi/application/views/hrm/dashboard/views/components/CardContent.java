package com.xpi.application.views.hrm.dashboard.views.components;

import org.vaadin.lineawesome.LineAwesomeIcon;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H3;
import com.xpi.application.module.hrm.dashboard.shared.DashboardDto;
import com.xpi.application.module.hrm.dashboard.shared.EmployeeSummary;
import com.xpi.application.views.base.utils.ColorUtils;
import com.xpi.application.views.base.views.ViewHelper;

public class CardContent extends Div {

    private final static String employeeMainView = "hrm/employees";
    private final static String contractMainView = "hrm/contracts";
    private final static String workdaysMainView = "hrm/workdays";

    private final static String employeeCardText = "Tổng số nhân viên";
    private final static String employeeResignCardText = "Nhân viên đã nghỉ";
    private final static String contractCardText = "Tổng số hợp đồng";
    private final static String payoutCardText = "Tổng mức lương chi trả";

    private DashboardDto dashboardDto;

    public CardContent(DashboardDto dashboardDto) {
        this.dashboardDto = dashboardDto;
        setupLayout();
    }

    private void setupLayout() {
        addClassName("card-container");

        if (dashboardDto == null) {
            add(new H3("Xảy ra lỗi! Vui lòng tải lại trang"));
            return;
        }

        initilizeCardContent();
    }

    private void initilizeCardContent() {

        EmployeeSummary employeeSummary = dashboardDto.getEmployee();

        if (employeeSummary == null)
            return;

        Div employeeTotalCard = createCard(employeeCardText, LineAwesomeIcon.USERS_SOLID,
                ColorUtils.SUCCESS, ColorUtils.WHITE, String.valueOf(employeeSummary.getTotalEmployees()));
        employeeTotalCard.addClickListener(e -> UI.getCurrent().navigate(employeeMainView));

        Div resignEmployeeCard = createCard(employeeResignCardText, LineAwesomeIcon.USER_SLASH_SOLID,
                ColorUtils.DANGER, ColorUtils.WHITE, String.valueOf(employeeSummary.getSuspendedEmployees()));
        employeeTotalCard.addClickListener(e -> UI.getCurrent().navigate(employeeMainView));

        Div payoutCard = createCard(payoutCardText, LineAwesomeIcon.MONEY_BILL_ALT,
                ColorUtils.SUCCESS, ColorUtils.WHITE,
                ViewHelper.formatSalary(dashboardDto.getTotalPayout()) + " VND");
        payoutCard.addClickListener(e -> UI.getCurrent().navigate(workdaysMainView));

        Div contractCard = createCard(contractCardText, LineAwesomeIcon.FILE_ALT, ColorUtils.SUCCESS, ColorUtils.WHITE,
                String.valueOf(dashboardDto.getContract().getTotalContracts()));
        contractCard.addClickListener(e -> UI.getCurrent().navigate(contractMainView));

        add(employeeTotalCard, resignEmployeeCard, contractCard, payoutCard);
    }

    private Div createCard(String title, LineAwesomeIcon icon, String iconColor, String backgroundColor,
            String content) {

        Div textHeader = ViewHelper.cardTitleWithIcon(title, icon, iconColor, backgroundColor);
        H3 contentHeader = new H3(content);
        contentHeader.getStyle().set("margin-left", "5px");

        Div card = new Div();
        card.addClassName("card");
        card.add(textHeader, contentHeader);

        return card;
    }
}
