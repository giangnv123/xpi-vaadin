package com.xpi.application.views.hrm.employees.component;

import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.xpi.application.module.hrm.dto.DepartmentDto;
import com.xpi.application.module.hrm.employees.shared.EmpStatus;
import com.xpi.application.module.hrm.employees.shared.EmployeeFilter;
import com.xpi.application.views.base.abs.AbstractFilterContainer;

public class EmployeeFilterContainer extends AbstractFilterContainer<EmployeeFilter> {

    private List<DepartmentDto> departments;

    private ComboBox<DepartmentDto> departmentField;

    private ComboBox<EmpStatus> statusField;

    public EmployeeFilterContainer(Consumer<EmployeeFilter> applyFilter, List<DepartmentDto> departments) {
        super(applyFilter);
        this.departments = departments;
        buildLayout();
    }

    @Override
    protected HorizontalLayout createFilterComponent() {
        statusField = new ComboBox<>();
        statusField.setItems(EmpStatus.values());
        statusField.setPlaceholder("Trạng thái");
        statusField.setItemLabelGenerator(EmpStatus::toReadableString);
        statusField.setValue(EmpStatus.Active);
        statusField.addValueChangeListener(event -> applyFilters());

        departmentField = new ComboBox<>();
        departmentField.setItems(departments);
        departmentField.setPlaceholder("Phòng ban");
        departmentField.setItemLabelGenerator(DepartmentDto::getName);
        departmentField.addValueChangeListener(event -> applyFilters());

        HorizontalLayout filterComponents = new HorizontalLayout();
        filterComponents.add(searchField, statusField, departmentField);
        return filterComponents;
    }

    @Override
    protected void clearFilterField() {
        searchField.clear();
        statusField.clear();
        departmentField.clear();
    }

    @Override
    protected EmployeeFilter getFilterValue() {
        return new EmployeeFilter(searchField.getValue(), statusField.getValue(),
                Optional.ofNullable(departmentField.getValue()).map(DepartmentDto::getId).orElse(null));
    }

    @Override
    protected EmployeeFilter getDefaultFilter() {
        return EmployeeFilter.defaultFilter();
    }

}
