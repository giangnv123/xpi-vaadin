package com.xpi.application.views.hrm.workdays.component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.function.Consumer;

import org.vaadin.lineawesome.LineAwesomeIcon;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.SvgIcon;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.xpi.application.module.hrm.workday.shared.EmployeeWorkdayDetailsDto;
import com.xpi.application.module.hrm.workday.shared.EmployeeWorkdayDto;
import com.xpi.application.module.hrm.workday.shared.WorkingDayOfMonthOverView;
import com.xpi.application.module.hrm.workday.shared._enum.WorkDayType;
import com.xpi.application.views.base.utils.ColorUtils;
import com.xpi.application.views.base.views.AvatarHelper;
import com.xpi.application.views.base.views.ViewHelper;

public class WorkdayGrid extends Grid<EmployeeWorkdayDto> {

    private WorkingDayOfMonthOverView workdayOfMonthOverView;

    private Consumer<EmployeeWorkdayDetailsDto> clickEdit;

    public WorkdayGrid(Consumer<EmployeeWorkdayDetailsDto> clickEdit,
            WorkingDayOfMonthOverView workdayOfMonthOverView) {
        this.clickEdit = clickEdit;
        this.workdayOfMonthOverView = workdayOfMonthOverView;
        if (workdayOfMonthOverView != null) {
            initialSetup();
        }
    }

    public void refreshOverView(WorkingDayOfMonthOverView newOverView) {
        this.workdayOfMonthOverView = newOverView;
        if (workdayOfMonthOverView != null) {
            removeAllColumns();
            initialSetup();
        }
    }

    private void initialSetup() {
        setupGrid();
    }

    private void setupGrid() {

        addThemeVariants(GridVariant.LUMO_NO_BORDER, GridVariant.LUMO_ROW_STRIPES,
                GridVariant.LUMO_COLUMN_BORDERS, GridVariant.LUMO_COMPACT);
        setSelectionMode(SelectionMode.MULTI);

        addColumn(displayEmployeeInfor()).setHeader("Nhân viên").setAutoWidth(true);

        List<LocalDate> allDaysOfMonth = workdayOfMonthOverView.getAllDaysOfMonth();

        if (allDaysOfMonth == null)
            return;

        addColumn(EmployeeWorkdayDto::getTotalWorkDay)
                .setHeader("Ngày làm việc")
                .setAutoWidth(true);

        addColumn(dto -> displayCount(dto.getTotalCount()))
                .setHeader("Tổng số công")
                .setAutoWidth(true);

        for (int i = 0; i < allDaysOfMonth.size(); i++) {
            LocalDate day = allDaysOfMonth.get(i);

            int index = i;

            addColumn(new ComponentRenderer<Span, EmployeeWorkdayDto>(
                    dto -> {
                        List<EmployeeWorkdayDetailsDto> workdays = dto.getWorkdays();
                        if (index >= 0 && index < workdays.size()) {
                            EmployeeWorkdayDetailsDto workday = workdays.get(index);
                            workday.setEmployeeId(dto.getEmployee().getId());
                            return displayWorkDayData(workday);
                        } else {
                            return new Span(); // Return an empty Span if the index is out of bounds
                        }
                    }))
                    .setAutoWidth(true)
                    .setHeader(day.format(DateTimeFormatter.ofPattern("d/M")));
        }

        addColumn(EmployeeWorkdayDto::getTotalPaidDaysOff)
                .setHeader("Nghỉ có lương")
                .setAutoWidth(true);

        addColumn(EmployeeWorkdayDto::getTotalUnPaidDaysOff)
                .setHeader("Nghỉ không lương")
                .setAutoWidth(true);
    }

    private ComponentRenderer<Component, EmployeeWorkdayDto> displayEmployeeInfor() {
        return new ComponentRenderer<>(dto -> AvatarHelper.displayBasicEmployeeInforWithAvatar(dto.getEmployee()));
    }

    private Span displayWorkDayData(EmployeeWorkdayDetailsDto dto) {
        Span dataRender = new Span();
        dataRender.addClassNames("flex-col-layout", "items-center", "justify-center");

        if (dto == null) {
            return dataRender; // Handle null dto case
        }

        if (dto.getType().equals(WorkDayType.Absence)) {
            return dataRender; // If the dto is before the employee's start dto, return an empty render
        }

        SvgIcon icon = determineIcon(dto.getType(), dto.getNote());

        if (icon != null) {
            dataRender.add(icon);
            if (dto.getType().equals(WorkDayType.WorkingDay) && dto.getCount() != 1) {
                dataRender.add(new Span(" (x" + (float) dto.getCount() + ")"));
            }
        }

        dataRender.addClickListener(event -> {
            clickEdit.accept(dto);
        });

        return dataRender;
    }

    private SvgIcon determineIcon(WorkDayType type, String note) {
        LineAwesomeIcon iconType;
        String color;

        switch (type) {
            case WeekendDaysOff:
                iconType = LineAwesomeIcon.SUN_SOLID;
                color = ColorUtils.GRAY;
                break;
            case PaidLeaveDaysOff:
                iconType = LineAwesomeIcon.CHECK_SOLID;
                color = ColorUtils.PRIMARY;
                break;
            case UnpaidLeaveDaysOff:
                iconType = LineAwesomeIcon.TIMES_SOLID;
                color = ColorUtils.DANGER;
                break;
            case HolidaysOff:
                iconType = LineAwesomeIcon.COCKTAIL_SOLID;
                color = ColorUtils.GRAY;
                break;
            default:
                iconType = LineAwesomeIcon.CHECK_SOLID;
                color = ColorUtils.SUCCESS;
                break;
        }

        return createIconWithLabel(iconType, color, note);
    }

    private SvgIcon createIconWithLabel(LineAwesomeIcon iconType, String color, String tooltipText) {
        SvgIcon icon = ViewHelper.createIcon(iconType, color, tooltipText, null);
        return icon;
    }

    private String displayCount(double totalCount) {
        return String.valueOf(totalCount + "/" + workdayOfMonthOverView.getTotalWorkDays());
    }
}
