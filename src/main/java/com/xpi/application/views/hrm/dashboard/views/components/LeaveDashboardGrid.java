package com.xpi.application.views.hrm.dashboard.views.components;

import java.util.List;
import java.util.function.Consumer;

import org.vaadin.lineawesome.LineAwesomeIcon;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H5;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.SvgIcon;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.xpi.application.module.hrm.dto.LeaveDto;
import com.xpi.application.module.hrm.leave.shared.LeaveStatus;
import com.xpi.application.views.base.utils.ColorUtils;
import com.xpi.application.views.base.views.AvatarHelper;
import com.xpi.application.views.base.views.ViewHelper;

public class LeaveDashboardGrid extends Grid<LeaveDto> {

    private List<LeaveDto> leaves;
    private Consumer<LeaveDto> updateLeave;

    public LeaveDashboardGrid(List<LeaveDto> leaves, Consumer<LeaveDto> updateLeave) {
        this.leaves = leaves;
        this.updateLeave = updateLeave;
        setupGrid();
    }

    private void setupGrid() {
        setItems(leaves);

        addThemeVariants(GridVariant.LUMO_NO_BORDER, GridVariant.LUMO_ROW_STRIPES,
                GridVariant.LUMO_COLUMN_BORDERS, GridVariant.LUMO_COMPACT);

        addColumn(displayEmployeeInfor()).setHeader("Nhân viên").setAutoWidth(true);
        addColumn(dto -> dto.getDateStart()).setHeader("Từ ngày").setAutoWidth(true);
        addColumn(dto -> dto.getDateEnd()).setHeader("Đến ngày").setAutoWidth(true);
        addColumn(createTitleRenderer()).setHeader("Ghi chú").setAutoWidth(true);
        addColumn(displayActions()).setHeader("Chức năng").setAutoWidth(true);
    }

    public void updateGridItems(List<LeaveDto> leaves) {
        this.leaves = leaves;
        setItems(leaves);
    }

    private ComponentRenderer<Component, LeaveDto> displayEmployeeInfor() {
        return new ComponentRenderer<>(dto -> {
            return AvatarHelper.displayBasicEmployeeInforWithAvatar(dto.getEmployee());
        });
    }

    private ComponentRenderer<Component, LeaveDto> createTitleRenderer() {
        return new ComponentRenderer<>(dto -> {
            Div container = new Div();
            container.addClassName("flex-col-layout");

            H5 title = new H5(dto.getTitle());
            title.addClassName("break-line-cell");

            Span description = new Span(dto.getDescription());
            description.addClassName("break-line-cell");

            container.add(title, description);
            return container;
        });
    }

    private ComponentRenderer<Component, LeaveDto> displayActions() {
        return new ComponentRenderer<>(dto -> {
            Div container = new Div();
            container.addClassName("flex-layout");

            SvgIcon timesIcon = ViewHelper.createIcon(LineAwesomeIcon.TIMES_SOLID, ColorUtils.DANGER,
                    "Từ chối đơn",
                    "25px");
            timesIcon.getStyle().set("cursor", "pointer");
            timesIcon.addClickListener(e -> {
                dto.setStatus(LeaveStatus.Rejected);
                updateLeave.accept(dto);
            });

            SvgIcon acceptIcon = ViewHelper.createIcon(LineAwesomeIcon.CHECK_SOLID, ColorUtils.SUCCESS, "Duyệt đơn",
                    "25px");
            acceptIcon.getStyle().set("cursor", "pointer");

            acceptIcon.addClickListener(e -> {
                dto.setStatus(LeaveStatus.Accepted);
                updateLeave.accept(dto);
            });

            container.add(acceptIcon, timesIcon);

            return container;
        });
    }

}
