package com.xpi.application.views.hrm.insurances.component.form_content;

import java.util.List;
import java.util.function.Consumer;

import com.vaadin.flow.component.html.Div;
import com.xpi.application.module.hrm.dto.EmployeeDto;
import com.xpi.application.module.hrm.dto.InsuranceDto;
import com.xpi.application.views.base.abs.AbstractFormControl;
import com.xpi.application.views.base.component.EmployeeInfor;
import com.xpi.application.views.hrm.insurances.shared.InsuranceProperties;

public class CreateFormLayout extends AbstractFormControl<InsuranceDto, InsuranceDto> {

    private InsuranceProperties properties = new InsuranceProperties();

    private EmployeeInfor employeeInfor;

    private List<EmployeeDto> employeees;

    private InsuranceForm mainForm;

    public CreateFormLayout(Consumer<InsuranceDto> save,
            InsuranceDto currentDto, List<EmployeeDto> employeees) {
        super(save, currentDto);
        this.employeees = employeees;

        setupLayout();
    }

    private Div employeeDisplay() {
        Div container = new Div();
        container.add(employeeInfor);
        return container;
    }

    public void updateEmployeeDisplay(EmployeeDto employeeDto) {
        employeeInfor.setEmployeeDto(employeeDto);
    }

    @Override
    protected void populateDto() {
        mainForm.setEntity(currentDto);
        mainForm.setCurrentEmployee(currentDto.getEmployee());
    }

    @Override
    protected Div createContentSection() {
        Div content = new Div();
        content.addClassName("contract-form-container");
        content.add(mainForm, employeeDisplay());
        return content;
    }

    @Override
    protected void intitializeComponent() {
        mainForm = new InsuranceForm(employeees, this::updateEmployeeDisplay, currentDto != null);
        employeeInfor = new EmployeeInfor(currentDto != null ? currentDto.getEmployee() : null);
    }

    @Override
    protected String getNavigatoMainView() {
        return properties.getMainView();
    }

    @Override
    protected void onSave() {
        /* Check validation */
        if (!mainForm.validateAndWrite())
            return;

        InsuranceDto dto = mainForm.getEntity();

        if (currentDto != null)
            dto.setId(currentDto.getId());
        else {
            dto.setEmployeeId(mainForm.getEmployeeId());
        }

        save.accept(dto);
    }
}
