package com.xpi.application.views.hrm.settings.views;

import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.xpi.application.views.hrm.settings.SettingsPresenter;
import com.xpi.application.views.hrm.settings.views.components.WeekendsSetting;
import com.xpi.application.views.layout.HRMLayout;

@PageTitle("Cài đặt")
@Route(value = "hrm/settings", layout = HRMLayout.class)
public class SettingsView extends HorizontalLayout {

    private SettingsPresenter presenter;

    private WeekendsSetting weekendsSetting;

    public SettingsView(SettingsPresenter presenter) {
        this.presenter = presenter;
        setupLayout();
    }

    private void initializeUI() {
        weekendsSetting = new WeekendsSetting(presenter.getWeekends(), presenter::updateOrCreateWeekends);
        add(weekendsSetting);
    }

    protected void setupLayout() {
        initializeUI();
    }
}
