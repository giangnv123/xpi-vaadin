package com.xpi.application.views.hrm.dashboard.views;

import org.vaadin.lineawesome.LineAwesomeIcon;

import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.xpi.application.module.hrm.dto.LeaveDto;
import com.xpi.application.module.hrm.leave.shared.LeaveStatus;
import com.xpi.application.views.base.abs.AbstractView;
import com.xpi.application.views.base.utils.ColorUtils;
import com.xpi.application.views.base.views.ViewHelper;
import com.xpi.application.views.hrm.dashboard.DashboardPresenter;
import com.xpi.application.views.hrm.dashboard.views.components.CardContent;
import com.xpi.application.views.hrm.dashboard.views.components.EmployeeDashboardGrid;
import com.xpi.application.views.hrm.dashboard.views.components.LeaveDashboardGrid;
import com.xpi.application.views.layout.HRMLayout;

@PageTitle("Dashboard")
@Route(value = "hrm/dashboard", layout = HRMLayout.class)
public class DashboardView extends AbstractView {

    private CardContent cardContent;

    private EmployeeDashboardGrid employeeGrid;
    private LeaveDashboardGrid leaveDashboardGrid;

    private DashboardPresenter presenter;

    public DashboardView(DashboardPresenter presenter) {
        this.presenter = presenter;
        setupLayout();
    }

    public void initializeUI() {
        cardContent = new CardContent(presenter.getDashboardInfor());
        employeeGrid = new EmployeeDashboardGrid(presenter.getSomeEmployees());
        leaveDashboardGrid = new LeaveDashboardGrid(presenter.getAllPendingLeaves(), this::updateLeave);
    }

    private void setupLayout() {
        initializeUI();
        addClassName("dashboard");
        add(cardContent, gridContainer());
    }

    private Div gridContainer() {
        Div container = new Div();
        container.addClassName("grid-container");
        container.add(createLeaveGrid(), createEmployeeGrid());
        return container;
    }

    private VerticalLayout createEmployeeGrid() {
        VerticalLayout container = new VerticalLayout();
        container.add(ViewHelper.cardTitleWithIcon("Nhân viên", LineAwesomeIcon.USERS_SOLID,
                ColorUtils.WHITE, ColorUtils.BG_SUCCESS));
        container.add(employeeGrid);
        return container;
    }

    private VerticalLayout createLeaveGrid() {
        VerticalLayout container = new VerticalLayout();
        container.add(ViewHelper.cardTitleWithIcon("Đơn nghỉ phép đang chờ duyệt", LineAwesomeIcon.FILE_SOLID,
                ColorUtils.WHITE, ColorUtils.BG_SUCCESS));
        container.add(leaveDashboardGrid);
        return container;
    }

    private void updateLeave(LeaveDto dto) {
        Boolean isSuccess = presenter.updateLeave(dto);

        /* Show notification */
        if (isSuccess) {
            leaveDashboardGrid.updateGridItems(presenter.getAllPendingLeaves());
            if (dto.getStatus() == LeaveStatus.Accepted) {
                showSuccessMessage("Đã duyệt đơn nghỉ phép");
            } else {
                showSuccessMessage("Đã từ chối đơn nghỉ phép");
            }
        } else {
            showErrorMessage("Không thể cập nhật đơn nghỉ phép. Vui lòng thử lại");
        }
    }

}
