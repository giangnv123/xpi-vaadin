package com.xpi.application.views.hrm.leaves.component.form_content;

import java.util.List;
import java.util.function.Consumer;

import com.vaadin.flow.component.html.Div;
import com.xpi.application.module.hrm.dto.EmployeeDto;
import com.xpi.application.module.hrm.dto.LeaveDto;
import com.xpi.application.views.base.abs.AbstractFormControl;
import com.xpi.application.views.base.component.EmployeeInfor;
import com.xpi.application.views.hrm.leaves.shared.LeaveProperties;

public class CreateFormLayout extends AbstractFormControl<LeaveDto, LeaveDto> {

    private LeaveProperties properties = new LeaveProperties();

    private EmployeeInfor employeeInfor;

    private List<EmployeeDto> employeees;

    private LeaveForm form;

    public CreateFormLayout(Consumer<LeaveDto> save,
            LeaveDto currentDto, List<EmployeeDto> employeees) {
        super(save, currentDto);
        this.employeees = employeees;

        setupLayout();
    }

    private Div employeeDisplay() {
        Div container = new Div();
        container.add(employeeInfor);
        return container;
    }

    public void updateEmployeeDisplay(EmployeeDto employeeDto) {
        employeeInfor.setEmployeeDto(employeeDto);
    }

    @Override
    protected void populateDto() {
        form.updateCurrentEmployeeUI(currentDto.getEmployee());
        form.setEntity(currentDto);
    }

    @Override
    protected Div createContentSection() {
        Div content = new Div();
        content.addClassName("contract-form-container");
        content.add(form, employeeDisplay());
        return content;
    }

    @Override
    protected void intitializeComponent() {
        Boolean updateMode = currentDto != null;

        form = new LeaveForm(employeees, this::updateEmployeeDisplay, updateMode);
        employeeInfor = new EmployeeInfor(currentDto != null ? currentDto.getEmployee() : null);
    }

    @Override
    protected String getNavigatoMainView() {
        return properties.getMainView();
    }

    @Override
    protected void onSave() {
        /* Check validation */
        if (!form.validateAndWrite())
            return;

        LeaveDto leaveDto = form.getEntity();

        leaveDto.setEmployeeId(form.getEmployeeId());

        if (currentDto != null)
            leaveDto.setId(currentDto.getId());

        save.accept(leaveDto);
    }
}
