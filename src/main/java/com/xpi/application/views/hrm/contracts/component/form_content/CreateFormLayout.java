package com.xpi.application.views.hrm.contracts.component.form_content;

import java.util.List;
import java.util.function.Consumer;

import com.vaadin.flow.component.html.Div;
import com.xpi.application.module.hrm.dto.ContractDto;
import com.xpi.application.module.hrm.dto.EmployeeDto;
import com.xpi.application.views.base.abs.AbstractFormControl;
import com.xpi.application.views.base.component.EmployeeInfor;
import com.xpi.application.views.hrm.contracts.shared.ContractProperties;

public class CreateFormLayout extends AbstractFormControl<ContractDto, ContractDto> {

    private ContractProperties properties = new ContractProperties();

    private EmployeeInfor employeeInfor;

    private List<EmployeeDto> employeees;

    private ContractForm contractForm;

    public CreateFormLayout(Consumer<ContractDto> save,
            ContractDto currentDto, List<EmployeeDto> employeees) {
        super(save, currentDto);
        this.employeees = employeees;

        setupLayout();
    }

    private Div employeeDisplay() {
        Div container = new Div();
        container.add(employeeInfor);
        return container;
    }

    public void updateEmployeeDisplay(EmployeeDto employeeDto) {
        employeeInfor.setEmployeeDto(employeeDto);
    }

    @Override
    protected void populateDto() {
        contractForm.setEntity(currentDto);
    }

    @Override
    protected Div createContentSection() {
        Div content = new Div();
        content.addClassName("contract-form-container");
        content.add(contractForm, employeeDisplay());
        return content;
    }

    @Override
    protected void intitializeComponent() {
        contractForm = new ContractForm(employeees, this::updateEmployeeDisplay, currentDto != null);
        employeeInfor = new EmployeeInfor(currentDto != null ? currentDto.getEmployee() : null);
    }

    @Override
    protected String getNavigatoMainView() {
        return properties.getMainView();
    }

    @Override
    protected void onSave() {
        /* Check validation */
        if (!contractForm.validateAndWrite())
            return;

        ContractDto contractDto = contractForm.getEntity();

        if (currentDto != null)
            contractDto.setId(currentDto.getId());
        else {
            contractDto.setEmployeeId(contractForm.getEmployeeId());
        }

        save.accept(contractDto);
    }
}
