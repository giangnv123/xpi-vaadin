package com.xpi.application.views.hrm.workdays.component;

import java.util.function.Consumer;
import java.time.Month;
import java.time.YearMonth;
import java.util.stream.IntStream;

import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.xpi.application.module.hrm.employees.shared.EmpStatus;
import com.xpi.application.module.hrm.workday.shared.WorkdayFilter;
import com.xpi.application.views.base.abs.AbstractFilterContainer;

public class WorkdayFilterContainer extends AbstractFilterContainer<WorkdayFilter> {

    private ComboBox<Integer> yearComboBox;
    private ComboBox<Month> monthComboBox;
    private ComboBox<EmpStatus> statusField;

    public WorkdayFilterContainer(Consumer<WorkdayFilter> applyFilter) {
        super(applyFilter);
        buildLayout();
    }

    @Override
    protected HorizontalLayout createFilterComponent() {
        initializeYearComboBox();
        initializeMonthComboBox();
        initializeStatusField();

        HorizontalLayout filterComponents = new HorizontalLayout(yearComboBox, monthComboBox, statusField);
        return filterComponents;
    }

    private void initializeYearComboBox() {
        yearComboBox = new ComboBox<>("Năm");
        yearComboBox.setItems(IntStream.range(2000, YearMonth.now().getYear() + 1).boxed().toList());
        yearComboBox.setValue(getDefaultFilter().year());
        yearComboBox.addValueChangeListener(e -> applyFilters());
    }

    private void initializeStatusField() {
        statusField = new ComboBox<>();
        statusField.setLabel("Trạng thái");
        statusField.setItems(EmpStatus.values());
        statusField.setPlaceholder("Trạng thái");
        statusField.setItemLabelGenerator(EmpStatus::toReadableString);
        statusField.setValue(getDefaultFilter().status());
        statusField.addValueChangeListener(event -> applyFilters());
    }

    private void initializeMonthComboBox() {
        monthComboBox = new ComboBox<>("Tháng");
        monthComboBox.setItems(Month.values());
        monthComboBox.setValue(Month.of(getDefaultFilter().month()));
        monthComboBox.addValueChangeListener(e -> applyFilters());
    }

    @Override
    protected void clearFilterField() {
        monthComboBox.setValue(Month.of(getDefaultFilter().month()));
        yearComboBox.setValue(getDefaultFilter().year());
    }

    @Override
    protected WorkdayFilter getFilterValue() {
        Integer selectedYear = yearComboBox.getValue();
        Month selectedMonth = monthComboBox.getValue();
        if (selectedYear != null && selectedMonth != null) {
            return new WorkdayFilter(selectedYear, selectedMonth, statusField.getValue());
        }
        return getDefaultFilter();
    }

    @Override
    protected WorkdayFilter getDefaultFilter() {
        return WorkdayFilter.defaultFilter();
    }
}
