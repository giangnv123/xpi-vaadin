package com.xpi.application.views.hrm.dashboard;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.flow.spring.annotation.SpringComponent;
import com.xpi.application.module.hrm.dashboard.DashboardService;
import com.xpi.application.module.hrm.dashboard.shared.DashboardDto;
import com.xpi.application.module.hrm.dto.EmployeeDto;
import com.xpi.application.module.hrm.dto.LeaveDto;
import com.xpi.application.module.hrm.shared.HelperService;

@SpringComponent
public class DashboardPresenter {

    DashboardService service;

    @Autowired
    private HelperService helperService;

    /* Basic View */
    public DashboardPresenter(DashboardService service) {
        this.service = service;
    }

    public DashboardDto getDashboardInfor() {
        return service.getDashboardDto();
    }

    public List<EmployeeDto> getSomeEmployees() {
        return helperService.getAllEmployees();
    }

    public List<LeaveDto> getAllPendingLeaves() {
        return service.getAllPendingLeaves();
    }

    public Boolean updateLeave(LeaveDto dto) {
        Boolean isSuccess = service.updateLeave(dto.getId(), dto);
        return isSuccess;
    }

}