package com.xpi.application.views.hrm.employees;

import java.util.stream.Stream;

import com.vaadin.flow.data.provider.AbstractBackEndDataProvider;
import com.vaadin.flow.data.provider.Query;
import com.xpi.application.base.OffsetSizePageRequest;
import com.xpi.application.module.hrm.dto.EmployeeDto;
import com.xpi.application.module.hrm.employees.EmployeeService;
import com.xpi.application.module.hrm.employees.shared.EmployeeFilter;

public class EmployeeDataProvider extends AbstractBackEndDataProvider<EmployeeDto, EmployeeFilter> {

    private static final long serialVersionUID = -2772672175400690881L;

    private EmployeeService service;

    public EmployeeDataProvider(EmployeeService service) {
        super();
        this.service = service;
    }

    @Override
    protected Stream<EmployeeDto> fetchFromBackEnd(Query<EmployeeDto, EmployeeFilter> query) {

        OffsetSizePageRequest pageRequest = new OffsetSizePageRequest(query.getOffset(), query.getLimit());

        return service.getAll(pageRequest, query.getFilter().orElse(null)).stream();

    }

    @Override
    protected int sizeInBackEnd(Query<EmployeeDto, EmployeeFilter> query) {
        return service.count(query.getFilter().orElse(null));
    }
}
