package com.xpi.application.views.hrm.department;

import java.util.stream.Stream;

import com.vaadin.flow.data.provider.AbstractBackEndDataProvider;
import com.vaadin.flow.data.provider.Query;
import com.xpi.application.base.OffsetSizePageRequest;
import com.xpi.application.module.hrm.department.DepartmentService;
import com.xpi.application.module.hrm.department.shared.DepartmentFilter;
import com.xpi.application.module.hrm.dto.DepartmentDto;

public class DepartmentDataProvider extends AbstractBackEndDataProvider<DepartmentDto, DepartmentFilter> {

    private static final long serialVersionUID = -2772672175400690881L;

    private DepartmentService service;

    public DepartmentDataProvider(DepartmentService service) {
        super();
        this.service = service;
    }

    @Override
    protected Stream<DepartmentDto> fetchFromBackEnd(Query<DepartmentDto, DepartmentFilter> query) {

        OffsetSizePageRequest pageRequest = new OffsetSizePageRequest(query.getOffset(), query.getLimit());

        return service.getAll(pageRequest, query.getFilter().orElse(null)).stream();

    }

    @Override
    protected int sizeInBackEnd(Query<DepartmentDto, DepartmentFilter> query) {
        return service.count(query.getFilter().orElseGet(DepartmentFilter::defaultFilter));
    }
}
