package com.xpi.application.views.hrm.contracts.shared;

import lombok.Getter;

@Getter
public class ContractProperties {
    private final String entityName = "Hợp đồng lao động";
    private final String navigateToCreateNewEntity = "hrm/contracts/new";
    private final String navigateToUpdateEntity = "hrm/contracts/";
    private final String mainView = "hrm/contracts";
}
