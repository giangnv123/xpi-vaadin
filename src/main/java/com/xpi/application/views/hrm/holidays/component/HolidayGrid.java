package com.xpi.application.views.hrm.holidays.component;

import java.util.function.Consumer;

import com.xpi.application.module.hrm.dto.HolidayDto;
import com.xpi.application.views.base.abs.AbstractGrid;

public class HolidayGrid extends AbstractGrid<HolidayDto> {

    public HolidayGrid(Consumer<HolidayDto> clickEdit, Consumer<HolidayDto> clickDelete) {
        super(clickEdit, clickDelete);
        initialSetup();
    }

    @Override
    protected void setupGrid() {
        addColumn(dto -> dto.getDate()).setHeader("Ngày nghỉ").setAutoWidth(true);
        addColumn(dto -> dto.getDescription()).setHeader("Ghi chú").setAutoWidth(true);
        addComponentColumn(this::createActionComponents).setHeader("Chức năng").setAutoWidth(true);
    }
}
