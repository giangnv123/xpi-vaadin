package com.xpi.application.views.hrm.insurances.component.form_content;

import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

import org.vaadin.lineawesome.LineAwesomeIcon;

import com.vaadin.flow.component.checkbox.CheckboxGroup;
import com.vaadin.flow.component.checkbox.CheckboxGroupVariant;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.combobox.ComboBox.ItemFilter;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.validator.DoubleRangeValidator;
import com.xpi.application.module.hrm.dto.EmployeeDto;
import com.xpi.application.module.hrm.dto.InsuranceDto;
import com.xpi.application.module.hrm.insurances.shared.InsuranceType;
import com.xpi.application.views.base.abs.AbstractForm;
import com.xpi.application.views.base.component.XFormLayout;
import com.xpi.application.views.base.utils.ColorUtils;
import com.xpi.application.views.base.views.AvatarHelper;
import com.xpi.application.views.base.views.ViewHelper;

public class InsuranceForm extends AbstractForm<InsuranceDto> {

        private static Double defaultContributionRate = 10.5;
        private static List<String> benefits = List.of("Chế độ ốm đau", "Chế độ thai sản",
                        "Chế độ tai nạn lao động và bệnh nghề nghiệp",
                        "Chế độ hưu trí", "Chế độ tử tuất");

        private List<EmployeeDto> employees;
        private Boolean updateMode;
        private EmployeeDto currentEmployee;

        private Consumer<EmployeeDto> setCurrentEmployee;

        /* Basic Details */
        private ComboBox<EmployeeDto> selectEmployeeField;
        private TextField insuranceNumberField;
        private TextField insuranceCompanyField;
        private TextField placeOfIssue;
        private DatePicker issueDateField;

        private CheckboxGroup<String> benefitsField;

        /* percent */
        private NumberField contributionRateField;
        private TextArea notesField;
        private Select<InsuranceType> selectType;
        private NumberField salaryContributeField;
        private NumberField contributeAmountField;

        public InsuranceForm(List<EmployeeDto> employees, Consumer<EmployeeDto> setCurrentEmployee,
                        Boolean updateMode) {
                super(InsuranceDto.class);
                this.setCurrentEmployee = setCurrentEmployee;
                this.employees = employees;
                this.updateMode = updateMode;
                initializeForm();
        }

        @Override
        protected void preBuild() {
                Div insuranceDetails = createInsuranceDetailsContainer();

                Div proportionDetails = createProportionDetailsContainer();

                addClassNames("flex-col-layout", "gap-4");
                add(insuranceDetails, proportionDetails);
        }

        private Div createInsuranceDetailsContainer() {
                Div container = new Div();
                container.add(ViewHelper.titleWithIcon("Thông tin bảo hiểm", LineAwesomeIcon.NEWSPAPER,
                                ColorUtils.WHITE, ColorUtils.BG_SUCCESS));
                container.addClassName("card");

                XFormLayout layout = new XFormLayout();

                insuranceNumberField = new TextField("Mã số bảo hiểm");
                insuranceCompanyField = new TextField("Công ty bảo hiểm");
                placeOfIssue = new TextField("Nơi cấp");

                benefitsField = new CheckboxGroup<>();
                benefitsField.setLabel("Chế độ đãi ngộ");
                benefitsField.setItems(benefits);
                benefitsField.addThemeVariants(CheckboxGroupVariant.LUMO_VERTICAL);
                benefitsField.addThemeVariants(CheckboxGroupVariant.LUMO_HELPER_ABOVE_FIELD);

                selectType = new Select<>();
                selectType.setLabel("Loại bảo hiểm");
                selectType.setItems(InsuranceType.values());
                selectType.setItemLabelGenerator(InsuranceType::toReadableString);
                selectType.addValueChangeListener(event -> updateBenefitsField(event.getValue()));

                if (!updateMode)
                        setupEmployeeField();

                issueDateField = new DatePicker("Ngày tham gia");
                if (selectEmployeeField != null)
                        layout.add(selectEmployeeField);
                layout.add(insuranceNumberField, insuranceCompanyField, placeOfIssue);
                layout.add(issueDateField, selectType, benefitsField);
                layout.setColspan(benefitsField, 2);

                container.add(layout);

                return container;
        }

        private Div createProportionDetailsContainer() {
                Div container = new Div();
                container.addClassName("card");
                container.add(ViewHelper.titleWithIcon("Mức dóng bảo hiểm", LineAwesomeIcon.BRIEFCASE_SOLID,
                                ColorUtils.WHITE, ColorUtils.BG_SUCCESS));

                XFormLayout layout = new XFormLayout();

                salaryContributeField = new NumberField("Mức lương tham gia");
                salaryContributeField.setSuffixComponent(new Span("VND"));
                salaryContributeField.addValueChangeListener(event -> {
                        updateContributeAmount();
                        updateSalaryTextHelper();
                });

                contributeAmountField = new NumberField("Số tiền phải đống");
                contributeAmountField.setSuffixComponent(new Span("VND"));
                contributeAmountField.setReadOnly(true);

                contributionRateField = new NumberField("Phần trăm phải đóng");
                contributionRateField.setSuffixComponent(new Span("%"));
                contributionRateField.setValue(defaultContributionRate);
                contributionRateField.setMax(20);
                contributionRateField.addValueChangeListener(event -> updateContributeAmount());

                notesField = new TextArea("Ghi chú");

                layout.add(contributionRateField, salaryContributeField, contributeAmountField, notesField);
                layout.setColspan(notesField, 2);

                container.add(layout);
                return container;
        }

        private void updateBenefitsField(InsuranceType type) {
                if (type == InsuranceType.Optional) {
                        benefitsField.clear();
                        benefitsField.select("Chế độ hưu trí", "Chế độ tử tuất");
                } else {
                        benefitsField.select(benefits);
                }
        }

        private void setupEmployeeField() {
                selectEmployeeField = new ComboBox<>("Chọn nhân viên");
                ItemFilter<EmployeeDto> filter = (employee,
                                filterString) -> (employee.getFullname() + " "
                                                + employee.getDesignation() + " " + employee.getPhone())
                                                .toLowerCase().indexOf(filterString.toLowerCase()) > -1;

                selectEmployeeField.setItems(filter, Optional.ofNullable(employees).orElse(List.of()));
                selectEmployeeField.setItemLabelGenerator(employee -> employee.getFullname());
                selectEmployeeField.setRenderer(AvatarHelper.createEmployeeRender());
                selectEmployeeField.addValueChangeListener(event -> setCurrentEmployee(event.getValue()));
        }

        public void setCurrentEmployee(EmployeeDto employee) {
                setCurrentEmployee.accept(employee);
                currentEmployee = employee;
                updateContributeAmount();
        }

        private void updateSalaryTextHelper() {
                salaryContributeField
                                .setHelperText(ViewHelper.formatSalary(salaryContributeField.getValue()) + " VND");
        }

        private void updateContributeAmount() {
                if (currentEmployee == null)
                        return;

                Double sallaryContribute = salaryContributeField.getValue();

                if (sallaryContribute != null) {
                        contributeAmountField
                                        .setValue(sallaryContribute * (contributionRateField.getValue() / 100));
                        contributeAmountField.setHelperText(ViewHelper
                                        .formatSalary(sallaryContribute * (contributionRateField.getValue() / 100))
                                        + " VND");
                }
        }

        @Override
        protected void setupBinder() {
                setupBasicInforBind();
                setupBasicProportionBind();
        }

        private void setupBasicInforBind() {

                binder.forField(salaryContributeField)
                                .asRequired("Hãy nhập mức lương tham gia")
                                .bind(InsuranceDto::getContributeAmount, InsuranceDto::setContributeAmount);

                binder.forField(insuranceCompanyField)
                                .asRequired("Hãy nhập công ty bảo hiểm")
                                .bind(InsuranceDto::getInsuranceCompany, InsuranceDto::setInsuranceCompany);

                binder.forField(placeOfIssue)
                                .asRequired("Hãy nhập nơi cấp")
                                .bind(InsuranceDto::getPlaceOfProvision, InsuranceDto::setPlaceOfProvision);

                binder.forField(selectType)
                                .asRequired("Hãy nhập thông tin bao gồm")
                                .bind(InsuranceDto::getType, InsuranceDto::setType);

                binder.forField(issueDateField)
                                .asRequired("Hãy nhập ngày tham gia")
                                .bind(InsuranceDto::getIssueDate, InsuranceDto::setIssueDate);

                if (selectEmployeeField != null) {
                        binder.forField(selectEmployeeField)
                                        .asRequired("Hãy chọn nhân viên")
                                        .bind(InsuranceDto::getEmployee, InsuranceDto::setEmployee);
                }
        }

        private void setupBasicProportionBind() {
                binder.forField(contributionRateField)
                                .asRequired("Hãy nhập phần trăm phải đóng")
                                .withValidator(new DoubleRangeValidator("Chỉ có thể đóng tối đa 20%", 0.0, 20.0))
                                .bind(InsuranceDto::getContributionRate, InsuranceDto::setContributionRate);

                binder.forField(insuranceNumberField)
                                .asRequired("Hãy nhập mã bảo hiểm")
                                .bind(InsuranceDto::getInsuranceNumber, InsuranceDto::setInsuranceNumber);

                binder.forField(notesField)
                                .bind(InsuranceDto::getNotes, InsuranceDto::setNotes);
        }

        public String getEmployeeId() {
                if (selectEmployeeField == null)
                        return null;
                return selectEmployeeField.getValue().getId();
        }

}
