package com.xpi.application.views.hrm.insurances.views;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.xpi.application.module.hrm.dto.InsuranceDto;
import com.xpi.application.views.base.abs.AbstractGridView;
import com.xpi.application.views.base.utils.CustomApiResponse;
import com.xpi.application.views.hrm.insurances.InsurancePresenter;
import com.xpi.application.views.hrm.insurances.component.InsuranceFilterContainer;
import com.xpi.application.views.hrm.insurances.component.InsuranceGrid;
import com.xpi.application.views.hrm.insurances.shared.InsuranceProperties;
import com.xpi.application.views.layout.HRMLayout;

@PageTitle("Bảo hiểm xã hội")
@Route(value = "hrm/insurances", layout = HRMLayout.class)
public class InsuranceGridView extends AbstractGridView<InsuranceDto> {

    private InsuranceProperties properties = new InsuranceProperties();

    private InsurancePresenter presenter;

    /* Components */
    private InsuranceFilterContainer filterContainer;
    private InsuranceGrid grid;

    public InsuranceGrid getGrid() {
        return this.grid;
    }

    public InsuranceGridView(InsurancePresenter presenter) {
        this.entityName = properties.getEntityName();
        this.grid = new InsuranceGrid(this::handleClickEdit, this::handleClickDelete);
        this.presenter = presenter;
        this.presenter.initView(this);
        initialSetup();
    }

    @Override
    protected void setupLayout() {
        this.filterContainer = new InsuranceFilterContainer(presenter::refreshGrid);

        Div header = new Div(filterContainer, createAddEntityButton());
        header.addClassName("header");
        add(header, grid);
    }

    @Override
    protected void performDelete() {
        CustomApiResponse<Void> response = presenter.delete(currentEntity.getId());
        String message = response.getMessage();
        showMessage(message, response.isSuccess());
    }

    @Override
    protected void handleClickEdit(InsuranceDto dto) {
        currentEntity = dto;
        UI.getCurrent().navigate(properties.getNavigateToUpdateEntity() + dto.getId());
    }

    @Override
    protected void handleClickDelete(InsuranceDto dto) {
        currentEntity = dto;
        deleteDialog.open();
    }

    @Override
    protected void handleClickAdd() {
        UI.getCurrent().navigate(properties.getNavigateToCreateNewEntity());
    }
}
