package com.xpi.application.views.hrm.insurances.views;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.xpi.application.module.hrm.dto.InsuranceDto;
import com.xpi.application.views.base.abs.AbstractControlView;
import com.xpi.application.views.base.utils.CustomApiResponse;
import com.xpi.application.views.hrm.insurances.InsurancePresenter;
import com.xpi.application.views.hrm.insurances.component.form_content.CreateFormLayout;
import com.xpi.application.views.hrm.insurances.shared.InsuranceProperties;
import com.xpi.application.views.layout.HRMLayout;

@PageTitle("Quản lý bảo hiểm xã hội")
@Route(value = "hrm/insurances", layout = HRMLayout.class)
public class InsuranceControlView extends AbstractControlView<InsuranceDto> {

    private InsuranceProperties properties = new InsuranceProperties();

    private InsurancePresenter presenter;

    private CreateFormLayout formLayout;

    private boolean isUpdateMode = false;

    @Override
    protected void parameterHandle(String parameter) {
        if (parameter.equals("new")) {
            isUpdateMode = false;
        } else {
            isUpdateMode = true;
            fetchEntity(parameter);
        }
        showContentResult();
    }

    private void showContentResult() {
        if (currentDto == null && isUpdateMode) {
            handleFetchFailedCase();
        } else if (currentDto != null && isUpdateMode) {
            handleFetchSuccessCase();
        } else {
            setupForm();
        }
    }

    private void setupForm() {
        formLayout = new CreateFormLayout(this::performSaveEntity, currentDto,
                presenter.getAllEmployeesDoNotHaveInsurance());
        add(formLayout);
    }

    private void performSaveEntity(InsuranceDto dto) {
        CustomApiResponse<InsuranceDto> response = presenter.save(dto);
        handleSaveOutcome(response);
    }

    private void handleSaveOutcome(CustomApiResponse<InsuranceDto> response) {
        Boolean isSuccess = response.isSuccess();

        if (isSuccess) {
            UI.getCurrent().navigate(properties.getMainView());
        }

        showMessage(response.getMessage(), isSuccess);
    }


    private void fetchEntity(String id) {
        try {
            currentDto = presenter.get(id);
        } catch (NumberFormatException e) {
            currentDto = null;
        }
    }

    private void handleFetchSuccessCase() {
        this.entityName = properties.getEntityName();
        setupForm();
    }

    public InsuranceControlView(InsurancePresenter presenter) {
        this.presenter = presenter;
        initialLayout();
    }

    private void initialLayout() {
        setSizeFull();
    }

    @Override
    protected String getMainViewUrl() {
        return properties.getMainView();
    }
}
