package com.xpi.application.views.hrm.settings;

import com.vaadin.flow.spring.annotation.SpringComponent;
import com.xpi.application.base.ApiResponse;
import com.xpi.application.module.hrm.settings.SettingsService;
import com.xpi.application.module.hrm.settings.WeekendsDto;

@SpringComponent
public class SettingsPresenter {

    SettingsService service;

    /* Basic View */
    public SettingsPresenter(SettingsService service) {
        this.service = service;
    }

    public WeekendsDto getWeekends() {
        ApiResponse<WeekendsDto> response = service.getWeekends();
        if (response.isSuccess()) {
            return response.getData();
        }
        return null;
    }

    public ApiResponse<WeekendsDto> updateOrCreateWeekends(WeekendsDto dto) {
        return service.updateOrCreateWeekends(dto);
    }
}