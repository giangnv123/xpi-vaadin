package com.xpi.application.views.hrm.leaves;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.flow.data.provider.ConfigurableFilterDataProvider;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.xpi.application.base.ApiResponse;
import com.xpi.application.module.hrm.dto.EmployeeDto;
import com.xpi.application.module.hrm.dto.LeaveDto;
import com.xpi.application.module.hrm.leave.LeaveService;
import com.xpi.application.module.hrm.leave.shared.LeaveFilter;
import com.xpi.application.module.hrm.shared.HelperService;
import com.xpi.application.views.base.utils.CustomApiResponse;
import com.xpi.application.views.hrm.leaves.views.LeaveGridView;

@SpringComponent
public class LeavePresenter {
    private static LeaveFilter defaultFilter = LeaveFilter.defaultFilter();

    private ConfigurableFilterDataProvider<LeaveDto, Void, LeaveFilter> filterDataProvider;

    private LeaveDataProvider dataProvider;

    private LeaveService service;

    @Autowired
    private HelperService helperService;

    private LeaveGridView view;

    /* Basic View */
    public LeavePresenter(LeaveService service) {
        this.service = service;
        this.dataProvider = new LeaveDataProvider(service);
        filterDataProvider = dataProvider.withConfigurableFilter();
    }

    public void initView(LeaveGridView view) {
        this.view = view;
        filterDataProvider.setFilter(defaultFilter);
        this.view.getGrid().setItems(filterDataProvider);
    }

    public void refreshGrid(LeaveFilter filter) {
        filterDataProvider.setFilter(filter);
    }

    private void updateView() {
        if (this.view != null)
            this.view.getGrid().setDataProvider(filterDataProvider);
    }

    /* Basic CRUD */
    public CustomApiResponse<Void> delete(String id) {
        ApiResponse<Void> response = service.delete(id);
        if (response.isSuccess())
            updateView();

        return CustomApiResponse.delete(response);
    }

    public CustomApiResponse<LeaveDto> save(LeaveDto dto) {
        CustomApiResponse<LeaveDto> response = service.save(dto);
        if (response.isSuccess())
            updateView();
        return response;
    }

    public LeaveDto get(String id) {
        return service.get(id);
    }

    /* Additional methods */

    public List<EmployeeDto> getAllEmployees() {
        return helperService.getAllEmployees();
    }
}