package com.xpi.application.views.hrm.employees.views;

import java.util.List;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.xpi.application.module.hrm.dto.DepartmentDto;
import com.xpi.application.module.hrm.dto.EmployeeControlDto;
import com.xpi.application.module.hrm.dto.EmployeeDto;
import com.xpi.application.views.base.abs.AbstractControlView;
import com.xpi.application.views.base.utils.CustomApiResponse;
import com.xpi.application.views.hrm.employees.EmployeePresenter;
import com.xpi.application.views.hrm.employees.component.form_content.CreateFormLayout;
import com.xpi.application.views.hrm.employees.shared.EmployeeProperties;
import com.xpi.application.views.hrm.shared.SelectDepartment;
import com.xpi.application.views.layout.HRMLayout;

@PageTitle("Quản lý nhân viên")
@Route(value = "hrm/employees", layout = HRMLayout.class)
public class EmployeeControlView extends AbstractControlView<EmployeeDto> {

    private SelectDepartment selectDepartment;

    private EmployeeProperties properties = new EmployeeProperties();

    private boolean isUpdateMode = false;

    private EmployeePresenter presenter;

    private CreateFormLayout formLayout;

    @Override
    protected void parameterHandle(String parameter) {
        if (parameter.equals("new")) {
            isUpdateMode = false;
        } else {
            isUpdateMode = true;
            fetchEntity(parameter);
        }
        showContentResult();
    }

    private void showContentResult() {
        if (currentDto == null && isUpdateMode) {
            handleFetchFailedCase();
        } else if (currentDto != null && isUpdateMode) {
            handleFetchSuccessCase();
        } else {
            setupForm();
        }
    }

    private void handleFetchSuccessCase() {
        setupForm();
    }

    private void fetchEntity(String id) {
        try {
            currentDto = presenter.get(id);
        } catch (NumberFormatException e) {
            currentDto = null;
        }
    }

    public EmployeeControlView(EmployeePresenter presenter) {
        this.entityName = properties.getEntityName();
        this.presenter = presenter;
        initialLayout();
    }

    private void initialLayout() {
        setupSelectDepartment();
        setSizeFull();
    }

    private void setupSelectDepartment() {
        selectDepartment = new SelectDepartment(this::getDepartment, this::saveDepartment);
    }

    private List<DepartmentDto> getDepartment(Void v) {
        return presenter.getDepartments();
    }

    protected CustomApiResponse<DepartmentDto> saveDepartment(DepartmentDto dto) {
        return presenter.saveDepartment(dto);
    }

    private void setupForm() {
        formLayout = new CreateFormLayout(this::performSaveEntity,
                currentDto, selectDepartment);
        add(formLayout);
    }

    private void performSaveEntity(EmployeeControlDto dto) {
        CustomApiResponse<EmployeeDto> response = null;
        response = presenter.save(dto);
        handleSaveOutcome(response);
    }

    private void handleSaveOutcome(CustomApiResponse<EmployeeDto> response) {
        Boolean isSuccess = response.getIsSuccess();
        if (isSuccess) {
            UI.getCurrent().navigate(properties.getMainViewUrl());
        } else {
            formLayout.resetMultipartFile();
        }
        showMessage(response.getMessage(), isSuccess);
    }

    @Override
    protected String getMainViewUrl() {
        return properties.getMainViewUrl();
    }
}
