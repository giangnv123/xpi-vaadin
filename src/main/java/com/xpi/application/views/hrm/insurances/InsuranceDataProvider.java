package com.xpi.application.views.hrm.insurances;

import java.util.stream.Stream;

import com.vaadin.flow.data.provider.AbstractBackEndDataProvider;
import com.vaadin.flow.data.provider.Query;
import com.xpi.application.base.OffsetSizePageRequest;
import com.xpi.application.module.hrm.dto.InsuranceDto;
import com.xpi.application.module.hrm.insurances.InsuranceService;
import com.xpi.application.module.hrm.insurances.shared.InsuranceFilter;

public class InsuranceDataProvider extends AbstractBackEndDataProvider<InsuranceDto, InsuranceFilter> {

    private static final long serialVersionUID = -2772672175400690881L;

    private InsuranceService service;

    public InsuranceDataProvider(InsuranceService service) {
        super();
        this.service = service;
    }

    @Override
    protected Stream<InsuranceDto> fetchFromBackEnd(Query<InsuranceDto, InsuranceFilter> query) {

        OffsetSizePageRequest pageRequest = new OffsetSizePageRequest(query.getOffset(), query.getLimit());

        return service.getAll(pageRequest, query.getFilter().orElse(null)).stream();
    }

    @Override
    protected int sizeInBackEnd(Query<InsuranceDto, InsuranceFilter> query) {
        return service.count(query.getFilter().orElse(null));
    }
}
