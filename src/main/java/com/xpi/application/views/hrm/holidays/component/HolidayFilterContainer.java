package com.xpi.application.views.hrm.holidays.component;

import java.util.function.Consumer;

import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.xpi.application.module.hrm.holidays.shared.HolidayFilter;
import com.xpi.application.views.base.abs.AbstractFilterContainer;

public class HolidayFilterContainer extends AbstractFilterContainer<HolidayFilter> {
    public HolidayFilterContainer(Consumer<HolidayFilter> applyFilter) {
        super(applyFilter);
        buildLayout();
    }

    @Override
    protected HorizontalLayout createFilterComponent() {
        HorizontalLayout filterComponents = new HorizontalLayout();
        filterComponents.add(searchField);
        return filterComponents;
    }

    @Override
    protected void clearFilterField() {
        searchField.clear();
    }

    @Override
    protected HolidayFilter getFilterValue() {
        return new HolidayFilter(searchField.getValue());
    }

    @Override
    protected HolidayFilter getDefaultFilter() {
        return HolidayFilter.defaultFilter();
    }
}
