package com.xpi.application.views.hrm.department.component;

import java.util.function.Consumer;

import com.xpi.application.module.hrm.dto.DepartmentDto;
import com.xpi.application.views.base.abs.AbstractGrid;

public class DepartmentGrid extends AbstractGrid<DepartmentDto> {

    public DepartmentGrid(Consumer<DepartmentDto> clickEdit, Consumer<DepartmentDto> clickDelete) {
        super(clickEdit, clickDelete);
        initialSetup();
    }

    @Override
    protected void setupGrid() {
        addColumn(dto -> dto.getName()).setHeader("Tên phòng ban").setAutoWidth(true);
        addColumn(dto -> dto.getDescription()).setHeader("Mô tả").setAutoWidth(true);
        addColumn(dto -> dto.getManager()).setHeader("Quản lý").setAutoWidth(true);
        addColumn(dto -> dto.getEmployees().size()).setHeader("Tổng số nhân viên").setAutoWidth(true);
        addComponentColumn(this::createActionComponents).setHeader("Chức năng").setAutoWidth(true);
    }

}
