package com.xpi.application.views.hrm.employees.component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.function.Consumer;

import org.vaadin.lineawesome.LineAwesomeIcon;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.avatar.Avatar;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H5;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.xpi.application.module.hrm.contracts.shared.ContractStatus;
import com.xpi.application.module.hrm.dto.ContractDto;
import com.xpi.application.module.hrm.dto.EmployeeDto;
import com.xpi.application.views.base.abs.AbstractGrid;
import com.xpi.application.views.base.views.AvatarHelper;
import com.xpi.application.views.base.views.ViewHelper;

public class EmployeeGrid extends AbstractGrid<EmployeeDto> {

    public EmployeeGrid(Consumer<EmployeeDto> clickEdit,
            Consumer<EmployeeDto> clickDelete) {
        super(clickEdit, clickDelete);
        initialSetup();
    }

    @Override
    protected void setupGrid() {
        addColumn(basicInfor()).setHeader("Nhân viên").setAutoWidth(true);
        addColumn(contactInfo()).setHeader("Thông tin liên lạc").setAutoWidth(true);
        addColumn(displaySalary()).setHeader("Lương").setAutoWidth(true);
        addColumn(displayDesignation()).setHeader("Chức vụ").setAutoWidth(true);
        addColumn(displayDayStart()).setHeader("Ngày vào làm việc").setAutoWidth(true);
        addColumn(displayContract()).setHeader("Tình trạng hợp đồng").setAutoWidth(true);
        addColumn(displayStatus()).setHeader("Trạng Thái").setAutoWidth(true);
    }

    private ComponentRenderer<Component, EmployeeDto> basicInfor() {
        // common name,
        return new ComponentRenderer<>(dto -> {
            Div info = new Div();
            info.addClassNames("flex-col-layout");

            info.add(new H5(dto.getFullname()));
            info.add(ViewHelper.createInfoItemWithIcon(LineAwesomeIcon.BUILDING_SOLID,
                    new Span(dto.getDepartment().getName()), null));

            Avatar image = AvatarHelper.createDefaultAvatar(dto.getPhoto(), dto.getFullname());
            Div basicInfo = new Div();
            basicInfo.addClassNames("flex-layout");
            basicInfo.add(image, info);
            return basicInfo;
        });
    }

    private ComponentRenderer<Component, EmployeeDto> displaySalary() {
        return new ComponentRenderer<>(dto -> {
            return new Span(ViewHelper.formatSalary(dto.getSalary()) + " VND");
        });
    }

    private ComponentRenderer<Component, EmployeeDto> displayDesignation() {
        return new ComponentRenderer<>(dto -> {
            return ViewHelper.createLimitTextComponent(dto.getDesignation());
        });
    }

    private ComponentRenderer<Component, EmployeeDto> contactInfo() {
        return new ComponentRenderer<>(dto -> {
            Div info = new Div();
            info.addClassNames("flex-col-layout");

            Span emailField = ViewHelper.createLimitTextComponent(dto.getEmail());

            info.add(
                    ViewHelper.createInfoItemWithIcon(LineAwesomeIcon.PHONE_ALT_SOLID, new Span(dto.getPhone()), null));
            info.add(ViewHelper.createInfoItemWithIcon(LineAwesomeIcon.ENVELOPE, emailField, null));
            return info;
        });
    }

    private ComponentRenderer<Component, EmployeeDto> displayStatus() {
        return new ComponentRenderer<>(dto -> {
            return ViewHelper.createStatusComponent(dto.getStatus().toReadableString(),
                    dto.getStatus().toString().toLowerCase());
        });
    }

    private ComponentRenderer<Component, EmployeeDto> displayDayStart() {
        return new ComponentRenderer<>(dto -> {
            LocalDate dateStart = dto.getDateStart();
            String formattedDate = dateStart.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));

            Span dateSpan = new Span(formattedDate);
            return dateSpan;
        });
    }

    private ComponentRenderer<Component, EmployeeDto> displayContract() {
        return new ComponentRenderer<>(dto -> {
            ContractStatus contractTextStatus;

            List<ContractDto> contractList = dto.getContract();

            if (contractList.isEmpty()) {
                contractTextStatus = ContractStatus.Empty;
            } else if (contractList.stream()
                    .anyMatch(contract -> ContractStatus.Running.equals(contract.getStatus()))) {
                contractTextStatus = ContractStatus.Running;
            } else {
                contractTextStatus = ContractStatus.Expired;
            }

            Span status = new Span(String.valueOf(contractTextStatus.toReadableString()));
            status.addClassName("status");
            status.addClassName(contractTextStatus.toString().toLowerCase());
            return status;
        });
    }
}
