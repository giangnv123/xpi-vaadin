package com.xpi.application.views.hrm.employees.views;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.xpi.application.module.hrm.dto.EmployeeDto;
import com.xpi.application.views.base.abs.AbstractGridView;
import com.xpi.application.views.base.utils.CustomApiResponse;
import com.xpi.application.views.hrm.employees.EmployeePresenter;
import com.xpi.application.views.hrm.employees.component.EmployeeFilterContainer;
import com.xpi.application.views.hrm.employees.component.EmployeeGrid;
import com.xpi.application.views.hrm.employees.shared.EmployeeProperties;
import com.xpi.application.views.layout.HRMLayout;

@PageTitle("Nhân viên")
@Route(value = "hrm/employees", layout = HRMLayout.class)
public class EmployeeGridView extends AbstractGridView<EmployeeDto> {

    private EmployeeProperties properties = new EmployeeProperties();

    private EmployeeFilterContainer filterContainer;
    private EmployeeGrid grid;
    private EmployeePresenter presenter;

    public EmployeeGrid getGrid() {
        return this.grid;
    }

    public EmployeeGridView(EmployeePresenter presenter) {
        this.presenter = presenter;
        this.entityName = properties.getEntityName();
        this.grid = new EmployeeGrid(this::handleClickEdit, this::handleClickDelete);
        this.filterContainer = new EmployeeFilterContainer(presenter::refreshGrid, presenter.getDepartments());
        this.presenter.initView(this);
        initialSetup();
    }

    @Override
    protected void setupLayout() {
        Div header = new Div(filterContainer, createAddEntityButton());
        header.addClassName("header");
        add(header, grid);
    }

    @Override
    protected void performDelete() {
        CustomApiResponse<EmployeeDto> response = presenter.delete(currentEntity.getId(), currentEntity);
        String message = response.getMessage();
        showMessage(message, response.getIsSuccess());
    }

    @Override
    protected void handleClickEdit(EmployeeDto dto) {
        currentEntity = dto;
        UI.getCurrent().navigate(properties.getUpdateViewUrl() + dto.getId());
    }

    @Override
    protected void handleClickDelete(EmployeeDto dto) {
        currentEntity = dto;
        deleteDialog.open();
    }

    @Override
    protected void handleClickAdd() {
        UI.getCurrent().navigate(properties.getNewViewUrl());
    }
}
