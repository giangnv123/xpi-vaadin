package com.xpi.application.views.hrm.leaves.component.form_content;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

import org.vaadin.lineawesome.LineAwesomeIcon;

import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.combobox.ComboBox.ItemFilter;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.xpi.application.module.hrm.dto.EmployeeDto;
import com.xpi.application.module.hrm.dto.LeaveDto;
import com.xpi.application.module.hrm.leave.shared.LeaveStatus;
import com.xpi.application.views.base.abs.AbstractForm;
import com.xpi.application.views.base.component.XFormLayout;
import com.xpi.application.views.base.utils.ColorUtils;
import com.xpi.application.views.base.utils.DateUtils;
import com.xpi.application.views.base.views.AvatarHelper;
import com.xpi.application.views.base.views.ViewHelper;

public class LeaveForm extends AbstractForm<LeaveDto> {

        private List<EmployeeDto> employees;
        private Boolean updateMode;
        private Consumer<EmployeeDto> updateCurrentEmployeeUI;

        private EmployeeDto currentEmployee = null;

        /* Job Detail */

        /* Contract Details */
        private ComboBox<EmployeeDto> selectEmployeeField;
        private TextField titleField;
        private TextArea descriptionField;
        private Select<LeaveStatus> statusField;
        private DatePicker startDateField;
        private IntegerField dayOffField;
        private DatePicker endDateField;
        private Checkbox setPaidLeaveField;

        public LeaveForm(List<EmployeeDto> employees, Consumer<EmployeeDto> updateCurrentEmployeeUI,
                        Boolean updateMode) {
                super(LeaveDto.class);
                this.updateMode = updateMode;
                this.updateCurrentEmployeeUI = updateCurrentEmployeeUI;
                this.employees = employees;
                initializeForm();
        }

        @Override
        protected void preBuild() {
                Div leaveDetailContainer = createLeaveDetailsContainer();
                addClassNames("flex-col-layout", "gap-4");
                add(leaveDetailContainer);
        }

        private Div createLeaveDetailsContainer() {

                Div container = new Div();
                container.add(ViewHelper.titleWithIcon("Thông tin nghỉ phép", LineAwesomeIcon.HANDSHAKE_SOLID,
                                ColorUtils.WHITE, ColorUtils.BG_SUCCESS));

                container.addClassName("card");

                XFormLayout layout = new XFormLayout();

                titleField = new TextField("Tiêu đề");
                descriptionField = new TextArea("Ghi chú");

                dayOffField = new IntegerField("Số ngày nghỉ");
                dayOffField.setValue(0);
                dayOffField.setReadOnly(true);
                dayOffField.setMin(0);

                setPaidLeaveField = new Checkbox("Tính phép");
                setPaidLeaveField.setTooltipText("Vẫn được trả lương khi nghỉ phép");

                statusField = new Select<>();
                statusField.setLabel("Trạng thái");
                statusField.setItems(LeaveStatus.values());
                statusField.setValue(LeaveStatus.Pending);
                statusField.setItemLabelGenerator(item -> item.toReadableString());

                if (!updateMode && employees != null)
                        setupEmployeeField();

                startDateField = new DatePicker("Ngày bắt đầu");

                if (!updateMode) {
                        startDateField.setMin(LocalDate.now());
                }

                startDateField.addValueChangeListener(event -> {
                        dateStartChanged(event.getValue());
                });

                endDateField = new DatePicker("Ngày kết thúc");
                endDateField.setReadOnly(true);
                endDateField.addValueChangeListener(event -> {
                        updatePaidLeaveHandlingUI();
                });

                layout.add(titleField, statusField);
                if (selectEmployeeField != null)
                        layout.add(selectEmployeeField);

                layout.add(startDateField, endDateField, dayOffField, setPaidLeaveField, descriptionField);

                layout.setColspan(descriptionField, 2);
                layout.setColspan(titleField, 2);

                container.add(layout);

                return container;
        }

        private void updatePaidLeaveHandlingUI() {
                updateDayOff();

                if (currentEmployee == null)
                        return;

                int paidDaysOffRemaining = Optional.ofNullable(currentEmployee.getPaidDaysOffRemaining()).orElse(0);

                if (dayOffField.getValue() > paidDaysOffRemaining) {
                        dayOffField.setHelperText(
                                        "Số ngày nghỉ phép còn lại của bạn không đủ");
                        setPaidLeaveField.setValue(false);
                        setPaidLeaveField.setEnabled(false);
                } else {
                        dayOffField.setHelperText("Bạn còn " + paidDaysOffRemaining
                                        + " ngày nghỉ phép");
                        setPaidLeaveField.setEnabled(true);
                }
        }

        private void dateStartChanged(LocalDate date) {
                endDateField.setMin(date);
                endDateField.setReadOnly(false);
                if (endDateField.getValue() == null)
                        endDateField.setValue(date);
                else if (date.isAfter(endDateField.getValue()))
                        endDateField.setValue(date);
                updatePaidLeaveHandlingUI();
        }

        private void updateDayOff() {
                if (startDateField.getValue() != null && endDateField.getValue() != null) {
                        long daysBetween = DateUtils.calcDateBetween(startDateField.getValue(),
                                        endDateField.getValue());
                        dayOffField.setValue((int) daysBetween);
                }
        }

        private void setupEmployeeField() {
                selectEmployeeField = new ComboBox<>("Chọn nhân viên");
                ItemFilter<EmployeeDto> filter = (employee,
                                filterString) -> (employee.getFullname() + " "
                                                + employee.getDesignation() + " " + employee.getPhone())
                                                .toLowerCase().indexOf(filterString.toLowerCase()) > -1;

                selectEmployeeField.setItems(filter, Optional.ofNullable(employees).orElse(List.of()));
                selectEmployeeField.setItemLabelGenerator(employee -> employee.getFullname());
                selectEmployeeField.setRenderer(AvatarHelper.createEmployeeRender());
                selectEmployeeField.addValueChangeListener(event -> updateCurrentEmployeeUI(event.getValue()));
        }

        public void updateCurrentEmployeeUI(EmployeeDto employee) {
                if (!updateMode) {
                        updateCurrentEmployeeUI.accept(selectEmployeeField.getValue());
                }

                currentEmployee = findTargetEmployee(employee.getId());
                updatePaidLeaveHandlingUI();
        }

        private EmployeeDto findTargetEmployee(String employeeId) {
                return employees.stream().filter(emp -> emp.getId() == employeeId).findFirst().orElse(null);
        }

        @Override
        protected void setupBinder() {
                setupContractDetailsBinder();
        }

        private void setupContractDetailsBinder() {
                binder.forField(titleField)
                                .asRequired("Hãy nhập tiêu đề")
                                .bind(LeaveDto::getTitle, LeaveDto::setTitle);

                binder.forField(statusField)
                                .asRequired("Hãy chọn trạng thái")
                                .bind(LeaveDto::getStatus, LeaveDto::setStatus);

                binder.forField(startDateField)
                                .asRequired("Hãy chọn ngày bắt đầu nghỉ phép")
                                .bind(LeaveDto::getDateStart, LeaveDto::setDateStart);

                binder.forField(endDateField)
                                .asRequired("Hãy chọn ngày kết thúc nghỉ phép")
                                .withValidator(endDateField -> endDateField
                                                .isAfter(startDateField.getValue().plusDays(-1)),
                                                "Ngày kết thúc phải sau ngày bắt đầu")
                                .bind(LeaveDto::getDateEnd, LeaveDto::setDateEnd);

                binder.forField(descriptionField)
                                .bind(LeaveDto::getDescription, LeaveDto::setDescription);

                binder.forField(setPaidLeaveField)
                                .bind(LeaveDto::getPaidLeave, LeaveDto::setPaidLeave);

                if (!updateMode) {
                        binder.forField(selectEmployeeField)
                                        .asRequired("Hãy chọn nhân viên")
                                        .bind(LeaveDto::getEmployee, LeaveDto::setEmployee);
                }
        }

        public String getEmployeeId() {
                if (selectEmployeeField == null)
                        return null;
                return selectEmployeeField.getValue().getId();
        }

}
