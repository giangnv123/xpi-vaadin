package com.xpi.application.views.hrm.workdays.shared;

import lombok.Getter;

@Getter
public class WorkdayProperties {
    private final String entityName = "Workday";
    private final String navigateToCreateNewEntity = "workdays/new";
    private final String navigateToUpdateEntity = "workdays/";
    private final String mainView = "workdays";
}
