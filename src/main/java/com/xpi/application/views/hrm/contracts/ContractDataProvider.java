package com.xpi.application.views.hrm.contracts;

import java.util.stream.Stream;

import com.vaadin.flow.data.provider.AbstractBackEndDataProvider;
import com.vaadin.flow.data.provider.Query;
import com.xpi.application.base.OffsetSizePageRequest;
import com.xpi.application.module.hrm.contracts.ContractService;
import com.xpi.application.module.hrm.contracts.shared.ContractFilter;
import com.xpi.application.module.hrm.dto.ContractDto;

public class ContractDataProvider extends AbstractBackEndDataProvider<ContractDto, ContractFilter> {

    private static final long serialVersionUID = -2772672175400690881L;

    private ContractService service;

    public ContractDataProvider(ContractService service) {
        super();
        this.service = service;
    }

    @Override
    protected Stream<ContractDto> fetchFromBackEnd(Query<ContractDto, ContractFilter> query) {
        OffsetSizePageRequest pageRequest = new OffsetSizePageRequest(query.getOffset(), query.getLimit());
        return service.getAll(pageRequest, query.getFilter().orElse(null)).stream();
    }

    @Override
    protected int sizeInBackEnd(Query<ContractDto, ContractFilter> query) {
        return service.count(query.getFilter().orElse(null));
    }
}
