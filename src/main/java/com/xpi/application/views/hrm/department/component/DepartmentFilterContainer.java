package com.xpi.application.views.hrm.department.component;

import java.util.function.Consumer;

import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.xpi.application.module.hrm.department.shared.DepartmentFilter;
import com.xpi.application.views.base.abs.AbstractFilterContainer;

public class DepartmentFilterContainer extends AbstractFilterContainer<DepartmentFilter> {
    public DepartmentFilterContainer(Consumer<DepartmentFilter> applyFilter) {
        super(applyFilter);
        buildLayout();
    }

    @Override
    protected HorizontalLayout createFilterComponent() {
        HorizontalLayout filterComponents = new HorizontalLayout();
        filterComponents.add(searchField);
        return filterComponents;
    }

    @Override
    protected void clearFilterField() {
        searchField.clear();
    }

    @Override
    protected DepartmentFilter getFilterValue() {
        return new DepartmentFilter(searchField.getValue());
    }

    @Override
    protected DepartmentFilter getDefaultFilter() {
        return DepartmentFilter.defaultFilter();
    }
}
