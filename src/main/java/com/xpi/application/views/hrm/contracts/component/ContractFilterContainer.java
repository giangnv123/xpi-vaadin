package com.xpi.application.views.hrm.contracts.component;

import java.util.function.Consumer;

import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.xpi.application.module.hrm.contracts.shared.ContractFilter;
import com.xpi.application.module.hrm.contracts.shared.ContractStatus;
import com.xpi.application.module.hrm.contracts.shared.ContractType;
import com.xpi.application.views.base.abs.AbstractFilterContainer;

public class ContractFilterContainer extends AbstractFilterContainer<ContractFilter> {
    private static final String TYPE_PLACEHOLDER = "Loại hợp đồng";
    private static final String STATUS_PLACEHOLDER = "Trạng thái";

    private ComboBox<ContractType> contractTypeField;
    private ComboBox<ContractStatus> contractStatusField;

    public ContractFilterContainer(Consumer<ContractFilter> applyFilter) {
        super(applyFilter);
        buildLayout();
    }

    @Override
    protected HorizontalLayout createFilterComponent() {
        initializeContractTypeField();
        initializeContractStatusField();

        HorizontalLayout filterComponents = new HorizontalLayout();
        filterComponents.add(searchField, contractTypeField, contractStatusField);
        return filterComponents;
    }

    private void initializeContractTypeField() {
        contractTypeField = new ComboBox<>();
        contractTypeField.setItems(ContractType.values());
        contractTypeField.setItemLabelGenerator(ContractType::toReadableString);
        contractTypeField.setPlaceholder(TYPE_PLACEHOLDER);
        contractTypeField.addValueChangeListener(e -> applyFilters());
    }

    private void initializeContractStatusField() {
        contractStatusField = new ComboBox<>();
        contractStatusField.setItems(ContractStatus.getValuesExceptEmpty());
        contractStatusField.setItemLabelGenerator(ContractStatus::toReadableString);
        contractStatusField.setPlaceholder(STATUS_PLACEHOLDER);
        contractStatusField.addValueChangeListener(e -> applyFilters());
    }

    @Override
    protected void clearFilterField() {
        searchField.clear();
        contractTypeField.clear();
        contractStatusField.clear();
    }

    @Override
    protected ContractFilter getFilterValue() {
        return new ContractFilter(searchField.getValue(), contractTypeField.getValue(), contractStatusField.getValue());
    }

    @Override
    protected ContractFilter getDefaultFilter() {
        return ContractFilter.defaultFilter();
    }
}
