package com.xpi.application.views.hrm.holidays.views;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.xpi.application.module.hrm.dto.HolidayDto;
import com.xpi.application.views.base.abs.AbstractBasicGridView;
import com.xpi.application.views.base.component.XConfirmDialog;
import com.xpi.application.views.base.component.XDialog;
import com.xpi.application.views.base.utils.CustomApiResponse;
import com.xpi.application.views.hrm.holidays.HolidayPresenter;
import com.xpi.application.views.hrm.holidays.component.Form;
import com.xpi.application.views.hrm.holidays.component.HolidayFilterContainer;
import com.xpi.application.views.hrm.holidays.component.HolidayGrid;
import com.xpi.application.views.layout.HRMLayout;

@PageTitle("Kỳ nghỉ phép")
@Route(value = "hrm/holidays", layout = HRMLayout.class)
public class HolidayGridView extends AbstractBasicGridView<HolidayDto, Long> {

    private HolidayPresenter presenter;

    /* Components */
    private HolidayFilterContainer filterContainer;
    private HolidayGrid grid;
    /* Components */

    private Form form = new Form();

    public HolidayGrid getGrid() {
        return this.grid;
    }

    public HolidayGridView(HolidayPresenter presenter) {
        this.entityName = "Kỳ nghỉ";
        this.presenter = presenter;
        grid = new HolidayGrid(this::handleClickEdit, this::handleClickDelete);
        this.presenter.initView(this);
        initialSetup();
    }

    @Override
    protected Div createHeaderSection() {
        this.filterContainer = new HolidayFilterContainer(presenter::refreshGrid);
        Div header = new Div(filterContainer, createAddEntityButton());
        header.addClassName("header");
        return header;
    }

    @Override
    protected Component createContentSection() {
        return grid;
    }

    private void performSave() {
        if (!form.validateAndWrite())
            return;

        HolidayDto dto = form.getEntity();

        Boolean isSuccess = false;

        isSuccess = save(dto);

        if (isSuccess) {
            controlDialog.close();
            form.resetForm();
        }
    }

    protected Boolean save(HolidayDto dto) {
        CustomApiResponse<HolidayDto> response = presenter.save(dto);
        showMessage(response.getMessage(), response.isSuccess());
        return response.isSuccess();
    }

    protected void setupDialog() {
        setupControlDialog();
        deleteDialog = new XConfirmDialog(entityName, this::performDelete);
    }

    protected void setupControlDialog() {
        controlDialog = new XDialog(entityName, this::performSave);
        controlDialog.add(form);
    }

    protected void performDelete() {
        CustomApiResponse<Void> response = presenter.delete(currentId);
        showMessage(response.getMessage(), response.isSuccess());
    }

    protected void handleClickEdit(HolidayDto dto) {
        form.setEntity(dto);
        controlDialog.open();
    }

    protected void handleClickDelete(HolidayDto dto) {
        currentId = dto.getId();
        deleteDialog.open();
    }

    protected void handleClickAdd() {
        form.setEntity(new HolidayDto());
        controlDialog.open();
    }
}
