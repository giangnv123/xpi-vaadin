package com.xpi.application.views.base.views;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Base64;

import com.vaadin.flow.component.avatar.Avatar;
import com.vaadin.flow.component.avatar.AvatarVariant;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H5;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.renderer.Renderer;
import com.xpi.application.module.hrm.dto.EmployeeDto;
import com.xpi.application.views.base.utils.SharedProperties;

public class AvatarHelper {

    public static Image createAvatar(String url, String className) {
        Image image = new Image();
        String imageUrl = url != null ? SharedProperties.API_SERVE_FILE + url
                : SharedProperties.DEFAULT_AVATAR;
        image.setSrc(imageUrl);
        if (className != null)
            image.addClassName(className);
        return image;
    }

    public static Avatar createDefaultAvatar(String url, String fullname) {
        Avatar avatar = new Avatar(fullname);
        avatar.addThemeVariants(AvatarVariant.LUMO_LARGE);
        avatar.setImage(SharedProperties.API_SERVE_FILE + url);
        return avatar;
    }

    public static Renderer<EmployeeDto> createEmployeeRender() {
        return new ComponentRenderer<>(employee -> {
            Avatar avatar = createDefaultAvatar(employee.getPhoto(), employee.getFullname());

            Div name = new Div(new Span(employee.getFullname()));
            Div phone = new Div(new Span(employee.getPhone()));

            Div inforContainer = new Div(name, phone);
            inforContainer.addClassName("flex-col-layout");

            Div layout = new Div(avatar, inforContainer);
            layout.addClassName("flex-layout");
            return layout;
        });
    }

    public static Image createPreviewImage(File file) {
        try {
            byte[] fileContent = Files.readAllBytes(file.toPath());

            String base64Image = Base64.getEncoder().encodeToString(fileContent);

            Image uploadedImage = new Image("data:image/png;base64," + base64Image, "Uploaded Image");
            uploadedImage.addClassName("employee-avatar-create");
            return uploadedImage;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Div displayBasicEmployeeInforWithAvatar(EmployeeDto dto) {
        Avatar avatar = createDefaultAvatar(dto.getPhoto(), dto.getFullname());

        Div info = new Div(new H5(dto.getFullname()), new Span(dto.getPhone()));
        info.addClassName("flex-col-layout");

        Div container = new Div(avatar, info);
        container.addClassName("flex-layout");
        return container;
    }

}
