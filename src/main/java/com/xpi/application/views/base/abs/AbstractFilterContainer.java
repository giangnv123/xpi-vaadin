package com.xpi.application.views.base.abs;

import java.util.function.Consumer;

import org.vaadin.lineawesome.LineAwesomeIcon;

import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.details.Details;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.xpi.application.views.base.views.ViewHelper;

public abstract class AbstractFilterContainer<F> extends Div {

    protected TextField searchField = new TextField();

    private Button resetButton = new Button(LineAwesomeIcon.TIMES_CIRCLE_SOLID.create());

    protected Consumer<F> applyFilter;

    public AbstractFilterContainer(Consumer<F> applyFilter) {
        this.applyFilter = applyFilter;
    }

    private HorizontalLayout createButtonLayout() {

        resetButton.addClickListener(event -> resetFilters());

        HorizontalLayout layout = new HorizontalLayout();
        layout.add(resetButton);
        return layout;
    }

    protected void setupSearchComponent() {
        searchField.setWidth("17.5vw");
        searchField.setPlaceholder(searchPlaceHolder());
        searchField.setMinWidth("150px");
        searchField.setPrefixComponent(ViewHelper.createIcon(LineAwesomeIcon.SEARCH_SOLID, null, null, null));
        searchField.addValueChangeListener(event -> {
            applyFilters();
        });
    }

    protected void setupFilterComponent() {
        setupSearchComponent();
    }

    protected abstract HorizontalLayout createFilterComponent();

    protected void buildLayout() {

        setupFilterComponent();

        Div filters = new Div(createFilterComponent(), createButtonLayout());
        filters.addClassName("filter-container");

        Span filterCom = new Span(ViewHelper.createIcon(LineAwesomeIcon.FILTER_SOLID, null, null, null),
                new Text("Lọc"));

        Details details = new Details(filterCom, filters);
        details.setOpened(false);

        Div container = new Div(details);

        add(container);
    }

    protected void applyFilters() {
        applyFilter.accept(getFilterValue());
    }

    private void resetFilters() {
        searchField.clear();
        clearFilterField();
        applyFilter.accept(getDefaultFilter());
    }

    protected abstract void clearFilterField();

    protected abstract F getFilterValue();

    protected abstract F getDefaultFilter();

    protected String searchPlaceHolder() {
        return "Tìm kiếm...";
    };
}
