package com.xpi.application.views.base.abs;

import org.vaadin.lineawesome.LineAwesomeIcon;

import com.vaadin.flow.component.button.Button;
import com.xpi.application.views.base.component.XConfirmDialog;
import com.xpi.application.views.base.utils.TextNotification;

public abstract class AbstractGridView<T> extends AbstractView {
    protected XConfirmDialog deleteDialog;

    protected TextNotification notification = new TextNotification();

    protected String entityName = "Item";
    protected T currentEntity;

    public AbstractGridView() {
    }

    private void setupDeleteDialog() {
        deleteDialog = new XConfirmDialog(entityName, this::performDelete);
    }

    protected void initialSetup() {
        setSizeFull();
        setupLayout();
        setupDeleteDialog();
    }

    protected Button createAddEntityButton() {
        Button button = new Button(LineAwesomeIcon.PLUS_CIRCLE_SOLID.create());
        button.addClickListener(event -> {
            handleClickAdd();
        });
        button.addClassName("absolute-position");
        return button;
    }

    protected abstract void setupLayout();

    protected abstract void performDelete();

    protected abstract void handleClickEdit(T dto);

    protected abstract void handleClickDelete(T dto);

    protected abstract void handleClickAdd();

}
