package com.xpi.application.views.base.abs;

import com.xpi.application.base.GenericHttpClient;

public abstract class AbstractPresenter<T, F, ID> {

    protected abstract GenericHttpClient<T, F, ID> getClient();
}
