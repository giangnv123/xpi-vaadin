package com.xpi.application.views.base.views;

import org.vaadin.lineawesome.LineAwesomeIcon;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.SvgIcon;
import com.xpi.application.views.base.utils.ColorUtils;

public class EditComponent {

    public static Div createEditableDiv(
            Component detailDiv,
            Component editComponent,
            Runnable onSave) {

        Div container = new Div();

        Span editSpan = createEditSpan();

        Div contentDiv = new Div();
        contentDiv.addClassName("editable-content-div");
        contentDiv.add(detailDiv, editSpan);

        Runnable onCancel = () -> {
            container.removeAll();
            container.add(contentDiv);
        };

        Div controlBox = createControlEditBox(onCancel, onSave);

        Div controlDiv = new Div(editComponent, controlBox);
        controlDiv.addClassName("editable-control-div");

        editSpan.addClickListener(e -> {
            container.removeAll();
            container.add(editComponent, controlBox);
        });

        container.add(contentDiv);
        return container;
    }

    private static Span createEditSpan() {
        SvgIcon editIcon = ViewHelper.createIcon(LineAwesomeIcon.EDIT_SOLID, ColorUtils.GRAY, "chỉnh sửa", "20px");
        Span editSpan = new Span(editIcon);
        editIcon.addClassName("editable-icon");
        return editSpan;
    }

    private static Div createControlEditBox(Runnable onCancel, Runnable onSave) {
        Div checkIconContainer = createIconContainer(LineAwesomeIcon.CHECK_SOLID, ColorUtils.SUCCESS, onSave);
        checkIconContainer.addClickShortcut(Key.ENTER);

        Div closeIconContainer = createIconContainer(LineAwesomeIcon.TIMES_SOLID, ColorUtils.DANGER, onCancel);
        closeIconContainer.addClickShortcut(Key.ESCAPE);

        Div controlBox = new Div(checkIconContainer, closeIconContainer);
        controlBox.addClassName("control-box");
        return controlBox;
    }

    private static Div createIconContainer(LineAwesomeIcon icon, String color, Runnable action) {
        SvgIcon svgIcon = ViewHelper.createIcon(icon, color, null, "20px");
        svgIcon.getStyle().set("cursor", "pointer");

        Div iconContainer = new Div();
        iconContainer.addClassName("icon-control-container");
        iconContainer.add(svgIcon);
        if (action != null) {
            iconContainer.addClickListener(e -> action.run());
        }
        return iconContainer;
    }
}
