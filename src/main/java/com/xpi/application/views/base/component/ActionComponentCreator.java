package com.xpi.application.views.base.component;

import java.util.function.Consumer;

import org.vaadin.lineawesome.LineAwesomeIcon;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.icon.SvgIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.xpi.application.views.base.utils.ColorUtils;
import com.xpi.application.views.base.views.ViewHelper;

public class ActionComponentCreator<T> extends HorizontalLayout {

    private Consumer<T> editAction;
    private Consumer<T> deleteAction;

    public ActionComponentCreator(Consumer<T> editAction, Consumer<T> deleteAction) {
        this.editAction = editAction;
        this.deleteAction = deleteAction;
    }

    public Component createComponents(T dto) {
        SvgIcon editIcon = ViewHelper.createIcon(LineAwesomeIcon.EDIT_SOLID, ColorUtils.PRIMARY, "Chỉnh sửa", null);
        editIcon.getStyle().set("cursor", "pointer");
        SvgIcon deleteIcon = ViewHelper.createIcon(LineAwesomeIcon.TRASH_SOLID, ColorUtils.DANGER, "Xóa", null);
        deleteIcon.getStyle().set("cursor", "pointer");

        editIcon.addClickListener(e -> editAction.accept(dto));
        deleteIcon.addClickListener(e -> deleteAction.accept(dto));

        HorizontalLayout actionsLayout = new HorizontalLayout(editIcon, deleteIcon);
        return actionsLayout;
    }


}
