package com.xpi.application.views.base.views;

import org.vaadin.lineawesome.LineAwesomeIcon;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.SvgIcon;

public class GridViewHelper {
    public static Component createIconTextHeader(LineAwesomeIcon iconValue, String text) {
        Div headerLayout = new Div();
        headerLayout.addClassName("flex-layout");
        SvgIcon icon = iconValue.create(); // Change icon style as needed
        Span headerSpan = new Span(text);
        headerLayout.add(icon, headerSpan);
        return headerLayout;
    }
}
