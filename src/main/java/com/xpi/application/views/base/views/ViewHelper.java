package com.xpi.application.views.base.views;

import java.text.NumberFormat;
import java.util.Locale;

import org.vaadin.lineawesome.LineAwesomeIcon;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H4;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.SvgIcon;
import com.vaadin.flow.component.shared.Tooltip;
import com.xpi.application.views.base.abs.AbstractForm;
import com.xpi.application.views.base.component.XDialog;
import com.xpi.application.views.base.component.XNotification;

public class ViewHelper {

    protected static XNotification notification = new XNotification();

    public static SvgIcon createIcon(LineAwesomeIcon iconType, String color, String toolTip, String size) {
        SvgIcon icon = iconType.create();

        if (color != null)
            icon.addClassName(color);
        if (size != null)
            icon.setSize(size);
        if (toolTip != null)
            icon.setTooltipText(toolTip);

        return icon;
    }


    public static Div titleWithIcon(String text, LineAwesomeIcon iconType, String iconColor, String bgColor) {
        Div circle = new Div();
        circle.addClassName("circle");
        circle.addClassName(bgColor);

        SvgIcon icon = createIcon(iconType, iconColor, null, null);
        circle.add(icon);

        Div title = new Div();
        title.addClassNames("flex", "items-center", "gap-2");

        title.add(circle, new H4(text));
        return title;
    }

    public static Div cardTitleWithIcon(String text, LineAwesomeIcon iconType, String iconColor, String bgColor) {

        Div circle = new Div();
        circle.addClassName("circle-card");
        circle.addClassName(bgColor);

        SvgIcon icon = createIcon(iconType, iconColor, null, null);
        circle.add(icon);

        Div title = new Div();
        title.addClassNames("flex", "items-center", "gap-2");
        title.add(circle, new H4(text));
        return title;
    }

    public static Div createInfoItemWithIcon(LineAwesomeIcon icon, Component component, String color) {
        Div item = new Div();
        item.addClassName("infor-item");
        SvgIcon svgIcon = createIcon(icon, color, null, null);
        item.add(svgIcon, component);
        return item;
    }

    public static Span createLimitTextComponent(String text) {
        Span span = new Span(text);
        span.addClassName("inline-cell-dot");

        Tooltip.forComponent(span)
                .withText(text)
                .withPosition(Tooltip.TooltipPosition.TOP_START);

        return span;
    }

    public static String formatSalary(Double number) {
        if (number == null) {
            return "0.00";
        }
        NumberFormat formatter = NumberFormat.getNumberInstance(Locale.US);
        return formatter.format(number);
    }

    public static void showMessage(String message, Boolean isSuccess) {
        isSuccess = isSuccess != null ? isSuccess : true;
        if (isSuccess) {
            showSuccessMessage(message);
        } else {
            showErrorMessage(message);
        }
    }

    public static void showErrorMessage(String message) {
        notification.createErrorNotification(message);
    }

    public static void showSuccessMessage(String message) {
        notification.createSuccessNotification(message);
    }


    public static Span createStatusComponent(String displayText, String... classNames) {
        Span status = new Span(displayText);
        status.addClassName("status");
        for (String className : classNames) {
            status.addClassName(className);
        }
        return status;
    }

    public static void handleResponse(Boolean isSuccess, String message, XDialog dialog, AbstractForm<?> form,
            Runnable onSuccess) {
        if (isSuccess) {
            dialog.close();
            form.resetForm();
            if (onSuccess != null)
                onSuccess.run();
        }
        showMessage(message, isSuccess);
    }
}
