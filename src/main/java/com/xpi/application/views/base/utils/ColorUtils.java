package com.xpi.application.views.base.utils;

public class ColorUtils {

    public static final String WHITE = "color-white";
    public static final String SUCCESS = "color-success";
    public static final String DANGER = "color-danger";
    public static final String WARNING = "color-warning";
    public static final String GRAY = "color-gray";
    public static final String GRAY_DARK = "color-gray-dark";
    public static final String PRIMARY = "color-primary";
    public static final String PURPLE = "color-purple";

    public static final String BG_WHITE = "bg-color-white";
    public static final String BG_SUCCESS = "bg-color-success";
    public static final String BG_DANGER = "bg-color-danger";
    public static final String BG_WARNING = "bg-color-warning";
    public static final String BG_GRAY = "bg-color-gray";
    public static final String BG_GRAY_DARK = "bg-color-gray-dark";
    public static final String BG_PRIMARY = "bg-color-primary";
    public static final String BG_PURPLE = "bg-color-purple";
}
