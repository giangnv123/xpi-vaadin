package com.xpi.application.views.base.abs;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.xpi.application.base.ApiResponse;

public interface ServiceInterface<T, D, ID, F> {

    public List<T> getEntities(Pageable page, F filter);

    public int countEntities(F filter);

    public T getEntity(ID id);

    public ApiResponse<T> createEntity(T dto);

    public ApiResponse<T> updateEntity(ID id, T dto);

    public ApiResponse<Void> deleteEntity(ID id);
}
