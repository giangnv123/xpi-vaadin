package com.xpi.application.views.base.utils;

public class XUtils {
    public static String PHONE_PATTERN = "\\d{10}|\\+\\d{1,3}[- ]?\\d{10}";
    public static String EMAIL_PATTERN = "^[A-Za-z0-9+_.-]+@[A-Za-z0-9.-]+$";

    public static String INVALID_PHONE_MESSAGE = "Số điện thoại không hợp lệ";
    public static String INVALID_EMAIL_MESSAGE = "Email không hợp lệ";

    public static Boolean isValidPhone(String phone) {
        return phone.matches(PHONE_PATTERN);
    }

    public static Boolean isValidEmail(String email) {
        return email.matches(EMAIL_PATTERN);
    }
}
