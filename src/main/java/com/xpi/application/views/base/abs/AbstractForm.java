package com.xpi.application.views.base.abs;

import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.data.binder.Binder;

public abstract class AbstractForm<T> extends Div {

    protected Binder<T> binder;

    protected T currentEntity;

    private final Class<T> beanClass;

    public AbstractForm(Class<T> beanClass) {
        this.binder = new Binder<>(beanClass);
        this.beanClass = beanClass;
        try {
            this.currentEntity = beanClass.getDeclaredConstructor().newInstance();
        } catch (Exception e) {
            throw new RuntimeException("Unable to create a new instance of " + beanClass.getName(), e);
        }
    }

    protected void initializeForm() {
        preBuild(); // Setup before buildFields
        setupBinder();
        postBuild(); // Complete the setup after bindings if necessary
    }

    protected void preBuild() {
    }

    protected abstract void setupBinder();

    protected void postBuild() {
        setupForm();
    }

    protected void setupForm() {
    }

    public void setEntity(T entity) {
        this.currentEntity = entity;
        binder.readBean(entity);
    }

    public boolean writeIfValid(T entity) {
        return binder.writeBeanIfValid(entity);
    }

    private boolean isFormValid() {
        return binder.validate().isOk();
    }

    public boolean validateAndWrite() {
        return isFormValid() && writeIfValid();
    }

    private boolean writeIfValid() {
        return binder.writeBeanIfValid(currentEntity);
    }

    public void resetForm() {
        try {
            this.currentEntity = beanClass.getDeclaredConstructor().newInstance();
        } catch (Exception e) {
            throw new RuntimeException("Unable to create a new instance of " + beanClass.getName(), e);
        }
        binder.readBean(null);
    }

    public Binder<T> getBinder() {
        return binder;
    }

    public T getEntity() {
        return currentEntity;
    }
}
