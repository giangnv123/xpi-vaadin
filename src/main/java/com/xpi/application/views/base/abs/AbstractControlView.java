package com.xpi.application.views.base.abs;

import org.vaadin.lineawesome.LineAwesomeIcon;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.xpi.application.views.base.utils.TextNotification;

/**
 * Abstract base class for views that handle a create or update view
 * 
 * @param <T> the type of the DTO being managed in this view.
 */
public abstract class AbstractControlView<T> extends AbstractView implements HasUrlParameter<String> {
    protected T currentDto;

    private String NOT_FOUND_MESSAGE = "Không tìm thấy dữ liệu";

    protected TextNotification textNotification = new TextNotification();

    protected String entityName = "Document";

    @Override
    public void setParameter(BeforeEvent event, String parameter) {
        parameterHandle(parameter);
    }

    protected abstract void parameterHandle(String parameter);

    protected Image createNotFoundImage() {
        Image image = new Image("/images/not_found.jpg", "not found");
        image.addClassName("not-found-image");
        return image;
    }

    protected Div notFoundDisplay() {
        add(createBackButton());

        Image image = createNotFoundImage();
        Div feedbackContainer = createFeedbackContainer();

        Div layout = new Div();
        layout.addClassName("not-found-layout");
        layout.add(image, feedbackContainer);

        return layout;
    }

    private Div createFeedbackContainer() {

        H3 feedbackText = new H3(NOT_FOUND_MESSAGE);
        Button backButton = createBackButton();

        Div container = new Div();
        container.addClassName("flex-layout");
        container.add(feedbackText, backButton);
        return container;
    }

    protected Button createBackButton() {
        Button backButton = new Button(LineAwesomeIcon.ANGLE_LEFT_SOLID.create());
        backButton.addClickListener(event -> UI.getCurrent().navigate(getMainViewUrl()));
        return backButton;
    }

    protected void handleFetchFailedCase() {
        showErrorMessage(NOT_FOUND_MESSAGE);
        add(notFoundDisplay());
    }

    protected abstract String getMainViewUrl();
}
