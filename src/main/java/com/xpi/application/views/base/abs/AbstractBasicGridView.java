package com.xpi.application.views.base.abs;

import org.vaadin.lineawesome.LineAwesomeIcon;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.Div;
import com.xpi.application.views.base.component.XConfirmDialog;
import com.xpi.application.views.base.component.XDialog;

public abstract class AbstractBasicGridView<T, ID> extends AbstractView {

    protected XConfirmDialog deleteDialog;
    protected XDialog controlDialog;
    protected ID currentId;

    protected String entityName = "Item";

    public AbstractBasicGridView() {

    }

    protected void initialSetup() {
        setSizeFull();
        setupDialog();
        addComponent();
    }

    protected Button createAddEntityButton() {
        Button button = new Button(LineAwesomeIcon.PLUS_CIRCLE_SOLID.create());
        button.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        button.addClickListener(event -> {
            handleClickAdd();
        });
        button.addClassName("absolute-position");
        return button;
    }

    protected void setupDialog() {
        setupControlDialog();
        setupDeleteDialog();
    };

    private void setupDeleteDialog() {
        deleteDialog = new XConfirmDialog(entityName, this::performDelete);
    }

    protected abstract void setupControlDialog();

    protected abstract void performDelete();

    protected abstract void handleClickEdit(T dto);

    protected abstract void handleClickDelete(T dto);

    protected abstract void handleClickAdd();

    protected void addComponent() {
        Div header = createHeaderSection();
        Component content = createContentSection();

        add(header, content);
    }

    protected abstract Div createHeaderSection();

    protected abstract Component createContentSection();

    protected void handleResponse(Boolean isSuccess, String message, XDialog dialog, AbstractForm<?> form,
            Runnable onSuccess) {
        if (isSuccess) {
            dialog.close();
            form.resetForm();
            if (onSuccess != null)
                onSuccess.run();
        }
        showMessage(message, isSuccess);
    }

}
