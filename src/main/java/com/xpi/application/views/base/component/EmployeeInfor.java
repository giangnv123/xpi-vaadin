package com.xpi.application.views.base.component;

import org.vaadin.lineawesome.LineAwesomeIcon;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H5;
import com.vaadin.flow.component.html.Span;
import com.xpi.application.module.hrm.dto.EmployeeDto;
import com.xpi.application.views.base.utils.ColorUtils;
import com.xpi.application.views.base.views.AvatarHelper;
import com.xpi.application.views.base.views.ViewHelper;

public class EmployeeInfor extends Div {

    private EmployeeDto employeeDto;

    public EmployeeInfor(EmployeeDto employeeDto) {
        this.employeeDto = employeeDto;
        setupLayout();
    }

    private void setupLayout() {
        removeAll();

        addClassNames("card");
        add(ViewHelper.titleWithIcon("Thông tin nhân viên", LineAwesomeIcon.USER,
                ColorUtils.WHITE, ColorUtils.BG_SUCCESS));
        add(AvatarHelper.createAvatar(employeeDto != null ? employeeDto.getPhoto() : null, "employee-avatar-round"));

        if (employeeDto == null)
            return;

        Div userDisplay = createInforItem(LineAwesomeIcon.USER, new H5(employeeDto.getFullname()), null);
        Div emailDisplay = createInforItem(LineAwesomeIcon.ENVELOPE, new Span(employeeDto.getEmail()), null);
        Div phoneDisplay = createInforItem(LineAwesomeIcon.PHONE_ALT_SOLID, new Span(employeeDto.getPhone()), null);
        Div designationDisplay = createInforItem(LineAwesomeIcon.BRIEFCASE_SOLID,
                new Span(employeeDto.getDesignation()), null);

        Div container = new Div(userDisplay, emailDisplay, phoneDisplay, designationDisplay);
        container.addClassName("infor-container");
        add(container);
    }

    private Div createInforItem(LineAwesomeIcon icon, Component component, String color) {
        return ViewHelper.createInfoItemWithIcon(icon, component, color);
    }

    public void setEmployeeDto(EmployeeDto employeeDto) {
        this.employeeDto = employeeDto;
        setupLayout();
    }
}
