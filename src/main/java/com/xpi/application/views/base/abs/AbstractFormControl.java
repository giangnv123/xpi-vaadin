package com.xpi.application.views.base.abs;

import java.util.function.Consumer;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.Div;

public abstract class AbstractFormControl<T, D> extends Div {

    protected T currentDto;
    protected Consumer<D> save;

    public AbstractFormControl(Consumer<D> save,
            T currentDto) {
        this.save = save;
        this.currentDto = currentDto;
    }

    protected abstract void populateDto();

    protected void setupLayout() {
        intitializeComponent();

        if (currentDto != null)
            populateDto();

        Div content = createContentSection();
        Div footer = createFooterSection();

        addClassName("relative");
        add(content, footer);
    }

    protected abstract Div createContentSection();

    protected abstract void intitializeComponent();

    protected abstract String getNavigatoMainView();

    /* Functionality */

    protected abstract void onSave();

    /* Footer */

    private Button createCancelButton() {
        Button button = new Button("Hủy", e -> {
            UI.getCurrent().navigate(getNavigatoMainView());
        });
        button.addThemeVariants(ButtonVariant.LUMO_CONTRAST, ButtonVariant.LUMO_LARGE);

        return button;
    }

    private Button createSaveButton() {
        Button button = new Button("Lưu", e -> onSave());

        button.addThemeVariants(ButtonVariant.LUMO_SUCCESS, ButtonVariant.LUMO_PRIMARY, ButtonVariant.LUMO_LARGE);
        button.getStyle().set("background", "#28B463");
        return button;
    }

    private Div createFooterSection() {
        Button canButton = createCancelButton();
        Button saveButton = createSaveButton();

        Div footer = new Div();
        footer.addClassName("footer");
        footer.add(saveButton, canButton);
        return footer;
    }

}
