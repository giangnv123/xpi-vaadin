package com.xpi.application.views.base.utils;

import lombok.Getter;

@Getter
public class TextNotification {
    /* Error message */
    public static String errorUpdatingMessage = "Gặp lỗi khi cập nhật. Vui lòng kiểm tra lại thông tin";
    public static String errorDeletingMessage = "Gặp lỗi khi xóa. Vui lòng thử lại sau";
    public static String errorCreatingMessage = "Gặp lỗi khi tạo mới. Vui lại kiểm tra thông tin";

    public static String undefinedErrorMessage = "Lỗi không xác định, vui lòng thử lại";

    /* Success message */
    public static String successUpdatingMessage = "Cập nhật thành công";
    public static String successDeletingMessage = "Xóa thành công";
    public static String successCreatingMessage = "Tạo thành công";
}
