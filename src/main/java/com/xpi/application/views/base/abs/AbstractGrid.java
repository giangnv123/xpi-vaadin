package com.xpi.application.views.base.abs;

import java.util.function.Consumer;

import org.vaadin.lineawesome.LineAwesomeIcon;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.grid.contextmenu.GridContextMenu;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.SvgIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.xpi.application.views.base.component.ActionComponentCreator;
import com.xpi.application.views.base.utils.ColorUtils;
import com.xpi.application.views.base.views.AvatarHelper;
import com.xpi.application.views.base.views.GridViewHelper;
import com.xpi.application.views.base.views.ViewHelper;
import com.xpi.application.views.crm.shared.components.XFlexLayout;

public abstract class AbstractGrid<T> extends Grid<T> {

    protected ViewHelper viewHelper = new ViewHelper();
    protected AvatarHelper avatarHelper = new AvatarHelper();

    protected Consumer<T> clickEdit;
    protected Consumer<T> clickDelete;
    protected ActionComponentCreator<T> actionCreator;

    protected HorizontalLayout createActionComponents(T dto) {
        actionCreator = new ActionComponentCreator<>(clickEdit, clickDelete);
        return (HorizontalLayout) actionCreator.createComponents(dto);
    }

    public AbstractGrid(Consumer<T> clickEdit, Consumer<T> clickDelete) {
        this.clickEdit = clickEdit;
        this.clickDelete = clickDelete;
    }

    protected GridContextMenu<T> menu = this.addContextMenu();

    protected void initialSetup() {
        addThemeVariants(GridVariant.LUMO_NO_BORDER, GridVariant.LUMO_ROW_STRIPES,
                GridVariant.LUMO_COLUMN_BORDERS, GridVariant.LUMO_COMPACT);
        setSelectionMode(SelectionMode.MULTI);
        setupGrid();
        setupContextMenu();
    }

    private void setupContextMenu() {
        menu.removeAll();

        XFlexLayout delete = new XFlexLayout();
        SvgIcon trashIcon = ViewHelper.createIcon(LineAwesomeIcon.TRASH_ALT_SOLID, ColorUtils.DANGER, null, "20px");
        delete.add(trashIcon, new Span("Xóa"));

        XFlexLayout details = new XFlexLayout();
        SvgIcon eyeIcon = ViewHelper.createIcon(LineAwesomeIcon.EDIT_SOLID, ColorUtils.PRIMARY, null, "20px");
        details.add(eyeIcon, new Span("Chỉnh sửa"));

        menu.addItem(details, event -> {
            clickEdit.accept(event.getItem().orElse(null));
        });

        menu.addItem(delete, event -> {
            clickDelete.accept(event.getItem().orElse(null));
        });

        addCustomContextMenuItems(menu);
    }

    protected void addColumnWithIcon(ComponentRenderer<Component, T> renderer, String header,
            LineAwesomeIcon icon) {

        addColumn(renderer)
                .setHeader(GridViewHelper.createIconTextHeader(icon, header))
                .setAutoWidth(true);
    }

    protected void addCustomContextMenuItems(GridContextMenu<T> menu) {

    };

    protected abstract void setupGrid();
}
