package com.xpi.application.views.base.component;

import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dialog.Dialog;

public class XDialog extends Dialog {
    private final String defaultWidth = "500px";

    public XDialog(String title, Runnable saveHandler) {
        this.setWidth(defaultWidth);
        this.setHeaderTitle(title);
        this.setDraggable(true);
        this.setResizable(true);

        initializeButtons(saveHandler);
    }

    private void initializeButtons(Runnable saveHandler) {
        Button saveButton = createSaveButton(saveHandler);
        Button cancelButton = createCancelButton();
        this.getFooter().add(cancelButton, saveButton);
    }

    private Button createSaveButton(Runnable saveHandler) {
        Button saveButton = new Button("Lưu");
        saveButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
        saveButton.addClickShortcut(Key.ENTER);
        saveButton.addClickListener(event -> saveHandler.run());
        return saveButton;
    }

    private Button createCancelButton() {
        return new Button("Hủy", event -> this.close());
    }
}
