package com.xpi.application.views.base.utils;

public class SharedProperties {
    public static String NEW_MAIN_VIEW = "new";

    public static String IMAGE_NOT_FOUND_URL = "/images/not_found.jpg";

    public static String DEFAULT_AVATAR = "/images/avatar-men-default.jpg";

    public static String API_SERVE_FILE = "http://localhost:8088/xapi/v1/file/files";

}
