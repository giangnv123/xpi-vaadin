package com.xpi.application.views.base.component;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.confirmdialog.ConfirmDialog;

public class XConfirmDialog extends ConfirmDialog {
    private final String defaultWidth = "400px";

    public XConfirmDialog(String entityName, Runnable confirmHandler) {
        setWidth(defaultWidth);

        setHeader("Xóa " + entityName);
        setText("Bạn có chắc chắn xóa bản ghi này?");

        Button confirmButton = new Button("Xóa", event -> {
            confirmHandler.run();
            close();
        });
        confirmButton.addThemeVariants(ButtonVariant.LUMO_PRIMARY, ButtonVariant.LUMO_ERROR);

        setConfirmButton(confirmButton);
        setCancelButton("Hủy", event -> close());

        addRejectListener(event -> close());
    }
}