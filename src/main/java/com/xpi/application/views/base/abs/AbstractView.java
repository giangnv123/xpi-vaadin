package com.xpi.application.views.base.abs;

import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.xpi.application.views.base.component.XNotification;

public abstract class AbstractView extends VerticalLayout {
    protected XNotification notification = new XNotification();

    public AbstractView() {
    }

    protected void showErrorMessage(String message) {
        notification.createErrorNotification(message);
    }

    protected void showSuccessMessage(String message) {
        notification.createSuccessNotification(message);
    }

    protected void showMessage(String message, Boolean isSuccess) {
        if (isSuccess) {
            showSuccessMessage(message);
        } else {
            showErrorMessage(message);
        }
    }
}
