package com.xpi.application.views.base.utils;

import lombok.Data;
import lombok.NoArgsConstructor;

import com.xpi.application.base.ApiResponse;

import lombok.AllArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CustomApiResponse<T> {
    private Boolean isSuccess;
    private String message;
    private T dto;

    public Boolean isSuccess() {
        return this.isSuccess;
    }

    public static <T> CustomApiResponse<T> save(ApiResponse<?> response, Boolean isUpdate, T dto) {
        String messageText;
        messageText = response.isSuccess()
                ? (response.getReadable() ? response.getMessage() : TextNotification.successCreatingMessage)
                : (response.getReadable() ? response.getMessage() : TextNotification.errorCreatingMessage);

        if (isUpdate) {
            messageText = response.isSuccess()
                    ? (response.getReadable() ? response.getMessage() : TextNotification.successUpdatingMessage)
                    : (response.getReadable() ? response.getMessage() : TextNotification.errorUpdatingMessage);
        }
        return new CustomApiResponse<T>(response.isSuccess(), messageText, dto);
    }

    // Handle delete success and failure
    public static <T> CustomApiResponse<T> delete(ApiResponse<T> response) {
        String messageText = response.isSuccess()
                ? (response.getReadable() ? response.getMessage() : TextNotification.successDeletingMessage)
                : (response.getReadable() ? response.getMessage() : TextNotification.errorDeletingMessage);
        return new CustomApiResponse<T>(response.isSuccess(), messageText, response.getData());
    }
}
