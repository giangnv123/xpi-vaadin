package com.xpi.application.views.base.component;

import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;

public class XNotification extends Notification {
        public Notification createSuccessNotification(String message) {
                Notification notification = new Notification();
                notification.setPosition(Position.BOTTOM_END);

                Icon icon = VaadinIcon.CHECK_CIRCLE.create();
                icon.setColor("var(--lumo-success-color)");

                Div uploadSuccessful = new Div(new Text("Thành công"));
                uploadSuccessful.getStyle()
                                .set("font-weight", "600")
                                .setColor("var(--lumo-success-text-color)");

                Div info = new Div(uploadSuccessful,
                                new Div(new Text(message)));

                info.getStyle()
                                .set("font-size", "var(--lumo-font-size-s)")
                                .setColor("var(--lumo-secondary-text-color)");

                var layout = new HorizontalLayout(icon, info,
                                createCloseBtn(notification));
                layout.setAlignItems(FlexComponent.Alignment.CENTER);

                notification.add(layout);
                notification.setDuration(5000);
                notification.open();

                return notification;
        }

        public Notification createErrorNotification(String message) {
                Notification notification = new Notification();
                // notification.addThemeVariants(NotificationVariant.LUMO_ERROR);
                notification.setPosition(Position.BOTTOM_END);

                Icon icon = VaadinIcon.WARNING.create();
                icon.setColor("var(--lumo-error-color)");

                var layout = new HorizontalLayout(icon, new Text(message),
                                createCloseBtn(notification));
                layout.getStyle().setColor("var(--lumo-error-color)");
                layout.setAlignItems(FlexComponent.Alignment.CENTER);

                notification.add(layout);
                notification.setDuration(5000);
                notification.open();

                return notification;
        }

        public static Button createCloseBtn(Notification notification) {
                Button closeBtn = new Button(VaadinIcon.CLOSE_SMALL.create(),
                                clickEvent -> notification.close());
                closeBtn.getStyle().set("cursor", "pointer");
                closeBtn.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
                return closeBtn;
        }
}
